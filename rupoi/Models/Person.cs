﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Data.Common;

namespace rupoi.Models
{
    public class Person
    {
        public int Id { get; set; }
        public int PrivateCaseNum { get; set; }
        public string PIN {get;set;}
        public int? DicDocumentId {get;set;}
        public DicDocument DicDocument { get; set; }
        public string Surname {get;set;}
        public string Name {get;set;}
        public string Patronimic {get;set;}
        public string Gender { get; set; }
        public DateTime BirthDate{get;set;}
        public int? DicDocumentSeriesId {get;set;}
        public DicDocumentSeries DicDocumentSeries { get; set; }
        public string DocumentNum {get;set;}
        public string DocumentCreateAuthority {get;set;}
        public DateTime DocumentCreateDate {get;set;}
        public string PensionCertificateNum {get;set;}
        public DateTime PensionCertificateCreateDate {get;set;}
        public string PensionCertificateCreateAuthority {get;set;}
        public string AddressRegRegion {get;set;}
        public string AddressRegDistrict {get;set;}
        public string AddressRegVillage {get;set;}
        public string AddressRegCity {get;set;}
        public string AddressRegStreet {get;set;}
        public string AddressRegHouse { get;set;}
        public string AddressRegApartment { get; set; }
        public int? DicRegionId {get;set;}
        public DicRegion DicRegion { get; set; }
        public int? DicDistrictId {get;set;}
        public DicDistrict DicDistrict { get; set; }
        public int? DicVillageId {get;set;}
        public DicVillage DicVillage { get;set;}
        public int? DicCityId {get;set;}
        public DicCity DicCity {get;set; }
        public string AddressActStreet {get;set;}
        public string AddressActHouse {get;set;}
        public string AddressActApartment { get; set; }
        public string PhoneNum {get;set;}
        public string PhoneNumOther {get;set;}
        public string PlaceOfWork {get;set;}
        public int? DicDisabilityCategoryId {get;set;}
        public DicDisabilityCategory DicDisabilityCategory { get;set; }
        public int? DicDisabilityGroupId {get;set;}
        public DicDisabilityGroup DicDisabilityGroup { get;set;}
        public int? DicDisabilityReasonId {get;set;}
        public DicDisabilityReason DicDisabilityReason { get;set;}
        public string OperatedWhereWhen { get; set; }
        public string Supplements { get; set; }
        public List<ServiceDirection> ServiceDirections { get; set; }
        public List<ProsthesisData> ProsthesisDatas { get; set; }
        public List<Prosthesis> Prostheses { get; set; }
        public List<Shoe> Shoes { get; set; }
        public List<Ottobock> Ottobocks { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace rupoi.Models
{
    public class Ottobock
    {
        public int Id {get;set;}
        public int PersonId {get;set; }
        public int? OrderNum { get; set; }
        [DataType(DataType.Date)]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:yyyy-MM-dd}")]
        public DateTime OrderDate { get; set; }


        public int? DicSubProductTypeId { get; set; }
        public DicSubProductType DicSubProductType { get; set; }

        public int? DicSubDiagnosisId { get; set; }
        public DicSubDiagnosis DicSubDiagnosis { get; set; }


        public int? DicProsthesisSideId { get; set; }
        public DicProsthesisSide DicProsthesisSide { get; set; }


        public int? DicHospitalizedId { get; set; }
        public DicHospitalized DicHospitalized { get; set; }


        public int? DicTypeOrderId { get; set; }
        public DicTypeOrder DicTypeOrder { get; set; }


        public int? DicOrderStatusId { get; set; }
        public DicOrderStatus DicOrderStatus { get; set; }


        public string OrderStatusReason { get; set; }

        public int? DicServiceId { get; set; }
        public DicService DicService { get; set; }


        [DataType(DataType.Date)]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:yyyy-MM-dd}")]
        public DateTime? CallDate1 { get; set; }
        [DataType(DataType.Date)]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:yyyy-MM-dd}")]
        public DateTime? CallDate2 { get; set; }
        [DataType(DataType.Date)]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:yyyy-MM-dd}")]
        public DateTime? CallDate3 { get; set; }
        [DataType(DataType.Date)]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:yyyy-MM-dd}")]
        public DateTime? ArrivalDate1 { get; set; }
        [DataType(DataType.Date)]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:yyyy-MM-dd}")]
        public DateTime? ArrivalDate2 { get; set; }
        [DataType(DataType.Date)]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:yyyy-MM-dd}")]
        public DateTime? ArrivalDate3 { get; set; }
        public DateTime? ManufacturingDate { get; set; }
        [DataType(DataType.Date)]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:yyyy-MM-dd}")]
        public DateTime? IssuingDate { get; set; }

        public int? DicFinishedStatusId { get; set; }
        public DicFinishedStatus DicFinishedStatus { get;set; }


        public int? DicImpressionId { get; set; }
        public DicImpression DicImpression { get; set; }

        public int? MaterialsAndEquipmentTableId { get; set; }
    }
}



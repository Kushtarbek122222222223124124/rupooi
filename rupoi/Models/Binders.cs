﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace rupoi.Models
{

    public class MaterialsAndEquipmentTableBind
    {
        public string VendorCode_1 { get; set; }
        public string NameOfMaterials_1 { get; set; }
        public string Unit_1 { get; set; }
        public string Quontity_1 { get; set; }
        public string Sum_1 { get; set; }
        public string Note_1 { get; set; }



        public string VendorCode_2 { get; set; }
        public string NameOfMaterials_2 { get; set; }
        public string Unit_2 { get; set; }
        public string Quontity_2 { get; set; }
        public string Sum_2 { get; set; }
        public string Note_2 { get; set; }


        public string VendorCode_3 { get; set; }
        public string NameOfMaterials_3 { get; set; }
        public string Unit_3 { get; set; }
        public string Quontity_3 { get; set; }
        public string Sum_3 { get; set; }
        public string Note_3 { get; set; }


        public string VendorCode_4 { get; set; }
        public string NameOfMaterials_4 { get; set; }
        public string Unit_4 { get; set; }
        public string Quontity_4 { get; set; }
        public string Sum_4 { get; set; }
        public string Note_4 { get; set; }

        public string VendorCode_5 { get; set; }
        public string NameOfMaterials_5 { get; set; }
        public string Unit_5 { get; set; }
        public string Quontity_5 { get; set; }
        public string Sum_5 { get; set; }
        public string Note_5 { get; set; }

        public string VendorCode_6 { get; set; }
        public string NameOfMaterials_6 { get; set; }
        public string Unit_6 { get; set; }
        public string Quontity_6 { get; set; }
        public string Sum_6 { get; set; }
        public string Note_6 { get; set; }

        public string VendorCode_7 { get; set; }
        public string NameOfMaterials_7 { get; set; }
        public string Unit_7 { get; set; }
        public string Quontity_7 { get; set; }
        public string Sum_7 { get; set; }
        public string Note_7 { get; set; }

        public string VendorCode_8 { get; set; }
        public string NameOfMaterials_8 { get; set; }
        public string Unit_8 { get; set; }
        public string Quontity_8 { get; set; }
        public string Sum_8 { get; set; }
        public string Note_8 { get; set; }

        public string VendorCode_9 { get; set; }
        public string NameOfMaterials_9 { get; set; }
        public string Unit_9 { get; set; }
        public string Quontity_9 { get; set; }
        public string Sum_9 { get; set; }
        public string Note_9 { get; set; }

        public string VendorCode_10 { get; set; }
        public string NameOfMaterials_10 { get; set; }
        public string Unit_10 { get; set; }
        public string Quontity_10 { get; set; }
        public string Sum_10 { get; set; }
        public string Note_10 { get; set; }

        public string VendorCode_11 { get; set; }
        public string NameOfMaterials_11 { get; set; }
        public string Unit_11 { get; set; }
        public string Quontity_11 { get; set; }
        public string Sum_11 { get; set; }
        public string Note_11 { get; set; }

        public string VendorCode_12 { get; set; }
        public string NameOfMaterials_12 { get; set; }
        public string Unit_12 { get; set; }
        public string Quontity_12 { get; set; }
        public string Sum_12 { get; set; }
        public string Note_12 { get; set; }


        public string VendorCode_13 { get; set; }
        public string NameOfMaterials_13 { get; set; }
        public string Unit_13 { get; set; }
        public string Quontity_13 { get; set; }
        public string Sum_13 { get; set; }
        public string Note_13 { get; set; }


        public string VendorCode_14 { get; set; }
        public string NameOfMaterials_14 { get; set; }
        public string Unit_14 { get; set; }
        public string Quontity_14 { get; set; }
        public string Sum_14 { get; set; }
        public string Note_14 { get; set; }

        public string VendorCode_15 { get; set; }
        public string NameOfMaterials_15 { get; set; }
        public string Unit_15 { get; set; }
        public string Quontity_15 { get; set; }
        public string Sum_15 { get; set; }
        public string Note_15 { get; set; }

        public string VendorCode_16 { get; set; }
        public string NameOfMaterials_16 { get; set; }
        public string Unit_16 { get; set; }
        public string Quontity_16 { get; set; }
        public string Sum_16 { get; set; }
        public string Note_16 { get; set; }

        public string VendorCode_17 { get; set; }
        public string NameOfMaterials_17 { get; set; }
        public string Unit_17 { get; set; }
        public string Quontity_17 { get; set; }
        public string Sum_17 { get; set; }
        public string Note_17 { get; set; }
    }

    // для получения такой формы
    public class ProsthesisProductsTableBind
    {

        public int ProductNameId_1 { get; set; }
        public int ProductCode_1 { get; set; }
        public int ProductSize_1 { get; set; }
        public int ProductLeftQuontity_1 { get; set; }
        public int ProductRightQuontity_1 { get; set; }

        public int ProductNameId_2 { get; set; }
        public int ProductCode_2 { get; set; }
        public int ProductSize_2 { get; set; }
        public int ProductLeftQuontity_2 { get; set; }
        public int ProductRightQuontity_2 { get; set; }

        public int ProductNameId_3 { get; set; }
        public int ProductCode_3 { get; set; }
        public int ProductSize_3 { get; set; }
        public int ProductLeftQuontity_3 { get; set; }
        public int ProductRightQuontity_3 { get; set; }

        public int ProductNameId_4 { get; set; }
        public int ProductCode_4 { get; set; }
        public int ProductSize_4 { get; set; }
        public int ProductLeftQuontity_4 { get; set; }
        public int ProductRightQuontity_4 { get; set; }

        public int ProductNameId_5 { get; set; }
        public int ProductCode_5 { get; set; }
        public int ProductSize_5 { get; set; }
        public int ProductLeftQuontity_5 { get; set; }
        public int ProductRightQuontity_5 { get; set; }

        public int ProductNameId_6 { get; set; }
        public int ProductCode_6 { get; set; }
        public int ProductSize_6 { get; set; }
        public int ProductLeftQuontity_6 { get; set; }
        public int ProductRightQuontity_6 { get; set; }

        public int ProductNameId_7 { get; set; }
        public int ProductCode_7 { get; set; }
        public int ProductSize_7 { get; set; }
        public int ProductLeftQuontity_7 { get; set; }
        public int ProductRightQuontity_7 { get; set; }

        public int ProductNameId_8 { get; set; }
        public int ProductCode_8 { get; set; }
        public int ProductSize_8 { get; set; }
        public int ProductLeftQuontity_8 { get; set; }
        public int ProductRightQuontity_8 { get; set; }

        public int ProductNameId_9 { get; set; }
        public int ProductCode_9 { get; set; }
        public int ProductSize_9 { get; set; }
        public int ProductLeftQuontity_9 { get; set; }
        public int ProductRightQuontity_9 { get; set; }

        public int ProductNameId_10 { get; set; }
        public int ProductCode_10 { get; set; }
        public int ProductSize_10 { get; set; }
        public int ProductLeftQuontity_10 { get; set; }
        public int ProductRightQuontity_10 { get; set; }

        public int ProductNameId_11 { get; set; }
        public int ProductCode_11 { get; set; }
        public int ProductSize_11 { get; set; }
        public int ProductLeftQuontity_11 { get; set; }
        public int ProductRightQuontity_11 { get; set; }

        public int ProductNameId_12 { get; set; }
        public int ProductCode_12 { get; set; }
        public int ProductSize_12 { get; set; }
        public int ProductLeftQuontity_12 { get; set; }
        public int ProductRightQuontity_12 { get; set; }

        public int ProductNameId_13 { get; set; }
        public int ProductCode_13 { get; set; }
        public int ProductSize_13 { get; set; }
        public int ProductLeftQuontity_13 { get; set; }
        public int ProductRightQuontity_13 { get; set; }

        public int ProductNameId_14 { get; set; }
        public int ProductCode_14 { get; set; }
        public int ProductSize_14 { get; set; }
        public int ProductLeftQuontity_14 { get; set; }
        public int ProductRightQuontity_14 { get; set; }

        public int ProductNameId_15 { get; set; }
        public int ProductCode_15 { get; set; }
        public int ProductSize_15 { get; set; }
        public int ProductLeftQuontity_15 { get; set; }
        public int ProductRightQuontity_15 { get; set; }

        public int ProductNameId_16 { get; set; }
        public int ProductCode_16 { get; set; }
        public int ProductSize_16 { get; set; }
        public int ProductLeftQuontity_16 { get; set; }
        public int ProductRightQuontity_16 { get; set; }

        public int ProductNameId_17 { get; set; }
        public int ProductCode_17 { get; set; }
        public int ProductSize_17 { get; set; }
        public int ProductLeftQuontity_17 { get; set; }
        public int ProductRightQuontity_17 { get; set; }
    }

    // для получения такой формы
    public class ShoeOperationTableBind
    {

        public int OperationId_1 { get; set; }
        public string ExecutorFullName_1 { get; set; }
        public DateTime ExecuteDate_1 { get; set; }
        public string OtkMark_1 { get; set; }

        public int OperationId_2 { get; set; }
        public string ExecutorFullName_2 { get; set; }
        public DateTime ExecuteDate_2 { get; set; }
        public string OtkMark_2 { get; set; }

        public int OperationId_3 { get; set; }
        public string ExecutorFullName_3 { get; set; }
        public DateTime ExecuteDate_3 { get; set; }
        public string OtkMark_3 { get; set; }

        public int OperationId_4 { get; set; }
        public string ExecutorFullName_4 { get; set; }
        public DateTime ExecuteDate_4 { get; set; }
        public string OtkMark_4 { get; set; }

        public int OperationId_5 { get; set; }
        public string ExecutorFullName_5 { get; set; }
        public DateTime ExecuteDate_5 { get; set; }
        public string OtkMark_5 { get; set; }

        public int OperationId_6 { get; set; }
        public string ExecutorFullName_6 { get; set; }
        public DateTime ExecuteDate_6 { get; set; }
        public string OtkMark_6 { get; set; }

        public int OperationId_7 { get; set; }
        public string ExecutorFullName_7 { get; set; }
        public DateTime ExecuteDate_7 { get; set; }
        public string OtkMark_7 { get; set; }

        public int OperationId_8 { get; set; }
        public string ExecutorFullName_8 { get; set; }
        public DateTime ExecuteDate_8 { get; set; }
        public string OtkMark_8 { get; set; }

        public int OperationId_9 { get; set; }
        public string ExecutorFullName_9 { get; set; }
        public DateTime ExecuteDate_9 { get; set; }
        public string OtkMark_9 { get; set; }

        public int OperationId_10 { get; set; }
        public string ExecutorFullName_10 { get; set; }
        public DateTime ExecuteDate_10 { get; set; }
        public string OtkMark_10 { get; set; }

        public int OperationId_11 { get; set; }
        public string ExecutorFullName_11 { get; set; }
        public DateTime ExecuteDate_11 { get; set; }
        public string OtkMark_11 { get; set; }

        public int OperationId_12 { get; set; }
        public string ExecutorFullName_12 { get; set; }
        public DateTime ExecuteDate_12 { get; set; }
        public string OtkMark_12 { get; set; }
    }

}

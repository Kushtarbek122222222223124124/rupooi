﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace rupoi.Models
{
    public class AddPrintDataShoe
    {
        public int? ShoeId { get; set; }

        public List<DicSubDescriptionProduct> DicSubDescriptionProducts { get; set; }
        public List<DicNameProduct> DicNameProducts { get; set; }
        public List<DicMountingType> DicMountingTypes { get; set; }
        public List<DicSoleType> DicSoleTypes { get; set; }
        public List<DicImpression> DicImpressions { get; set; }
    }

    public class AddPrintDataProsthesis
    {
        public int? ProsthesisId { get; set; }

        public List<DicNameProduct> DicNameProducts { get; set; }
        public List<DicCodeProduct> DicCodeProducts { get; set; }
        public List<DicImpression> DicImpressions { get; set; }
    }


    public class CreateOttobock
    {
        public int? PersonId { get; set; }
        public List<DicProductType> DicProductTypes { get; set; }
        public List<DicSubProductType> DicSubProductTypes { get; set; }
        public List<DicDiagnosis> DicDiagnoses { get; set; }
        public List<DicSubDiagnosis> DicSubDiagnoses { get; set; }
        public List<DicProsthesisSide> DicProsthesisSides { get; set; }
        public List<DicHospitalized> DicHospitalizeds { get; set; }
        public List<DicTypeOrder> DicTypeOrders { get; set; }
        public List<DicOrderStatus> DicOrderStatuses { get; set; }
        public List<DicService> DicServices { get; set; }
        public List<DicFinishedStatus> DicFinishedStatuses { get; set; }
        public List<DicNameProduct> DicNameProducts { get; set; }
    }

    public class CreateShoe
    {
        public int? PersonId { get; set; }
        public List<DicProductType> DicProductTypes { get; set; }
        public List<DicSubProductType> DicSubProductTypes { get; set; }
        public List<DicDiagnosis> DicDiagnoses { get; set; }
        public List<DicSubDiagnosis> DicSubDiagnoses { get; set; }
        public List<DicModel> DicModels { get; set; }
        public List<DicColor> DicColors { get; set; }
        public List<DicHospitalized> DicHospitalizeds { get; set; }
        public List<DicTypeOrder> DicTypeOrders { get; set; }
        public List<DicOrderStatus> DicOrderStatuses { get; set; }
        public List<DicService> DicServices { get; set; }
        public List<DicFinishedStatus> DicFinishedStatuses { get; set; }

        public List<DicShoeOperation> DicShoeOperations { get; set; }
    }

    public class CreateProsthesis
    {
        public int? PersonId {get;set;}
        public List<DicProductType> DicProductTypes {get;set;}
        public List<DicSubProductType> DicSubProductTypes { get;set;}
        public List<DicDiagnosis> DicDiagnoses { get;set;}
        public List<DicSubDiagnosis> DicSubDiagnoses {get;set;}
        public List<DicProsthesisSide> DicProsthesisSides {get;set;}
        public List<DicHospitalized> DicHospitalizeds {get;set; }
        public List<DicTypeOrder> DicTypeOrders { get; set; }
        public List<DicOrderStatus> DicOrderStatuses {get;set;}
        public List<DicService> DicServices {get;set;}
        public List<DicFinishedStatus> DicFinishedStatuses {get;set;}
        public List<DicNameProduct> DicNameProducts {get;set;}
    }

    public class CreateProsthesisData
    {
        public int? PersonId { get; set; }
        public List<DicStumpShape> DicStumpShapes { get; set; }
        public List<DicStumpMobility> DicStumpMobilities { get; set; }
        public List<DicScar> DicScars { get; set; }
        public List<DicStumpSkinAndTissues> DicStumpSkinAndTissues { get; set; }
        public List<DicBoneSawdust> DicBoneSawdusts { get; set; }
        public List<DicStumpSupport> DicStumpSupports { get; set; }
    }

    public class CreateServiceDirection
    {
        public int? PersonId { get; set; }
        public List<DicServiceType> DicServiceTypes { get; set; }
    }

    public class CreatePerson
    {
        public List<DicDocument> DicDocuments { get; set; }
        public List<DicDocumentSeries> DicDocumentSeries { get; set; }
        public List<DicRegion> DicRegions { get; set; }
        public List<DicDistrict> DicDistricts { get; set; }
        public List<DicVillage> DicVillages { get; set; }
        public List<DicCity> DicCities { get; set; }
        public List<DicDisabilityCategory> DicDisabilityCategories { get; set; }
        public List<DicDisabilityGroup> DicDisabilityGroups { get; set; }
        public List<DicDisabilityReason> DicDisabilityReasons { get; set; }
    }
}

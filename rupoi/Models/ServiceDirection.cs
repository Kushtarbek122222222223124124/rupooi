﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace rupoi.Models
{
    public class ServiceDirection
    {
        public int Id { get; set; }
        public int PersonId {get;set; }
        [DataType(DataType.Date)]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:yyyy-MM-dd}")]
        public DateTime ReferalDate { get; set; }
        public int ReferalDiagnosisId { get;set;}
        public string Institution { get; set; }
        public string DoctorFullName { get;set;}
        public int DicServiceTypeId { get;set;}
        public DicServiceType DicServiceType { get;set;}
    }
}

﻿using rupoi.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace rupoi.Models.Dictionaries
{
    public class DictionariesFileterModel
    {
        [LocalizedDisplayName("DictionariesFileterModel_Name")]
        public string Name { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace rupoi.Models
{
    public class ProsthesisData
    {
        public int Id { get; set; }
        public int PersonId { get; set; }
        public int StumpLength { get; set; }
        public int DicStumpShapeId { get; set; }
        public DicStumpShape DicStumpShape { get; set; }
        public int DicStumpMobilityId { get; set; }
        public DicStumpMobility DicStumpMobility { get; set; }
        public int DicScarId { get; set; }
        public DicScar DicScar { get; set; }
        public int DicStumpSkinAndTissuesId { get; set; }
        public DicStumpSkinAndTissues DicStumpSkinAndTissues { get; set; }
        public int DicBoneSawdustId { get; set; }
        public DicBoneSawdust DicBoneSawdust { get; set; }
        public int DicStumpSupportId { get; set; }
        public DicStumpSupport DicStumpSupport { get; set; }
        public string ObjectiveData { get; set; }

    }
}

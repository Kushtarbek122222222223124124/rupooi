﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace rupoi.Models
{
    public class Shoe
    {
        public int Id {get;set;}
        public int PersonId {get;set; }
        public int? OrderNum { get; set; }
        [DataType(DataType.Date)]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:yyyy-MM-dd}")]
        public DateTime OrderDate { get; set; }

        public int? DicSubDiagnosisId { get; set; }
        public DicSubDiagnosis DicSubDiagnosis { get; set; }

        public int? DicModelId {get;set;}
        public DicModel DicModel { get;set; }

        public int? DicColorId {get;set;}
        public DicColor DicColor { get;set;}

        public string Material {get;set;}
        public string HeelHeight {get;set;}
        public string HeelMaterial {get;set;}

        public int? DicHospitalizedId { get; set; }
        public DicHospitalized DicHospitalized { get; set; }


        public int? DicTypeOrderId { get; set; }
        public DicTypeOrder DicTypeOrder { get; set; }



        public string Table {get;set; }
        
        public int? DicOrderStatusId { get; set; }
        public DicOrderStatus DicOrderStatus { get; set; }
        
        public string OrderStatusReason { get; set; }
        
        public int? DicServiceId { get; set; }
        public DicService DicService { get; set; }
        
        public DateTime CallDate1 { get; set; }
        [DataType(DataType.Date)]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:yyyy-MM-dd}")]
        public DateTime CallDate2 { get; set; }
        [DataType(DataType.Date)]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:yyyy-MM-dd}")]
        public DateTime CallDate3 { get; set; }
        [DataType(DataType.Date)]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:yyyy-MM-dd}")]
        public DateTime ArrivalDate1 { get; set; }
        [DataType(DataType.Date)]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:yyyy-MM-dd}")]
        public DateTime ArrivalDate2 { get; set; }
        [DataType(DataType.Date)]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:yyyy-MM-dd}")]
        public DateTime ArrivalDate3 { get; set; }
        [DataType(DataType.Date)]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:yyyy-MM-dd}")]
        public DateTime ManufacturingDate { get; set; }
        [DataType(DataType.Date)]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:yyyy-MM-dd}")]
        public DateTime IssuingDate { get; set; }
        
        public int? DicFinishedStatusId { get; set; }
        public DicFinishedStatus DicFinishedStatus { get; set; }

        
        public int? DicSubDescriptionProductId { get; set; }
        public DicSubDescriptionProduct DicSubDescriptionProduct { get; set; }
        
        public int? DicNameProductId { get; set; }
        public DicNameProduct DicNameProduct { get; set; }

        public int? DicMountingTypeId { get; set; }
        public DicMountingType DicMountingType { get; set; }

        public int? DicSoleTypeId { get; set; }
        public DicSoleType DicSoleType { get; set; }

        public int? ArchTabSize { get; set; }
        public int? PronatorSize { get; set; }
        public int? FootSize { get; set; }
        public int? ShorteningSize { get; set; }
        public int? HeelCorkSize { get; set; }

        public int? DicImpressionId { get; set; }
        public DicImpression DicImpression { get; set; }

        public int? ShoeOperationsTableId { get; set; }
    }
}

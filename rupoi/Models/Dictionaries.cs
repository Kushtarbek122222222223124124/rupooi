﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace rupoi.Models
{
    public class DicBase
    {
        public int Id { get; set; }
        public string NameKyrg { get; set; }
        public string NameRus { get; set; }
    }
    public class DicDocument : DicBase
    {
    }

    public class DicDocumentSeries : DicBase
    {
    }

    public class DicRegion : DicBase
    {
        public List<DicDistrict> Districts { get; set; }
    }

    public class DicDistrict : DicBase
    {
        public int RegionId { get; set; }
        public List<DicVillage> Villages { get; set; }
    }

    public class DicVillage : DicBase
    {
        public int DistrictId { get; set; }
    }

    public class DicCity : DicBase
    {
        public int RegionId { get; set; }
    }

    public class DicDisabilityCategory : DicBase
    {
    }

    public class DicDisabilityGroup : DicBase
    {
    }

    public class DicDisabilityReason : DicBase
    {
    }

    public class DicServiceType : DicBase
    {
    }

    public class DicDiagnosis : DicBase
    {
    }

    public class DicService : DicBase
    {
    }

    public class DicOrderStatus : DicBase
    {
    }

    public class DicHospitalized : DicBase
    {
    }


    // слепок
    public class DicImpression : DicBase
    {
    }

    public class DicNameProduct : DicBase
    {
        public List<DicCodeProduct> DicCodeProducts { get; set; }
    }
    public class DicShoeOperation : DicBase
    {
    }


    public class DicCodeProduct : DicBase
    {
        public int DicNameProductId { get; set; }
    }

    public class DicProductType : DicBase
    {
    }

    public class DicProsthesisSide : DicBase
    {
    }

    // рубец
    public class DicScar : DicBase
    {
    }

    public class DicStumpSupport : DicBase
    {
    }

    public class DicBoneSawdust : DicBase
    {
    }

    public class DicStumpSkinAndTissues : DicBase
    {
    }

    public class DicStumpMobility : DicBase
    {
    }

    public class DicStumpShape : DicBase
    {
    }

    public class DicModel : DicBase
    {
    }

    public class DicColor : DicBase
    {
    }

    public class DicDescriptionProduct : DicBase
    {
    }

    public class DicTypeOrder : DicBase
    {
    }

    // вид подошвы
    public class DicSoleType : DicBase
    {
    }

    // вид крепления
    public class DicMountingType : DicBase
    {
    }

    // дополнительные справочники для справочников
    public class DicPersonCardStatus
    {
        public int Id { get; set; }

        public string Name { get; set; }
    }

    public static class DicPersonCardStatusValues
    {
        public static readonly int First = 1;
        public static readonly int Second = 2;
        public static readonly int Third = 3;
        public static readonly int Fourth = 4;
        public static readonly int Fifth = 5;
        public static readonly int Sixth = 5;
    }

    public class DicSubDiagnosis : DicBase
    {
        public int DicDiagnosisId { get; set; }
    }

    public class DicSubProductType : DicBase
    {
        public int DicProductTypeId { get; set; }
        public string Code { get; set; }
    }

    public class DicSubDescriptionProduct : DicBase
    {
        public int DicDescriptionProductId { get; set; }
    }

    public class DicFinishedStatus : DicBase
    {
    }

    // дополнительная таблица, которая поможет использовать статистическую таблицу внутри таблицы

    // таблица изделий
    // связывающая таблица таблицы изделий с протезом
    public class ProsthesisProductsTable
    {
        public int Id { get; set; }
        public List<ProsthesisProductsRow> ProsthesisProductsRows { get; set; }
    }

    public class ProsthesisProductsRow
    {
        public int Id { get; set; }
        public int ProsthesisProductsTableId { get; set; }

        public int DicNameProductId { get; set; }
        public DicNameProduct DicNameProduct { get; set; }
        public int DicCodeProductId { get; set; }
        public DicCodeProduct DicCodeProduct { get; set; }
        public int Size { get; set; }
        public int LeftQuontity { get; set; }
        public int RightQuontity { get; set; }
    }


    // таблица операций
    public class ShoeOperationsTable
    {
        public int Id { get; set; }
        public List<ShoeOperationsRow> ShoeOperationsRows { get; set; }
    }

    public class ShoeOperationsRow
    {
        public int Id { get; set; }
        public int ShoeOperationsTableId { get; set; }

        public int DicShoeOperationId { get; set; }
        public DicShoeOperation DicShoeOperation { get; set; }

        public string ExecutorFullName { get; set; }
        public DateTime ExecuteDate { get; set; }
        public string OtkMark { get; set; }
    }


    // Таблица Материалы и комплектация
    public class MaterialsAndEquipmentTable
    {
        public int Id { get; set; }
        public List<MaterialsAndEquipmentRow> MaterialsAndEquipmentRows { get; set; } 
    }

    public class MaterialsAndEquipmentRow
    {
        public int Id { get; set; }
        public int MaterialsAndEquipmentTableId { get; set; }

        public string VendorCode { get; set; }

        public string NameOfMaterials { get; set; }

        public string Unit { get; set; }

        public string Quontity { get; set; }

        public string Sum { get; set; }

        public string Note { get; set; }
    }
}

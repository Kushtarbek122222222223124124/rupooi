﻿using rupoi.Infrastructure;
using rupoi.Resources;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace rupoi.Models.Registration
{
    public class RegistrationAdd
    {
        [LocalizedDisplayName("RegistrationAdd_Inn")]
        public string Inn { get; set; }

        [UIHint("DropDown")]
        [LocalizedDisplayName("RegistrationAdd_DocumentType")]
        public int DocumentType { get; set; }

        [LocalizedDisplayName("RegistrationAdd_FirstName")]
        public string FirstName { get; set; }

        [LocalizedDisplayName("RegistrationAdd_Name")]
        public string Name { get; set; }

        [LocalizedDisplayName("RegistrationAdd_ParentName")]
        public string ParentName { get; set; }

        [UIHint("DropDown")]
        [LocalizedDisplayName("RegistrationAdd_Sex")]
        public int Sex { get; set; }

        [UIHint("Date")]
        [LocalizedDisplayName("RegistrationAdd_BirthDate")]
        public DateTime BirthDate { get; set; }

        [UIHint("DropDown")]
        [LocalizedDisplayName("RegistrationAdd_DocumentSeries")]
        public string DocumentSeries { get; set; }

        [LocalizedDisplayName("RegistrationAdd_DocumentNumber")]
        public string DocumentNumber { get; set; }

        [UIHint("Date")]
        [LocalizedDisplayName("RegistrationAdd_DocumentIssueDate")]
        public DateTime DocumentIssueDate { get; set; }

        [LocalizedDisplayName("RegistrationAdd_DocumentIssuedBy")]
        public string DocumentIssuedBy { get; set; }

        [LocalizedDisplayName("RegistrationAdd_Address")]
        public string Address { get; set; }

        [LocalizedDisplayName("RegistrationAdd_PhoneNumber")]
        public string PhoneNumber { get; set; }

        [LocalizedDisplayName("RegistrationAdd_LovzType")]
        public string LovzType { get; set; }

        [LocalizedDisplayName("RegistrationAdd_LovzGroupe")]
        public int LovzGroupe { get; set; }

        [LocalizedDisplayName("RegistrationAdd_Note")]
        public string Note { get; set; }
    }
}

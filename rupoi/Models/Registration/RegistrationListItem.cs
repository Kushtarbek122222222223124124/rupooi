﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace rupoi.Models.Registration
{
    public class RegistrationListItem
    {
        public int Id { get; set; }
        public string Inn { get; set; }
        public string SurName { get; set; }
        public string Name { get; set; }
        public string ParentName { get; set; }
        public string Sex { get; set; }
        public string Document { get; set; }
    }
}

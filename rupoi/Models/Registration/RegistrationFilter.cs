﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace rupoi.Models.Registration
{
    public class RegistrationFilter
    {
        public string Inn { get; set; }
        public string SurName { get; set; }
        public string Name { get; set; }
        public string ParentName { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace rupoi.Data
{
    public class DirectToService
    {
        [Key]
        public int Id { get; set; }

        [ForeignKey("CartId")]
        public Cart Cart { get; set; }

        [Required]
        public DateTime DirectDate { get; set; }

        [StringLength(500)]
        public string DirectDiagnosis { get; set; }

        [StringLength(200)]
        public string Organization { get; set; }

        [StringLength(200)]
        public string Doctor { get; set; }

        [StringLength(100)]
        public string ServiceType { get; set; }
    }
}

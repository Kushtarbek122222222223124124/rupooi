﻿using Microsoft.EntityFrameworkCore;
using rupoi.Data.Dictionaries;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using rupoi.Data;
using rupoi.Models;

namespace rupoi.Data
{
    public class ApplicationDbContext : DbContext
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options) : base(options)
        {
            //Database.EnsureDeleted();
            Database.EnsureCreated();
        }

        public DbSet<User> Users { get; set; }
        public DbSet<DiagnosisType> DiagnosisType { get; set; }
        public DbSet<ScarTypes> ScarTypes { get; set; }

        public DbSet<Person> Persons { get; set; }
        public DbSet<ServiceDirection> ServiceDirections { get; set; }
        public DbSet<Prosthesis> Prostheses { get; set; }
        public DbSet<ProsthesisData> ProsthesisDatas { get; set; }
        public DbSet<Shoe> Shoes { get; set; }
        public DbSet<Ottobock> Ottobocks { get; set; }

        public DbSet<DicDocument> DicDocuments { get; set; }
        public DbSet<DicDocumentSeries> DicDocumentSeries { get; set; }
        public DbSet<DicRegion> DicRegions { get; set; }
        public DbSet<DicDistrict> DicDistricts { get; set; }
        public DbSet<DicVillage> DicVillages { get; set; }
        public DbSet<DicCity> DicCities { get; set; }
        public DbSet<DicDisabilityCategory> DicDisabilityCategories { get; set; }
        public DbSet<DicDisabilityGroup> DicDisabilityGroups { get; set; }
        public DbSet<DicDisabilityReason> DicDisabilityReasons { get; set; }
        public DbSet<DicServiceType> DicServiceTypes { get; set; }
        public DbSet<DicDiagnosis> DicDiagnoses { get; set; }
        public DbSet<DicService> DicServices { get; set; }
        public DbSet<DicOrderStatus> DicOrderStatuses { get; set; }
        public DbSet<DicHospitalized> DicHospitalizeds { get; set; }
        public DbSet<DicImpression> DicImpressions { get; set; }
        public DbSet<DicNameProduct> DicNameProducts { get; set; }
        public DbSet<DicProductType> DicProductTypes { get; set; }
        public DbSet<DicProsthesisSide> DicProsthesisSides { get; set; }
        public DbSet<DicScar> DicScars { get; set; }
        public DbSet<DicStumpSupport> DicStumpSupports { get; set; }
        public DbSet<DicBoneSawdust> DicBoneSawdusts { get; set; }
        public DbSet<DicStumpSkinAndTissues> DicStumpSkinAndTissues { get; set; }
        public DbSet<DicStumpMobility> DicStumpMobilities { get; set; }
        public DbSet<DicStumpShape> DicStumpShapes { get; set; }
        public DbSet<DicCodeProduct> DicCodeProducts { get; set; }
        public DbSet<DicModel> DicModels { get; set; }
        public DbSet<DicColor> DicColors { get; set; }
        public DbSet<DicDescriptionProduct> DicDescriptionProducts { get; set; }
        public DbSet<DicSoleType> DicSoleTypes { get; set; }
        public DbSet<DicMountingType> DicMountingTypes { get; set; }
        public DbSet<DicSubDiagnosis> DicSubDiagnoses { get; set; }
        public DbSet<DicSubProductType> DicSubProductTypes { get; set; }
        public DbSet<DicPersonCardStatus> DicPersonCardStatuses { get; set; }
        public DbSet<DicSubDescriptionProduct> DicSubDescriptionProducts { get; set; }
        public DbSet<DicFinishedStatus> DicFinishedStatuses { get; set; }
        public DbSet<ProsthesisProductsRow> ProsthesisProductsRows { get; set; }
        public DbSet<ProsthesisProductsTable> ProsthesisProductsTables { get; set; }
        public DbSet<ShoeOperationsRow> ShoeOperationsRows { get; set; }
        public DbSet<ShoeOperationsTable> ShoeOperationsTables { get; set; }
        public DbSet<DicShoeOperation> DicShoeOperations { get; set; }
        public DbSet<DicTypeOrder> DicTypeOrders { get; set; }
        public DbSet<MaterialsAndEquipmentRow> MaterialsAndEquipmentRows { get; set; }
        public DbSet<MaterialsAndEquipmentTable> MaterialsAndEquipmentTables { get; set; }


        //protected override void OnModelCreating(ModelBuilder modelBuilder)
        //{   
        //    modelBuilder.Entity<User>().ToTable("Users");
        //    modelBuilder.Entity<Cart>().ToTable("Cart");
        //    modelBuilder.Entity<BoneDustTypes>().ToTable("BoneDustTypes");
        //    modelBuilder.Entity<DiagnosisType>().ToTable("DiagnosisType");
        //    modelBuilder.Entity<ProductType>().ToTable("ProductType");
        //    modelBuilder.Entity<ScarTypes>().ToTable("ScarTypes");
        //    modelBuilder.Entity<SkinConditionTypes>().ToTable("SkinConditionTypes");
        //    modelBuilder.Entity<StumpForm>().ToTable("StumpForm");
        //    modelBuilder.Entity<DirectToService>().ToTable("DirectToService");
        //    modelBuilder.Entity<ProstheticsData>().ToTable("ProstheticsData");
        //}
    }
}

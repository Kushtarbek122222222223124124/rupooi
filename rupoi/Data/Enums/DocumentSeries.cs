﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace rupoi.Data.Enums
{
    public enum DocumentSeries
    {
        ID,
        AC,
        AN,
        KGZ01,
        KRX
    }
}

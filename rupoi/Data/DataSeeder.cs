using rupoi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace rupoi.Data
{
    public class DataSeeder
    {
        public static void SeedData(ApplicationDbContext context)
        {

            try
            {
                if (!context.DicTypeOrders.Any())
                {
                    context.DicTypeOrders.Add(new DicTypeOrder { NameRus = "Первичный" });
                    context.DicTypeOrders.Add(new DicTypeOrder { NameRus = "Вторичный" });
                    context.SaveChanges();
                }

                if (!context.DicShoeOperations.Any())
                {
                    context.DicShoeOperations.Add(new DicShoeOperation { NameRus = "Колодка или слепок" });
                    context.DicShoeOperations.Add(new DicShoeOperation { NameRus = "Крой модели" });
                    context.DicShoeOperations.Add(new DicShoeOperation { NameRus = "Крой верха" });
                    context.DicShoeOperations.Add(new DicShoeOperation { NameRus = "Наклейка" });
                    context.DicShoeOperations.Add(new DicShoeOperation { NameRus = "Строчка" });
                    context.DicShoeOperations.Add(new DicShoeOperation { NameRus = "Затяжка пяток" });
                    context.DicShoeOperations.Add(new DicShoeOperation { NameRus = "Затяжка носков" });
                    context.DicShoeOperations.Add(new DicShoeOperation { NameRus = "Пришивка равта" });
                    context.DicShoeOperations.Add(new DicShoeOperation { NameRus = "Простилка" });
                    context.DicShoeOperations.Add(new DicShoeOperation { NameRus = "Пришивка подошв" });
                    context.DicShoeOperations.Add(new DicShoeOperation { NameRus = "Установка каблука" });
                    context.DicShoeOperations.Add(new DicShoeOperation { NameRus = "Отделка" });
                    context.SaveChanges();
                }

                if (!context.DicSoleTypes.Any())
                {
                    context.DicSoleTypes.Add(new DicSoleType { NameRus = "полиуретановая" });
                    context.DicSoleTypes.Add(new DicSoleType { NameRus = "микропористая" });
                    context.SaveChanges();
                }

                if (!context.DicMountingTypes.Any())
                {
                    context.DicMountingTypes.Add(new DicMountingType { NameRus = "на шнуровке" });
                    context.DicMountingTypes.Add(new DicMountingType { NameRus = "на замочках" });
                    context.DicMountingTypes.Add(new DicMountingType { NameRus = "на ремешках" });
                    context.DicMountingTypes.Add(new DicMountingType { NameRus = "на резинках" });
                    context.DicMountingTypes.Add(new DicMountingType { NameRus = "с одним перекидным ремешком" });
                    context.SaveChanges();
                }

                if (!context.DicImpressions.Any())
                {
                    context.DicImpressions.Add(new DicImpression { NameRus = "Да" });
                    context.DicImpressions.Add(new DicImpression { NameRus = "Нет" });
                    context.SaveChanges();
                }

                if (!context.DicColors.Any())
                {
                    context.DicColors.Add(new DicColor { NameRus = "черный" });
                    context.DicColors.Add(new DicColor { NameRus = "красный" });
                    context.DicColors.Add(new DicColor { NameRus = "комбинированный" });
                    context.DicColors.Add(new DicColor { NameRus = "бордовый" });
                    context.SaveChanges();
                }

                if (!context.DicModels.Any())
                {
                    context.DicModels.Add(new DicModel { NameRus = "Mark II" });
                    context.SaveChanges();
                }

                if (!context.DicDocuments.Any())
                {
                    context.DicDocuments.Add(new DicDocument { NameRus = "Паспорт" });
                    context.DicDocuments.Add(new DicDocument { NameRus = "Свидетельство о рождении" });
                    context.SaveChanges();
                }

                if (!context.DicDocumentSeries.Any())
                {
                    context.DicDocumentSeries.Add(new DicDocumentSeries { NameRus = "ID" });
                    context.DicDocumentSeries.Add(new DicDocumentSeries { NameRus = "AC" });
                    context.DicDocumentSeries.Add(new DicDocumentSeries { NameRus = "AN" });
                    context.DicDocumentSeries.Add(new DicDocumentSeries { NameRus = "KGZ01" });
                    context.DicDocumentSeries.Add(new DicDocumentSeries { NameRus = "KR-X" });
                    context.SaveChanges();
                }

                if (!context.DicRegions.Any())
                {
                    context.DicRegions.Add(new DicRegion { Id = 1, NameRus = "Жалал-Абад" });
                    context.SaveChanges();
                }

                if (!context.DicDistricts.Any())
                {
                    context.DicDistricts.Add(new DicDistrict { Id = 1, RegionId = 1, NameRus = "Ала-Бука" });
                    context.SaveChanges();
                }

                if (!context.DicVillages.Any())
                {
                    context.DicVillages.Add(new DicVillage { DistrictId = 1, NameRus = "Сары-Таш" });
                    context.SaveChanges();
                }

                if (!context.DicCities.Any())
                {
                    context.DicCities.Add(new DicCity { RegionId = 1, NameRus = "Жалал-Абад" });
                    context.SaveChanges();
                }

                if (!context.DicDisabilityCategories.Any())
                {
                    context.DicDisabilityCategories.Add(new DicDisabilityCategory { NameRus = "ЛОВЗ до 18 лет" });
                    context.DicDisabilityCategories.Add(new DicDisabilityCategory { NameRus = "ЛОВЗ с детства" });
                    context.DicDisabilityCategories.Add(new DicDisabilityCategory { NameRus = "Инвалид ВОВ" });
                    context.DicDisabilityCategories.Add(new DicDisabilityCategory { NameRus = "Инвалид советской армии" });
                    context.DicDisabilityCategories.Add(new DicDisabilityCategory { NameRus = "Инвалид труда" });
                    context.SaveChanges();
                }
                if (!context.DicDisabilityGroups.Any())
                {
                    context.DicDisabilityGroups.Add(new DicDisabilityGroup { NameRus = "1 группа" });
                    context.DicDisabilityGroups.Add(new DicDisabilityGroup { NameRus = "2 группа" });
                    context.DicDisabilityGroups.Add(new DicDisabilityGroup { NameRus = "3 группа" });
                    context.SaveChanges();
                }
                if (!context.DicDisabilityReasons.Any())
                {
                    context.DicDisabilityReasons.Add(new DicDisabilityReason { NameRus = "Травма" });
                    context.DicDisabilityReasons.Add(new DicDisabilityReason { NameRus = "Врожденный" });
                    context.DicDisabilityReasons.Add(new DicDisabilityReason { NameRus = "Заболевание" });
                    context.SaveChanges();
                }
                if (!context.DicFinishedStatuses.Any())
                {
                    context.DicFinishedStatuses.Add(new DicFinishedStatus { NameRus = "выполнено" });
                    context.DicFinishedStatuses.Add(new DicFinishedStatus { NameRus = "не выполнено" });
                    context.SaveChanges();
                }

                if (!context.DicProsthesisSides.Any())
                {
                    context.DicProsthesisSides.Add(new DicProsthesisSide { NameRus = "Левый" });
                    context.DicProsthesisSides.Add(new DicProsthesisSide { NameRus = "Правый" });
                    context.SaveChanges();
                }

                if (!context.DicHospitalizeds.Any())
                {
                    context.DicHospitalizeds.Add(new DicHospitalized { NameRus = "Да" });
                    context.DicHospitalizeds.Add(new DicHospitalized { NameRus = "Нет" });
                    context.SaveChanges();
                }

                if (!context.DicOrderStatuses.Any())
                {
                    context.DicOrderStatuses.Add(new DicOrderStatus { NameRus = "Срочный" });
                    context.DicOrderStatuses.Add(new DicOrderStatus { NameRus = "Обычный" });
                    context.SaveChanges();
                }
                //if (!context.Roles.Any())
                //{
                //    IdentityRole role = new IdentityRole("Администратор");
                //    context.Roles.Add(new IdentityRole { ConcurrencyStamp = (new Guid()).ToString(), Name = "Администратор", NormalizedName = "АДМИНИСТРАТОР" });
                //    context.Roles.Add(new IdentityRole { ConcurrencyStamp = (new Guid()).ToString(), Name = "Регистратор", NormalizedName = "РЕГИСТРАТОР" });
                //    context.Roles.Add(new IdentityRole { ConcurrencyStamp = (new Guid()).ToString(), Name = "Заведующий", NormalizedName = "ЗАВЕДУЮЩИЙ" });
                //    context.Roles.Add(new IdentityRole { ConcurrencyStamp = (new Guid()).ToString(), Name = "Техник", NormalizedName = "ТЕХНИК" });
                //    context.Roles.Add(new IdentityRole { ConcurrencyStamp = (new Guid()).ToString(), Name = "Пользователь", NormalizedName = "ПОЛЬЗОВАТЕЛЬ" });
                //    context.SaveChanges();
                //}

                // нужно из файла взять
                ////регионы
                //if (!context.Region.Any())
                //{
                //    context.Region.Add(new Region() { Id = 1000001, NameRus = "город Бишкек", NameKyrg = "Бишкек шаары" });
                //    context.Region.Add(new Region() { Id = 1000002, NameRus = "город Ош", NameKyrg = "Ош шаары" });
                //    context.Region.Add(new Region() { Id = 1000003, NameRus = "	Баткенская область", NameKyrg = "Баткен облусу" });
                //    context.Region.Add(new Region() { Id = 1000004, NameRus = "	Джалал-Абадская область", NameKyrg = "Жалал-Абад облусу" });
                //    context.Region.Add(new Region() { Id = 1000005, NameRus = "Иссык-Кульская область", NameKyrg = "Ысык-Көл облусу" });
                //    context.Region.Add(new Region() { Id = 1000006, NameRus = "	Нарынская область", NameKyrg = "Нарын облусу" });
                //    context.Region.Add(new Region() { Id = 1000007, NameRus = "Ошская область", NameKyrg = "Ош облусу" });
                //    context.Region.Add(new Region() { Id = 1000008, NameRus = "Таласская область", NameKyrg = "Талас облусу" });
                //    context.Region.Add(new Region() { Id = 1000009, NameRus = "	Чуйская область", NameKyrg = "Чүй облусу" });

                //    context.SaveChanges();
                //}
                //if (!context.DictDistrict.Any())
                //{
                //    //город Бишкек
                //    context.DictDistrict.Add(new DictDistrict() { RegionId = 1000001, NameKyrg = "   ", NameRus = "Октябрьский район" });
                //    context.DictDistrict.Add(new DictDistrict() { RegionId = 1000001, NameKyrg = "   ", NameRus = "Первомайский район" });
                //    context.DictDistrict.Add(new DictDistrict() { RegionId = 1000001, NameKyrg = "   ", NameRus = "Свердловский район" });
                //    context.DictDistrict.Add(new DictDistrict() { RegionId = 1000001, NameKyrg = "   ", NameRus = "Ленинский район" });

                //    //Баткенская область
                //    context.DictDistrict.Add(new DictDistrict() { RegionId = 1000003, NameKyrg = "Баткен району", NameRus = "Баткенский район" });
                //    context.DictDistrict.Add(new DictDistrict() { RegionId = 1000003, NameKyrg = "Кадамжай району", NameRus = "Кадамжайский район" });
                //    context.DictDistrict.Add(new DictDistrict() { RegionId = 1000003, NameKyrg = "Лейлек району", NameRus = "Лейлекский район" });
                //    context.DictDistrict.Add(new DictDistrict() { RegionId = 1000003, NameKyrg = "Баткен шаары", NameRus = "город Баткен" });
                //    context.DictDistrict.Add(new DictDistrict() { RegionId = 1000003, NameKyrg = "Кызылкыя шаары", NameRus = "	город Кызыл-Кия" }); 
                //    context.DictDistrict.Add(new DictDistrict() { RegionId = 1000003, NameKyrg = "город Сулюкта", NameRus = "	Сүлүктү шаары" });

                //    //Джалал-Абадская область
                //  context.DictDistrict.Add(new DictDistrict() { RegionId = 1000004, NameKyrg = "Аксы району", NameRus = "Аксыйский район" });
                //  context.DictDistrict.Add(new DictDistrict() { RegionId = 1000004, NameKyrg = "Ала-Бука району", NameRus = "Ала-Букинский район" });
                //  context.DictDistrict.Add(new DictDistrict() { RegionId = 1000004, NameKyrg = "Базар-Коргон району", NameRus = "Базар-Коргонский район" });
                //  context.DictDistrict.Add(new DictDistrict() { RegionId = 1000004, NameKyrg = "Ноокен району", NameRus = "	Ноокенский район" });
                //  context.DictDistrict.Add(new DictDistrict() { RegionId = 1000004, NameKyrg = "Сузак району", NameRus = "	Сузакский район" });
                //  context.DictDistrict.Add(new DictDistrict() { RegionId = 1000004, NameKyrg = "Тогуз-Торо району", NameRus = "	Тогуз-Тороуский район" });
                //  context.DictDistrict.Add(new DictDistrict() { RegionId = 1000004, NameKyrg = "Токтогул району", NameRus = "Токтогульский район" });
                //  context.DictDistrict.Add(new DictDistrict() { RegionId = 1000004, NameKyrg = "Чаткал району", NameRus = "Чаткальский район" });
                //  context.DictDistrict.Add(new DictDistrict() { RegionId = 1000004, NameKyrg = "Жалал-Абад шаары", NameRus = "город Джалал-Абад" });
                //  context.DictDistrict.Add(new DictDistrict() { RegionId = 1000004, NameKyrg = "Кара-Көл шаары", NameRus = "город Кара-Куль" });
                //  context.DictDistrict.Add(new DictDistrict() { RegionId = 1000004, NameKyrg = "Майлуу-Суу шаары", NameRus = "город Майлуу-Суу" });
                //  context.DictDistrict.Add(new DictDistrict() { RegionId = 1000004, NameKyrg = "Таш-Көмүр шаары", NameRus = "город Таш-Кумыр" });

                //    //Иссык-Кульская область
                //   context.DictDistrict.Add(new DictDistrict() { RegionId = 1000005, NameKyrg = "Ак-Суу району", NameRus = "Ак-Суйский район" });
                //   context.DictDistrict.Add(new DictDistrict() { RegionId = 1000005, NameKyrg = "Жети-Өгүз району", NameRus = "Джети-Огузский район" });
                //   context.DictDistrict.Add(new DictDistrict() { RegionId = 1000005, NameKyrg = "Ысык-Көл району", NameRus = "Иссык-Кульский район" });
                //   context.DictDistrict.Add(new DictDistrict() { RegionId = 1000005, NameKyrg = "Тоң району", NameRus = "Тонский район" });
                //   context.DictDistrict.Add(new DictDistrict() { RegionId = 1000005, NameKyrg = "Түп району", NameRus = "Тюпский район" });
                //   context.DictDistrict.Add(new DictDistrict() { RegionId = 1000005, NameKyrg = "Балыкчы шаары", NameRus = "город Балыкчи" });
                //    context.DictDistrict.Add(new DictDistrict() { RegionId = 1000005, NameKyrg = "Каракол шаары", NameRus = "город Каракол" });

                //    //Нарынская область
                //   context.DictDistrict.Add(new DictDistrict() { RegionId = 1000006, NameKyrg = "Ак-Талаа району", NameRus = "Ак-Талинский район" });
                //   context.DictDistrict.Add(new DictDistrict() { RegionId = 1000006, NameKyrg = "Ат-Башы району", NameRus = "Ат-Башинский район" });
                //   context.DictDistrict.Add(new DictDistrict() { RegionId = 1000006, NameKyrg = "Жумгал району", NameRus = "Жумгальский район" });
                //   context.DictDistrict.Add(new DictDistrict() { RegionId = 1000006, NameKyrg = "Кочкор району", NameRus = "Кочкорский район" });
                //   context.DictDistrict.Add(new DictDistrict() { RegionId = 1000006, NameKyrg = "Нарын району", NameRus = "	Нарынский район" });
                //   context.DictDistrict.Add(new DictDistrict() { RegionId = 1000006, NameKyrg = "Нарын шаары", NameRus = "город Нарын" });

                //    //Ошская область
                //   context.DictDistrict.Add(new DictDistrict() { RegionId = 1000007, NameKyrg = "Алай району", NameRus = "	Алайский район" });
                //   context.DictDistrict.Add(new DictDistrict() { RegionId = 1000007, NameKyrg = "Араван району", NameRus = "Араванский район" });
                //   context.DictDistrict.Add(new DictDistrict() { RegionId = 1000007, NameKyrg = "Кара-Кулжа району", NameRus = "Кара-Кульджинский район" });
                //   context.DictDistrict.Add(new DictDistrict() { RegionId = 1000007, NameKyrg = "Кара-Суу району", NameRus = "Кара-Сууский район" });
                //   context.DictDistrict.Add(new DictDistrict() { RegionId = 1000007, NameKyrg = "Ноокат району", NameRus = "Ноокатский район" });
                //   context.DictDistrict.Add(new DictDistrict() { RegionId = 1000007, NameKyrg = " Өзгөн району", NameRus = "Узгенский район" });
                //   context.DictDistrict.Add(new DictDistrict() { RegionId = 1000007, NameKyrg = "Чоң Алай району", NameRus = "Чон-Алайский район" });

                //    //Таласская область
                //   context.DictDistrict.Add(new DictDistrict() { RegionId = 1000008, NameKyrg = "Бакай-Ата району", NameRus = "Бакай-Атинский район" });
                //   context.DictDistrict.Add(new DictDistrict() { RegionId = 1000008, NameKyrg = "Кара-Буура району", NameRus = "Кара-Бууринский район" });
                //   context.DictDistrict.Add(new DictDistrict() { RegionId = 1000008, NameKyrg = "Манас району", NameRus = "	Манасский район" });
                //   context.DictDistrict.Add(new DictDistrict() { RegionId = 1000008, NameKyrg = "Талас району", NameRus = "Таласский район" });
                //   context.DictDistrict.Add(new DictDistrict() { RegionId = 1000008, NameKyrg = "Талас шаары", NameRus = "город Талас" });

                //    //Чуйская область
                //  context.DictDistrict.Add(new DictDistrict() { RegionId = 1000009, NameKyrg = "Аламүдүн району", NameRus = "Аламудунский район" });
                //  context.DictDistrict.Add(new DictDistrict() { RegionId = 1000009, NameKyrg = "Жайыл району", NameRus = "Жайыльский район" });
                //  context.DictDistrict.Add(new DictDistrict() { RegionId = 1000009, NameKyrg = "Кемин району", NameRus = "Кеминский район" });
                //  context.DictDistrict.Add(new DictDistrict() { RegionId = 1000009, NameKyrg = "Москва району", NameRus = "Московский район" });
                //  context.DictDistrict.Add(new DictDistrict() { RegionId = 1000009, NameKyrg = "Панфилов району", NameRus = "Панфиловский район" });
                //  context.DictDistrict.Add(new DictDistrict() { RegionId = 1000009, NameKyrg = "Сокулук району", NameRus = "Сокулукский район" });
                //  context.DictDistrict.Add(new DictDistrict() { RegionId = 1000009, NameKyrg = "Чүй району", NameRus = "Чуйский район" });
                //  context.DictDistrict.Add(new DictDistrict() { RegionId = 1000009, NameKyrg = "Ысык-Ата району", NameRus = "Ысык-Атинский район" });
                //  context.DictDistrict.Add(new DictDistrict() { RegionId = 1000009, NameKyrg = "Токмок шаары", NameRus = "город Токмак" });

                //    context.SaveChanges();
                //}

                if (!context.DicServices.Any())
                {
                    context.DicServices.Add(new DicService() { Id = 1, NameRus = "Платная" });
                    context.DicServices.Add(new DicService() { Id = 2, NameRus = "Бесплатная" });
                   context.SaveChanges();
                }
                if (!context.DicServiceTypes.Any())
                {
                    context.DicServiceTypes.Add(new DicServiceType() { Id = 1, NameRus = "Заказ" });
                    context.DicServiceTypes.Add(new DicServiceType() { Id = 2, NameRus = "Ремонт" });
                    context.SaveChanges();
                }
                //Категория инвалидности 
                if (!context.DicDisabilityCategories.Any())
                {
                    context.DicDisabilityCategories.Add(new DicDisabilityCategory() { NameRus = "ЛОВЗ до 18 лет" });
                    context.DicDisabilityCategories.Add(new DicDisabilityCategory() { NameRus = "инвалид с детства" });
                    context.DicDisabilityCategories.Add(new DicDisabilityCategory() { NameRus = "инвалид по возрасту" });
                    context.DicDisabilityCategories.Add(new DicDisabilityCategory() { NameRus = "Инвалид советской армии " });
                    context.SaveChanges();
                }
                // такого не было
                ////Возрастная группа
                //if (!context.AgeGroup.Any())
                //{
                //    context.AgeGroup.Add(new AgeGroup() { NameRus = "малодетсткое", NameKyrg = "малодетсткое" });
                //    context.AgeGroup.Add(new AgeGroup() { NameRus = "детское", NameKyrg = "детское" });
                //    context.AgeGroup.Add(new AgeGroup() { NameRus = "подростковое", NameKyrg = "подростковое" });
                //    context.AgeGroup.Add(new AgeGroup() { NameRus = "взрослое", NameKyrg = "взрослое" });
                //    context.SaveChanges();
                //}

                if (!context.DicDiagnoses.Any())
                {
                    context.DicDiagnoses.Add(new DicDiagnosis() { Id = 1000003, NameRus = "ОртОбувь " });
                    context.DicDiagnoses.Add(new DicDiagnosis() { Id = 1000002, NameRus = "Протезирование (Ампутация нижних конечностей)  " });
                    context.DicDiagnoses.Add(new DicDiagnosis() { Id = 1000001, NameRus = "Протезирование (Ампутация верхних конечностей) " });
                    context.SaveChanges();
                }
                if (!context.DicSubDiagnoses.Any())
                {
                    context.DicSubDiagnoses.Add(new DicSubDiagnosis() { DicDiagnosisId = 1000003, NameRus = "ДЦП"  });
                    context.DicSubDiagnoses.Add(new DicSubDiagnosis() { DicDiagnosisId = 1000003, NameRus = "косолапость правосторонняя "});
                    context.DicSubDiagnoses.Add(new DicSubDiagnosis() { DicDiagnosisId = 1000003, NameRus = "косолапость левосторонняя" });
                    context.DicSubDiagnoses.Add(new DicSubDiagnosis() { DicDiagnosisId = 1000003, NameRus = " Укорочение левой нижней конечности" });
                    context.DicSubDiagnoses.Add(new DicSubDiagnosis() { DicDiagnosisId = 1000003, NameRus = " Укорочение правой нижней конечности" });
                    context.DicSubDiagnoses.Add(new DicSubDiagnosis() { DicDiagnosisId = 1000003, NameRus = "Деформация пальцев стоп левый" });
                    context.DicSubDiagnoses.Add(new DicSubDiagnosis() { DicDiagnosisId = 1000003, NameRus = " Деформация пальцев стоп правый" });
                    context.DicSubDiagnoses.Add(new DicSubDiagnosis() { DicDiagnosisId = 1000003, NameRus = "  Левая Слоновость" });
                    context.DicSubDiagnoses.Add(new DicSubDiagnosis() { DicDiagnosisId = 1000003, NameRus = "Правая Слоновость"});
                    context.DicSubDiagnoses.Add(new DicSubDiagnosis() { DicDiagnosisId = 1000003, NameRus = " Левый порез " });
                    context.DicSubDiagnoses.Add(new DicSubDiagnosis() { DicDiagnosisId = 1000003, NameRus = "Правый порез " });
                    context.DicSubDiagnoses.Add(new DicSubDiagnosis() { DicDiagnosisId = 1000003, NameRus = " Левая конская стопа" });
                    context.DicSubDiagnoses.Add(new DicSubDiagnosis() { DicDiagnosisId = 1000003, NameRus = " Правая конская стопа" });

                   context.DicSubDiagnoses.Add(new DicSubDiagnosis() { DicDiagnosisId = 1000002,NameRus = " левого бедра " });
                   context.DicSubDiagnoses.Add(new DicSubDiagnosis() { DicDiagnosisId = 1000002,NameRus = " правого бедра" });
                   context.DicSubDiagnoses.Add(new DicSubDiagnosis() { DicDiagnosisId = 1000002,NameRus = " левой голени " });
                   context.DicSubDiagnoses.Add(new DicSubDiagnosis() { DicDiagnosisId = 1000002,NameRus = " правой голени " });
                   context.DicSubDiagnoses.Add(new DicSubDiagnosis() { DicDiagnosisId = 1000002,NameRus = " обеих бедер " });
                   context.DicSubDiagnoses.Add(new DicSubDiagnosis() { DicDiagnosisId = 1000002,NameRus = " Обеих голеней" });
                   context.DicSubDiagnoses.Add(new DicSubDiagnosis() { DicDiagnosisId = 1000002,NameRus = " левого тазобедренного сустава" });
                   context.DicSubDiagnoses.Add(new DicSubDiagnosis() { DicDiagnosisId = 1000002,NameRus = " правого тазобедренного сустава " });
                   context.DicSubDiagnoses.Add(new DicSubDiagnosis() { DicDiagnosisId = 1000002,NameRus = " Левого По Пирогову " });
                   context.DicSubDiagnoses.Add(new DicSubDiagnosis() { DicDiagnosisId = 1000002,NameRus = " правого по Пирогову" });
                   context.DicSubDiagnoses.Add(new DicSubDiagnosis() { DicDiagnosisId = 1000002,NameRus = " Правого по Шопару" });
                   context.DicSubDiagnoses.Add(new DicSubDiagnosis() { DicDiagnosisId = 1000002,NameRus = " левого по Шопару" });
                   context.DicSubDiagnoses.Add(new DicSubDiagnosis() { DicDiagnosisId = 1000002,NameRus = " Левого по Лесфранку" });
                   context.DicSubDiagnoses.Add(new DicSubDiagnosis() { DicDiagnosisId = 1000002,NameRus = " правого по Лесфранку " });
                   context.DicSubDiagnoses.Add(new DicSubDiagnosis() { DicDiagnosisId = 1000002,NameRus = " Обеих по Шопару " });
                   context.DicSubDiagnoses.Add(new DicSubDiagnosis() { DicDiagnosisId = 1000002,NameRus = " обеих по Лесфранку " });

                    context.DicSubDiagnoses.Add(new DicSubDiagnosis() { DicDiagnosisId = 1000001, NameRus = "левого плеча" });
                    context.DicSubDiagnoses.Add(new DicSubDiagnosis() { DicDiagnosisId = 1000001, NameRus = "правого плеча" });
                    context.DicSubDiagnoses.Add(new DicSubDiagnosis() { DicDiagnosisId = 1000001, NameRus = "обеих плеч" });
                    context.DicSubDiagnoses.Add(new DicSubDiagnosis() { DicDiagnosisId = 1000001, NameRus = "левого предплечия" });
                    context.DicSubDiagnoses.Add(new DicSubDiagnosis() { DicDiagnosisId = 1000001, NameRus = "правого предплечья" });
                    context.DicSubDiagnoses.Add(new DicSubDiagnosis() { DicDiagnosisId = 1000001, NameRus = "обеих предплечий" });
                    context.DicSubDiagnoses.Add(new DicSubDiagnosis() { DicDiagnosisId = 1000001, NameRus = "левой кисти" });
                    context.DicSubDiagnoses.Add(new DicSubDiagnosis() { DicDiagnosisId = 1000001, NameRus = "правой кисти" });
                    context.DicSubDiagnoses.Add(new DicSubDiagnosis() { DicDiagnosisId = 1000001, NameRus = "обеих кистей" });
                    context.SaveChanges();
                }

                if (!context.DicProductTypes.Any())
                {
                    context.DicProductTypes.Add(new DicProductType() { Id = 1000004, NameRus = "Протезный" });
                    context.DicProductTypes.Add(new DicProductType() { Id = 1000005, NameRus = "Обувной" });
                    context.DicProductTypes.Add(new DicProductType() { Id = 1000006, NameRus = "ОттоБок" });
                    context.SaveChanges();
                }

                if (!context.DicSubProductTypes.Any())
                {
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000004,Code= "ПРО - 03",  NameRus = "  протез кисти с манжеткой"});
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000004,Code= "ПРО - 12",  NameRus = "     протезы после ампутации кисти в пределах пястья хлорвиниловый" });
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000004,Code= "ПРО - 12",  NameRus = "   протезы после ампутации кисти в пределах пястья из пластмассы"});
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000004,Code= "ПР 2 - 09", NameRus = "  протез предплечья на короткую культю"  });
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000004,Code= "ПР 2 - 12", NameRus = "  протез предплечья рабочий" });
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000004,Code= "ПР 2 - 17", NameRus = "   протез предплечья пластмассовый с кожаной манжеткой на плечо"});
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000004,Code= "ПР 2 - 18 ",NameRus = "  протез предплечья косметический крепление уздечкой"});
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000004,Code= "ПР 2 - 18", NameRus = "   протез предплечья с кожаной манжеткой на плечо" });
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000004,Code= "ПР 2 - 30", NameRus = " протез предплечья с тяговым управлением и мышечной ротацией крепление уздечкой"});
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000004,Code= "ПР 2 - 33", NameRus = " протез предплечья для детей до 6 лет"});
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000004,Code= "ПР 4 - 16", NameRus = "  протез плеча рабочий"});
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000004,Code= "ПР 4 - 22", NameRus = "  протез плеча пластмассовый" });
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000004,Code= "ПР 4 - 23", NameRus = "  протез плеча косметический" });
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000004,Code= "ПР 4 - 25", NameRus = "     протез плеча на длинную культю пластмассовый" });
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000004,Code= "ПР 4 - 28", NameRus = "     протез плеча рабочий" });
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000004,Code= "ПР 4 - 34", NameRus = "    протез плеча функционально - косметический с кожаной гильзой плеча"});
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000004,Code= "ПР 4 - 34", NameRus = "    протез плеча функционально - косметический с пластмассовой гильзой плеча" });
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000004,Code= "ПР 4 - 34", NameRus = "     протез плеча функционально - косметический с ламинированной гильзой плеча" });
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000004,Code= "ПР 8 - 02", NameRus = "    протез после вычленения плеча косметический" });
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000004,Code= "ПР 8 - 04", NameRus = "    протез после вычленения плеча, лопатки, и ключицы косметический"});
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000004,Code= "ПР 8 - 06", NameRus = "   протез после вычленения плеча  пластмассовый с полиэтиленовой гильзой" });
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000004,Code= "ПР 8 - 06", NameRus = "   протез после вычленения плеча пластмассовый с нитролаковой гильзой" });
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000004,Code= "ПР 8 - 07", NameRus = "    протез после вычленения плеча для детей до 6 лет"});
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000004,Code= "ПН 3 - 01", NameRus = "    протез голени шинно - кожаный(культя по Пирогову) без манжетки" });
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000004,Code= "ПН 3 - 01", NameRus = "    протез голени шинно - кожаный(культя по Пирогову) с манжеткой"});
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000004,Code= "ПН 3 - 05", NameRus = "    протез голени деревянный(культя по Пирогову)" });
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000004,Code= "ПН 3 - 12", NameRus = "    протез голени шинно - кожаный"});
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000004,Code= "ПН 3 - 13", NameRus = "    протез голени шинно - кожаный с сидением" });
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000004,Code= "ПН 3 - 14", NameRus = "     протез голени шинно - кожаный"});
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000004,Code= "ПН 3 - 22", NameRus = "     протез голени на согнутое колено на 23П узел" });
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000004,Code= "ПН 3 - 22", NameRus = "    протез голени на согнутое колено шинно-кожаный" });
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000004,Code= "ПН 3 - 27", NameRus = "    протез голени на согнутое колено без стопы шинно-кожаный"});
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000004,Code= "ПН 3 - 41", NameRus = "   протез голени с глубокой посадкой деревянный без гильзы бедра"});
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000004,Code= "ПН 3 - 41", NameRus = "   протез голени деревянный с кожаной гильзой бедра"});
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000004,Code= "ПН 3 - 42", NameRus = "     протез голени с глубокой посадкой и эластичной облицовкой с деревянным приемником на 23 П узел" });
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000004,Code= "ПН 3 - 42", NameRus = "    протез голени с глубокой посадкой с кожаной гильзой бедра и деревянным вкладышем на 23 П узел"});
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000004,Code= "ПН 6 - 12", NameRus = "    протез подставка после двухсторонней ампутации бедра с деревянным приемником" });
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000004,Code= "ПН 6 - 12", NameRus = "    протез подставка после двухсторонней ампутации бедра с кожаным приемником" });
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000004,Code= "ПН 6 - 20", NameRus = "  протез бедра без стопы с кожаным приемником" });
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000004,Code= "ПН 6 - 20", NameRus = "   протез бедра  без стопы с деревянным приемником"});
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000004,Code= "ПН 6 - 35", NameRus = "     протез бедра универсального назначения с деревянным приемником" });
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000004,Code= "ПН 6 - 35", NameRus = "    протез бедра универсального назначения немецкий с деревянным приемником" });
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000004,Code= "ПН 6 – 36", NameRus = "     протез бедра на опорную культю с эластичной облицовкой" });
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000004,Code= "ПН 6 – 36 - 1", NameRus = "   протез бедра на опорную культю с эластичной облицовкой  на 23 узел" });
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000004,Code= "АН 8 - 01", NameRus = "    аппарат на всю ногу шинно - кожаный"});
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000004,Code= "АН 8 – 01Д", NameRus = "  аппарат на всю ногу шинно-кожаный детский"});
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000004,Code= "АН 8 - 01", NameRus = "    аппарат на всю ногу из полиэтилена"});
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000004,Code= "АН 8 - 04", NameRus = "     аппарат на всю ногу"});
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000004,Code= "АН 8 - 06", NameRus = "     аппарат на всю ногу с полукорсетом"});
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000004,Code= "АН 8 - 07", NameRus = "     аппарат на всю ногу с двойным следом"});
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000004,Code= "АН 8 – 07Д", NameRus = "   аппарат на всю ногу с двойным следом детский"});
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000004,Code= "ТРО - 03", NameRus = "    Тутор на лучезапястный сустав из полевика(кистедержатель)"});
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000004,Code= "ТРО - 02", NameRus = "   Тутор на лучезапястный сустав из полиэтилена(кистедержатель)" });
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000004,Code= "ТРО - 02Д", NameRus = "  Тутор на лучезапястный сустав из полиэтилена детский" });
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000004,Code= "ТР2 - 03", NameRus = "   Тутор на предплечье из полевика" });
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000004,Code= "ТР2 - 03Д", NameRus = "  Тутор на предплечье из полевика детский" });
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000004,Code= "ТНО - 03", NameRus = "     тутор на голеностопный сустав из полевика"});
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000004,Code= "ТНО – 03Д", NameRus = "  тутор на голеностопный сустав из полевика детский" });
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000004,Code= "ТНО - 02", NameRus = "      тутор на голеностопный сустав из полиэтилена" });
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000004,Code= "ТНО – 02Д", NameRus = "  тутор на голеностопный сустав из полиэтилена детский" });
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000004,Code= "ТН 2 - 03", NameRus = "     тутор на голень косметический из полевика"});
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000004,Code= "ТН 2 – 03Д", NameRus = "   тутор на голень косметический из полевика детский" });
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000004,Code= "ТН 2 - 04", NameRus = "   тутор на голень косметический из полиэтилена"});
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000004,Code= "ТН 4 - 02 - 01", NameRus = "      тутор из полиэтилена с обтяжкой педилином и креплением ремешками" });
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000004,Code= "ТН 4 - 02 - 01Д", NameRus = "   тутор из полиэтилена с обтяжкой педилином и креплением ремешками детский" });
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000004,Code= "ТН 4 - 02", NameRus = "    тутор на коленный сустав с захватом голени и бедра из полиэтилена"});
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000004,Code= "ТН4 – 02 Д", NameRus = "   тутор на коленный сустав с захватом голени и бедра из полиэтилена детский" });
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000004,Code= "ТН4 - 03Д", NameRus = "   тутор на коленный сустав из полевика детский"});
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000004,Code= "ТН 6 - 02", NameRus = "     тутор на тазобедренный сустав из полиэтилена" });
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000004,Code= "ТН 8 - 02", NameRus = "     тутор на всю ногу ш / к"});
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000004,Code= "ТН 8 – 02Д", NameRus = "   тутор на всю ногу ш/ к детский" });
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000004,Code= "ТН 8 - 02", NameRus = "     тутор на всю ногу из " });
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000004,Code= "ТН 8 – 02Д", NameRus = "   тутор на всю ногу из полиэтилена детский"});
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000004,Code= "ТН 8 – 02Д", NameRus = "   тутор на всю ногу из полиэтилена с обтяжкой из ортофома детский"});
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000004,Code= "ТН 8 - 04", NameRus = "     тутор на всю ногу с полукорсетом ш / к" });
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000004,Code= "КРО - 02", NameRus = "   Корсет головодержатель из полиэтилена"});
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000004,Code= "КРО - 02 Д", NameRus = "   Корсет головодержатель из полиэтилена детский"});
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000004,Code= "КРО - 14", NameRus = "     Корсет текстильный(Ленинградского типа)"});
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000004,Code= "КРО – 14Д", NameRus = "  Корсет текстильный(Ленинградского     типа) детский" });
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000004,Code= "КРО - 19", NameRus = "      Корсет шинно-кожаный на поясничный отдел позвоночника"});
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000004,Code= "КРО - 24", NameRus = "     Корсет на нижне - грудной отдел позвоночника из полиэтилена" });
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000004,Code= "КРО – 24Д", NameRus = "   Корсет на нижне-грудной отдел позвоночника из полиэтилена детский"});
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000004,Code= "КРО - 25", NameRus = "     Корсет шинно-кожаный на средне-грудной отдел позвоночника" });
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000004,Code= "КР 1 - 01", NameRus = "     Реклинатор - текстильный" });
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000004,Code= "КР 1 – 01Д", NameRus = "   Реклинатор -текстильный детский"});
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000004,Code= "ПИО - 04", NameRus = "      Фрейка(ЦИТО)"});
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000004,Code= "ПИО - 05", NameRus = "     шина Виленского" });
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000004,Code= "ПИО - 06", NameRus = "     шина Жумабекова" });
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000004,Code= "БН3 - 01,   БН3 - 02", NameRus = "    бандаж при опущений и заболевании почек, послеоперационный, при грыжах и опущении органов брюшной полости, при искуственном анусе, мужской, женский"});
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000004,Code= "БН 2 - 01", NameRus = "     бандаж дородовой и послеродовой" });
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000004,Code= "БН 3 - 01", NameRus = "     бандаж при опущении и заболевании почек, послеоперационный (Российская ткань)" });
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000004,Code= "БН 3 - 21", NameRus = "    Бандаж - полукорсет, женский"});
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000004,Code= "ПИ - 01", NameRus = "  суспензорий на узком поясе" });
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000004,Code= "ПИ - 02", NameRus = "  суспензорий на широком поясе" });
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000004,Code="    ", NameRus = "     Обтуратор"});
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000004,Code="    ", NameRus = "     вытяжной пояс -жилет" });
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000004,Code="    ", NameRus = "     Петля Глиссона" });
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000004,Code="    ", NameRus = " костыли" });
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000004,Code="    ", NameRus = "    трость металлическая" });
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000004,Code="    ", NameRus = "   малогаборитные инвалидные коляски" });
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000004,Code="    ", NameRus = "    Индивидуальная малог инв коляска"});
                  
                    context.SaveChanges();
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000006, Code = "", NameRus = " модульный протез голени" });
                   context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000006, Code = "", NameRus = " модульный протез голени детский" });
                   context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000006, Code = "", NameRus = " модульный протез  голени(смена приемной гильзы)" });
                   context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000006, Code = "", NameRus = " модульный протез  голени c лайнером" });
                   context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000006, Code = "", NameRus = " тонкостенный протез голени" });
                   context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000006, Code = "", NameRus = " тонкостенный протез голени детский" });
                   context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000006, Code = "", NameRus = " Модульный протез бедра" });
                   context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000006, Code = "", NameRus = " Модульный протез бедра детский" });
                   context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000006, Code = "", NameRus = " Модульный протез бедра(с лайнером)" });
                   context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000006, Code = "", NameRus = " Модульный протез бедра смена приемной гильзы(с лайнером)" });
                   context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000006, Code = "", NameRus = " Модульный протез бедра(смена приемной гильзы)" });
                   context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000006, Code = "", NameRus = " Модульный протез бедра(смена приемной гильзы) детский" });
                   context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000006, Code = "", NameRus = " протез бедра тип 4  на вычленение" });
                   context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000006, Code = "", NameRus = " протез бедра тип 4  на вычленение(смена приемной гильзы)" });
                   context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000006, Code = "", NameRus = " Фикс аппарат" });
                   context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000006, Code = "", NameRus = " Вкладной башмак по Шопару" });
                   context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000006, Code = "", NameRus = " Вкладной башмак по Шопару  детский" });
                   context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000006, Code = "", NameRus = "Тутор" });
                   context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000006, Code = "", NameRus = " Тутор детский" });
                   context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000006, Code = "", NameRus = " Головодержатель" });
                   context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000006, Code = "", NameRus = " Головодержатель детский" });
                   context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000006, Code = "", NameRus = " корсет" });
                   context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000006, Code = "", NameRus = " Корсет коррегирующий" });
                   context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000006, Code = "", NameRus = " Стельки" });
                   context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000006, Code = "", NameRus = " Стельки детский" });
                    context.SaveChanges();
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000005, Code = "09 ПСНМ", NameRus = "Полусапоги на протез мужские" });
                   context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000005, Code = "09 ПСНЖ", NameRus = "Полусапоги на протез женские" });
                   context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000005, Code = "09 ПСН2-1М", NameRus = "Полусапоги мужские с супинатором или пронатором или невысокой боковой поддержкой,  выкладкой свода, углублениями в межстелечном слое в местах омозолинности выносом каблука при плоскостопии 3-й степени" });
                   context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000005, Code = "09 ПСН2-1Ж", NameRus = "Полусапоги женские с супинатором или пронатором или невысокой боковой поддержкой,  выкладкой свода, углублениями в межстелечном слое в местах омозолинности выносом каблука при плоскостопии 3-й степени" });
                   context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000005, Code = "09 ПСН2-2М", NameRus = "Полусапоги при укорочении стопы до 6 см мужские" });
                   context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000005, Code = "09 ПСН2-2Ж", NameRus = "Полусапоги при укорочении стопы до 6 см женские" });
                   context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000005, Code = "02- К1Мм", NameRus = "Ботинки на протез мужские на меху" });
                   context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000005, Code = "02- К1Мт", NameRus = "Ботинки на протез мужские на текстиле" });
                   context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000005, Code = "02- К1Мк", NameRus = "Ботинки на протез мужские на кожзаменителе" });
                   context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000005, Code = "02- К1Жм", NameRus = "Ботинки на протез женские на меху" });
                   context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000005, Code = "  02 - К1Жт ", NameRus = "   Ботинки на протез женские на текстиле" });
                   context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000005, Code = " 02 - К1Жк", NameRus = "    Ботинки на протез женские на кожзаменителе" });
                   context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000005, Code = " 02 - К 1 МДм", NameRus = " Ботинки на протез малодетские на меху" });
                   context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000005, Code = "02 - К 1 МДо", NameRus = " Ботинки на протез малодетские на овчине" });
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000005, Code = "02 - К 1  Дм ", NameRus = "Ботинки на протез детские на меху" });
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000005, Code = "02 - К 1  ", NameRus = "До Ботинки на протез детские на овчине"});
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000005, Code = "02 - К 1  Пм", NameRus = " Ботинки на протез подростковые на меху"});
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000005, Code = "02 - К 1  По", NameRus = " Ботинки на протез подростковые на овчине"});
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000005, Code = "02 - К3Мм", NameRus = "     Ботинки с супинатором или пронатором или невысокой боковой поддержкой, разгружающие при не опорной стопе, увеличенные размеры, с двойным следом мужские на меху"});
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000005, Code = "02 - К3Мт ", NameRus = "    Ботинки с супинатором или пронатором или невысокой боковой поддержкой, разгружающие при не опорной стопе, увеличенные размеры, с двойным следом мужские на текстиле"});
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000005, Code = "02 - К3Мк ", NameRus = "    Ботинки с супинатором или пронатором или невысокой боковой поддержкой, разгружающие при не опорной стопе, увеличенные размеры, с двойным следом мужские на кожзаменителе"});
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000005, Code = "02 - К3Жм", NameRus = "     Ботинки с супинатором или пронатором или невысокой боковой поддержкой, разгружающие при не опорной стопе, увеличенные размеры, с двойным следом женские на меху"});
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000005, Code = "02 - К3Жт ", NameRus = "    Ботинки с супинатором или пронатором или невысокой боковой поддержкой, разгружающие при не опорной стопе, увеличенные размеры, с двойным следом женские на текстиле"});
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000005, Code = "02 - К3Жк", NameRus = "     Ботинки с супинатором или пронатором или невысокой боковой поддержкой, разгружающие при не опорной стопе, увеличенные размеры, с двойным следом женские на кожзаменителе"});
                     context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000005, Code = "02 - К4Мм ", NameRus = "    Ботинки на ортопедический аппарат мужские на меху"});
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000005, Code = "02 - К4Мт  ", NameRus = "   Ботинки на ортопедический аппарат мужские на текстиле"});
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000005, Code = "02 - К4Мк  ", NameRus = "   Ботинки на ортопедический аппарат мужские на кожзаменителе"});
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000005, Code = "02 - КЖм", NameRus = "  Ботинки на ортопедический аппарат женские на меху"});
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000005, Code = "02 - К4Жт ", NameRus = "    Ботинки на ортопедический аппарат женские текстиле"});
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000005, Code = "02 - К4Жк ", NameRus = "    Ботинки на ортопедический аппарат женские на кожзаменителе"});
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000005, Code = "02 - К 4 МДм", NameRus = "  Ботинки на ортопедический аппарат малодетские на меху"});
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000005, Code = "02 - К 4 МДо", NameRus = "  Ботинки на ортопедический аппарат малодетские на овчине"});
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000005, Code = "02 - К 4  Дм", NameRus = "  Ботинки на ортопедический аппарат детские на меху"});
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000005, Code = "02 - К 4 До ", NameRus = " Ботинки на ортопедический аппарат детские на овчине"});
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000005, Code = "02 - К 4 Пм ", NameRus = " Ботинки на ортопедический аппарат подростковые на меху"});
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000005, Code = "02 - К 4 По", NameRus = "  Ботинки на ортопедический аппарат подростковые на овчине"});
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000005, Code = "02 - К6Мм  ", NameRus = "   Ботинки при укрочении стопы от 1 до 3 см мужские на меху"});
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000005, Code = "02 - К6Мт   ", NameRus = "  Ботинки при укрочении стопы от 1 до 3 см мужские на текстиле"});
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000005, Code = "02 - К6Мк  ", NameRus = "   Ботинки при укрочении стопы от 1 до 3 см мужские на кожзаменителе"});
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000005, Code = "02 - К6Жм   ", NameRus = "  Ботинки при укрочении стопы от 1 до 3 см женские на меху"});
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000005, Code = "02 - К6Жт  ", NameRus = "   Ботинки при укрочении стопы от 1 до 3 см женские текстиле"});
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000005, Code = "02 - К6Жк ", NameRus = "    Ботинки при укрочении стопы от 1 до 3 см женские  на кожзаменителе"});
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000005, Code = "02 - К 6 МДм ", NameRus = " Ботинки при укорочении стопы от 1 до 3 см малодетские на меху"});
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000005, Code = "02 - К 6 МДо ", NameRus = " Ботинки при укорочении стопы от 1 до 3 см малодетские на овчине"});
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000005, Code = "02 - К 6 Дм", NameRus = "  Ботинки при укорочении стопы от 1 до 3 см детские на меху"});
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000005, Code = "02 - К 6 До", NameRus = "  Ботинки при укорочении стопы от 1 до 3 см детские на овчине"});
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000005, Code = "02 - К 6  Пм ", NameRus = " Ботинки при укорочении стопы от 1 до 3 см подростковые на меху"});
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000005, Code = "02 - К 6  По ", NameRus = " Ботинки при укорочении стопы от 1 до 3 см подростковые на овчине"});
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000005, Code = "02 - К7Мм  ", NameRus = "   Ботинки при укрочениии стопы от 3 до 6 см мужские на меху"});
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000005, Code = "02 - К7Мт ", NameRus = "    Ботинки при укрочениии стопы от 3 до 6 см мужские на текстиле"});
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000005, Code = "02 - К7Мк ", NameRus = "    Ботинки при укрочениии стопы от 3 до 6 см мужские на кожзаменителе"});
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000005, Code = "02 - К7Жм", NameRus = " Ботинки при укрочениии стопы от 3 до 6 см женские на меху"});
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000005, Code = "02 - К7Жт", NameRus = " Ботинки при укрочениии стопы от 3 до 6 см женские на текстиле"});
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000005, Code = "02 - К7Жк ", NameRus = "    Ботинки при укрочениии стопы от 3 до 6 см женские на кожзаменителе"});
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000005, Code = "02 - К 7 МДм", NameRus = "  Ботинки при укорочении стопы от 4 до 6 см малодетские на меху"});
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000005, Code = "02 - К 7 МДо", NameRus = "  Ботинки при укрочениии стопы от 3 до 6 см малодетские на овчине"});
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000005, Code = "02 - К7Дм  ", NameRus = "   Ботинки при укрочениии стопы от 3 до 6 см детские  на меху"});
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000005, Code = "02 - К7До  ", NameRus = "   Ботинки при укрочениии стопы от 3 до 6 см детские  на овчине"});
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000005, Code = "02 - К7Пм  ", NameRus = "   Ботинки при укрочениии стопы от 3 до 6 см подростковые на меху"});
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000005, Code = "02 - К7По ", NameRus = "    Ботинки при укрочениии стопы от 3 до 6 см подростковые на овчине"});
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000005, Code = "02 - К8Мм ", NameRus = "    Ботинки при укрочениии стопы от 7 до 9 см мужские на меху"});
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000005, Code = "02 - К8Мт ", NameRus = "    Ботинки при укрочениии стопы от 7 до 9 см мужские на текстиле"});
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000005, Code = "02 - К8Мк  ", NameRus = "   Ботинки при укрочениии стопы от 7 до 9 см мужские на кожзаменителе"});
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000005, Code = "02 - К8Жм  ", NameRus = "   Ботинки при укрочениии стопы от 7 до 9 см женские на меху"});
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000005, Code = "02 - К8Жт  ", NameRus = "   Ботинки при укрочениии стопы от 7 до 9 см женские на текстиле"});
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000005, Code = "02 - К8Жк ", NameRus = "    Ботинки при укрочениии стопы от 7 до 9 см женские на кожзаменителе"});
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000005, Code = "02 - К9Мм ", NameRus = "    Ботинки при укорочении стопы 10 до 12 см мужские на меху"});
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000005, Code = "02 - К9Мт ", NameRus = "    Ботинки при укорочении стопы 10 до 12 см мужские на текстиле"});
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000005, Code = "02 - К9Мк  ", NameRus = "   Ботинки при укорочении стопы 10 до 12 см мужские на кожзаменителе"});
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000005, Code = "02 - К9Жм  ", NameRus = "   Ботинки при укорочении стопы 10 до 12 см женские на меху"});
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000005, Code = "02 - К9Жт ", NameRus = "    Ботинки при укорочении стопы 10 до 12 см женские на текстиле"});
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000005, Code = "02 - К9Жк  ", NameRus = "   Ботинки при укорочении стопы 10 до 12 см женские на кожзаменителе"});
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000005, Code = "02 - К10Мм ", NameRus = "   Ботинки при укорочении стопы от 13 до 15 см мужские на меху"});
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000005, Code = "02 - К10Мт ", NameRus = "   Ботинки при укорочении стопы от 13 до 15 см мужские на текстиле"});
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000005, Code = "02 - К10Мк ", NameRus = "   Ботинки при укорочении стопы от 13 до 15 см мужские на кожзаменителе"});
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000005, Code = "02 - К10Жм ", NameRus = "   Ботинки при укорочении стопы от 13 до 15 см женские на меху"});
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000005, Code = "02 - К10Жт ", NameRus = "   Ботинки при укорочении стопы от 13 до 15 см женские на текстиле"});
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000005, Code = "02 - К10Жк ", NameRus = "   Ботинки при укорочении стопы от 13 до 15 см женские на кожзаментиле"});
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000005, Code = "02 - К11Мм", NameRus = " Ботинки при укорочении стопы от 16 до 20 см мужские на меху"});
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000005, Code = "02 - К11Мт  ", NameRus = "  Ботинки при укорочении стопы от 16 до 20 см мужские на текстиле"});
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000005, Code = "02 - К11Мк ", NameRus = "   Ботинки при укорочении стопы от 16 до 20 см мужские на кожзаментиле"});
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000005, Code = "02 - К11Жм ", NameRus = "   Ботинки при укорочении стопы от 16 до 20 см женские на меху"});
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000005, Code = "02 - К11Жт ", NameRus = "   Ботинки при укорочении стопы от 16 до 20 см женские на текстиле"});
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000005, Code = "02 - К11Жк ", NameRus = "   Ботинки при укорочении стопы от 16 до 20 см женские на кожзаменителе"});
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000005, Code = "02 - К13Мм  ", NameRus = "  Ботинки на слоновую стопу мужские на меху"});
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000005, Code = "02 - К13Мт  ", NameRus = "  Ботинки на слоновую стопу мужские на текстиле"});
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000005, Code = "02 - К13Мк  ", NameRus = "  Ботинки на слоновую стопу мужские на кожзаменителе"});
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000005, Code = "02 - К13Жм  ", NameRus = "  Ботинки на слоновую стопу на меху"});
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000005, Code = "02 - К13Жт  ", NameRus = "  Ботинки на слоновую стопу на текстиле"});
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000005, Code = "02 - К13Жк  ", NameRus = "  Ботинки на слоновую стопу на кожзаменителе"});
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000005, Code = "02 - К14Мм  ", NameRus = "  Ботинки на сложную деформированную стопу(конскую, эквиноварусную, половарусную, при косолапости) мужские на меху"});
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000005, Code = "02 - К14Мт  ", NameRus = "  Ботинки на сложную деформированную стопу(конскую, эквиноварусную, половарусную, при косолапости) мужские на текстиле"});
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000005, Code = "02 - К14Мк ", NameRus = "   Ботинки на сложную деформированную стопу(конскую, эквиноварусную, половарусную, при косолапости) мужские на кожзаменителе"});
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000005, Code = "02 - К14Жм  ", NameRus = "  Ботинки на сложную деформированную стопу(конскую, эквиноварусную, половарусную, при косолапости) женские на меху"});
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000005, Code = "02 - К14Жт  ", NameRus = "  Ботинки на сложную деформированную стопу(конскую, эквиноварусную, половарусную, при косолапости) женские на текстиле"});
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000005, Code = "02 - К14Жк  ", NameRus = "  Ботинки на сложную деформированную стопу(конскую, эквиноварусную, половарусную, при косолапости) женские на кожзаменителе"});
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000005, Code = "02 - К15Мм  ", NameRus = "  Ботинки после ампутации ноги по Пирогову мужские на меху"});
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000005, Code = "02 - К15Мт  ", NameRus = "  Ботинки после ампутации ноги по Пирогову мужские на текстиле"});
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000005, Code = "02 - К15Мк  ", NameRus = "  Ботинки после ампутации ноги по Пирогову мужские на кожзаменителе"});
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000005, Code = "02 - К15Жм  ", NameRus = "  Ботинки после ампутации ноги по Пирогову женские на меху"});
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000005, Code = "02 - К15Жт  ", NameRus = "  Ботинки после ампутации ноги по Пирогову женские на текстиле"});
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000005, Code = "02 - К15Жк", NameRus = " Ботинки после ампутации ноги по Пирогову женские на кожзаменителе"});
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000005, Code = "02 - К16Мм  ", NameRus = "  Ботинки после ампутации стопы по Шопару, с жестким передом при не опорной стопе мужские на меху"});
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000005, Code = "02 - К16Мт  ", NameRus = "  Ботинки после ампутации стопы по Шопару, с жестким передом при не опорной стопе мужские на текстиле"});
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000005, Code = "02 - К16Мк  ", NameRus = "  Ботинки после ампутации стопы по Шопару, с жестким передом при не опорной стопе мужские на кожзаменителе"});
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000005, Code = "02 - К16Жм  ", NameRus = "  Ботинки после ампутации стопы по Шопару, с жестким передом при не опорной стопе женские на меху"});
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000005, Code = "02 - К16Жт ", NameRus = "   Ботинки после ампутации стопы по Шопару, с жестким передом при не опорной стопе женские на текстиле"});
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000005, Code = "02 - К16Жк  ", NameRus = "  Ботинки после ампутации стопы по Шопару, с жестким передом при не опорной стопе женские на кожзаменителе"});
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000005, Code = "02 - К 16 МДм", NameRus = "  Ботинки после ампутации по Шопару малодетские на меху"});
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000005, Code = "02 - К 16 МДо", NameRus = "  Ботинки после ампутации по Шопару малодетские на овчине"});
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000005, Code = "02 - К 16  Дм ", NameRus = " Ботинки после ампутации по Шопару детские на меху"});
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000005, Code = "02 - К 16  До", NameRus = "  Ботинки после ампутации по Шопару детские на овчине"});
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000005, Code = "02 - К 16 Пм", NameRus = "  Ботинки после ампутации по Шопару подростковые на меху"});
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000005, Code = "02 - К 16 По ", NameRus = " Ботинки после ампутации по Шопару подростковые на овчине"});
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000005, Code = "02 - К17Мм ", NameRus = "   Ботинки на стопу после ампутации по Шопару или Лисфранку, или при разной длине стопы мужские на меху"});
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000005, Code = "02 - К17Мт  ", NameRus = "  Ботинки на стопу после ампутации по Шопару или Лисфранку, или при разной длине стопы мужские на текстиле"});
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000005, Code = "02 - К17Мк ", NameRus = "   Ботинки на стопу после ампутации по Шопару или Лисфранку, или при разной длине стопы мужские на кожзаменителе"});
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000005, Code = "02 - К17Жм ", NameRus = "   Ботинки на стопу после ампутации по Шопару или Лисфранку, или при разной длине стопы женские на меху"});
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000005, Code = "02 - К17Жт ", NameRus = "   Ботинки на стопу после ампутации по Шопару или Лисфранку, или при разной длине стопы женские на текстиле"});
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000005, Code = "02 - К17Жк  ", NameRus = "  Ботинки на стопу после ампутации по Шопару или Лисфранку, или при разной длине стопы женские на кожзаменителе"});
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000005, Code = "02 - К 17МДм", NameRus = "  Ботинки после ампутации по Лисфранку или при разной длине стопы малодетские на меху"});
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000005, Code = "02 - К 17МДо ", NameRus = " Ботинки после ампутации по Лисфранку или при разной длине стопы малодетские на овчине"});
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000005, Code = "02 - К 17Дм ", NameRus = " Ботинки после ампутации по Лисфранку или при разной длине стопы детские на меху"});
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000005, Code = "02 - К 17До ", NameRus = " Ботинки после ампутации по Лисфранку или при разной длине стопы детские на овчине"});
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000005, Code = "02 - К 17Пм ", NameRus = " Ботинки после ампутации по Лисфранку или при разной длине стопы подростковые на меху"});
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000005, Code = "02 - К 17По", NameRus = "  Ботинки после ампутации по Лисфранку или при разной длине стопы подростковые на овчине"});
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000005, Code = "02 - К18Мм ", NameRus = "   Ботинки с корсетом или полукорсетом или с высокой боковой поддержкой или с жестким берцем мужские на меху"});
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000005, Code = "02 - К18Мт ", NameRus = "   Ботинки с корсетом или полукорсетом или с высокой боковой поддержкой или с жестким берцем мужские на текстиле"});
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000005, Code = "02 - К18Мк ", NameRus = "   Ботинки с корсетом или полукорсетом или с высокой боковой поддержкой или с жестким берцем мужские на кожзаменителе"});
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000005, Code = "02 - К18Жм  ", NameRus = "  Ботинки с корсетом или полукорсетом или с высокой боковой поддержкой или с жестким берцем женские на меху"});
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000005, Code = "02 - К18Жт ", NameRus = "   Ботинки с корсетом или полукорсетом или с высокой боковой поддержкой или с жестким берцем женские на текстиле"});
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000005, Code = "02 - К18Жк ", NameRus = "   Ботинки с корсетом или полукорсетом или с высокой боковой поддержкой или с жестким берцем женские на кожзаменителе"});
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000005, Code = "02 - К21М  ", NameRus = "   Вставной башмачок в обувь(нормальную) после ампутации переднего отдела стопы мужской"});
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000005, Code = "02 - К21Ж  ", NameRus = "   Вставной башмачок в обувь(нормальную) после ампутации переднего отдела стопы женский"});
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000005, Code = "02 - К 21 МД ", NameRus = " Вставной башмак после ампутации переднего отдела стопы в обувь(нормальную)  малодетский"});
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000005, Code = "02 - К 21 Д ", NameRus = " Вставной башмак после ампутации переднего отдела стопы в обувь(нормальную) детский"});
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000005, Code = "02 - К 21 П ", NameRus = " Вставной башмак после ампутации переднего отдела стопы в обувь(нормальную) подростковый"});
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000005, Code = "02 - К23 М ", NameRus = " Вставной сапожок в обувь(нормальную) немецкого типа, после ампутации стопы по Шопару мужской"});
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000005, Code = "02 - К23Ж  ", NameRus = "   Вставной сапожок в обувь(нормальную) немецкого типа, после ампутации стопы по Шопару женский"});
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000005, Code = "02 - К23 МД ", NameRus = " Вставной сапожок в обувь(нормальную) немецкого типа, после ампутации стопы по Шопару малодетский"});
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000005, Code = "02 - К23 Д ", NameRus = " Вставной сапожок в обувь(нормальную) немецкого типа, после ампутации стопы по Шопару детский"});
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000005, Code = "02 - К23П  ", NameRus = "   Вставной сапожок в обувь(нормальную) немецкого типа, после ампутации стопы по Шопару подростковый"});
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000005, Code = "02 - К24М  ", NameRus = "   Вставной башмачок с двойным следом в обувь(нормальную) мужской"});
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000005, Code = "02 - К24Ж  ", NameRus = "   Вставной башмачок с двойным следом в обувь(нормальную)  женский"});
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000005, Code = "02 - К24МД", NameRus = " Вставной башмачок с двойным следом в обувь(нормальную) малодетский"});
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000005, Code = "02 - К24Д  ", NameRus = "   Вставной башмачок с двойным следом в обувь(нормальную) детский"});
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000005, Code = "02 - К24П", NameRus = " Вставной башмачок с двойным следом в обувь(нормальную) подростковый"});
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000005, Code = "02 - К25Мм  ", NameRus = "  Ботинкис металлическими шинами после ампутации стопы по Пирогову мужской на меху"});
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000005, Code = "02 - К25Мт  ", NameRus = "  Ботинкис металлическими шинами после ампутации стопы по Пирогову мужской на текстиле"});
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000005, Code = "02 - К25Мк ", NameRus = "   Ботинкис металлическими шинами после ампутации стопы по Пирогову мужской на кожзаменителе"});
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000005, Code = "02 - К25Жм  ", NameRus = " Ботинкис металлическими шинами после ампутации стопы по Пирогову женский на меху"});
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000005, Code = "02 - К25Жт  ", NameRus = "   Ботинкис металлическими шинами после ампутации стопы по Пирогову женский натекстиле"});
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000005, Code = "02 - К25Жк ", NameRus = "    Ботинкис металлическими шинами после ампутации стопы по Пирогову женский на кожзаменитиле"});
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000005, Code = "02 - К 25 МДм", NameRus = "   Ботинки после ампутации голени по Пирогову с металлическими шинами  малодетские на меху"});
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000005, Code = "02 - К 25 МДо ", NameRus = "  Ботинки после ампутации голени по Пирогову с металлическими шинами  малодетские на овчине"});
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000005, Code = "02 - К 25  Дм ", NameRus = "  Ботинки после ампутации голени по Пирогову с металлическими шинами детские на меху"});
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000005, Code = "02 - К 25  До ", NameRus = "  Ботинки после ампутации голени по Пирогову с металлическими шинами детские на овчине"});
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000005, Code = "02 - К 25 Пм", NameRus = "   Ботинки после ампутации голени по Пирогову с металлическими шинами  подростковые на меху"});
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000005, Code = "02 - К 25 По ", NameRus = "  Ботинки после ампутации голени по Пирогову с металлическими шинами  подростковые на овчине"});
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000005, Code = "03 - К 1Мм ", NameRus = "  Полуботинки на протез мужские на меху"});
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000005, Code = "03 - К 1Мт ", NameRus = "  Полуботинки на протез мужские на текстиле"});
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000005, Code = "03 - К 1Мк", NameRus = "   Полуботинки на протез мужские на кожзаменителе"});
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000005, Code = "03 - К 1Жм ", NameRus = "  Полуботинки на протез женские на меху"});
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000005, Code = "03 - К 1Жт ", NameRus = "  Полуботинки на протез женские на текстиле"});
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000005, Code = "03 - К 1Жк", NameRus = "   Полуботинки на протез женские на кожзаменителе"});
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000005, Code = "03 - К1 МДм", NameRus = "   Полуботинки на протез малодетские на меху"});
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000005, Code = "03 - К1 МДо", NameRus = " Полуботинки на протез малодетские на овчине"});
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000005, Code = "03 - К1 Дм ", NameRus = "Полуботинки на протез детские на меху"});
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000005, Code = "03 - К1 До ", NameRus = "Полуботинки на протез детские на овчине"});
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000005, Code = "03 - К1 Пм ", NameRus = "Полуботинки на протез подростковые на меху"});
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000005, Code = "03 - К1 По ", NameRus = "Полуботинки на протез подростковые на овчине"});
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000005, Code = "03 - К1 - 1 ", NameRus = "Женские полуботинки на протез"});
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000005, Code = "03 - К 3Мм", NameRus = " Полуботинки с супинатором или пронатором, или невысокой боковой поддержкой мужские на меху"});
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000005, Code = "03 - К 3Мт", NameRus = " Полуботинки с супинатором или пронатором, или невысокой боковой поддержкой мужские на текстиле"});
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000005, Code = "03 - К 3Мк ", NameRus = "Полуботинки с супинатором или пронатором, или невысокой боковой поддержкой мужские на кожзаменителе"});
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000005, Code = "03 - К 3Жм", NameRus = " Полуботинки с супинатором или пронатором, или невысокой боковой поддержкой женские на меху"});
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000005, Code = "03 - К 3Жт", NameRus = " Полуботинки с супинатором или пронатором, или невысокой боковой поддержкой женские на текстиле"});
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000005, Code = "03 - К 3Жк", NameRus = " Полуботинки с супинатором или пронатором, или невысокой боковой поддержкой женские на кожзаменителе"});
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000005, Code = "03 - К3 МДм", NameRus = " Полуботинки с супинатором или пронатором, или невысокой боковой поддержкой малодетские на меху"});
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000005, Code = "03 - К3 МДо ", NameRus = "Полуботинки с супинатором или пронатором, или невысокой боковой поддержкой малодетские на овчине"});
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000005, Code = "03 - К3 Дм", NameRus = " Полуботинки с супинатором или пронатором, или невысокой боковой поддержкой детские на меху"});
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000005, Code = "03 - К3 До", NameRus = " Полуботинки с супинатором или пронатором, или невысокой боковой поддержкой детские на овчине"});
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000005, Code = "03 - К3 Пм", NameRus = " Полуботинки с супинатором или пронатором, или невысокой боковой поддержкой подростковые на меху"});
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000005, Code = "03 - К3 По ", NameRus = "Полуботинки с супинатором или пронатором, или невысокой боковой поддержкой подростковые на овчине"});
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000005, Code = "03 - К 3 - 1 ", NameRus = " Женские полуботинки с супинатором или пронатором"});
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000005, Code = "03 - К 4Мм ", NameRus = "Полуботинки  на фиксационный аппарат мужские на меху"});
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000005, Code = "03 - К 4Мт", NameRus = " Полуботинки  на фиксационный аппарат мужские на текстиле"});
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000005, Code = "03 - К 4Мк ", NameRus = "Полуботинки  на фиксационный аппарат мужские на кожзаменителе"});
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000005, Code = "03 - К 4Жм ", NameRus = "Полуботинки  на фиксационный аппарат женские на меху"});
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000005, Code = "04 - К 4Жт ", NameRus = "Полуботинки  на фиксационный аппарат женские на тектиле"});
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000005, Code = "05 - К 4Жк ", NameRus = "Полуботинки  на фиксационный аппарат женские на кожзаменителе"});
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000005, Code = "03 - К4 МДм ", NameRus = "Полуботинки на ортопедический аппарат малодетские на меху"});
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000005, Code = "03 - К4 МДо", NameRus = " Полуботинки на ортопедический аппарат малодетские на овчине"});
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000005, Code = "03 - К4 Дм ", NameRus = "Полуботинки на ортопедический аппарат детские меху"});
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000005, Code = "03 - К4 До ", NameRus = "Полуботинки на ортопедический аппарат детские на овчине"});
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000005, Code = "03 - К4 Пм ", NameRus = "Полуботинки на ортопедический аппарат подростковые на меху"});
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000005, Code = "03 - К4 По ", NameRus = "Полуботинки на ортопедический аппарат подростковые на овчине"});
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000005, Code = "03 - К 6 Мм ", NameRus = "Полуботинки при укорочении стопы до 3 см мужские на меху"});
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000005, Code = "03 - К 6 Мт ", NameRus = "Полуботинки при укорочении стопы до 3 см мужские на текстиле"});
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000005, Code = "03 - К 6 Мк", NameRus = " Полуботинки при укорочении стопы до 3 см мужские на кожзаменителе"});
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000005, Code = "03 - К 6Жм ", NameRus = "Полуботинки при укорочении стопы до 3 см женские на меху"});
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000005, Code = "03 - К 6Жт", NameRus = " Полуботинки при укорочении стопы до 3 см женские на текстиле"});
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000005, Code = "03 - К 6Жк ", NameRus = "Полуботинки при укорочении стопы до 3 см женские на кожзаменителе"});
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000005, Code = "03 - К6 МДм ", NameRus = "Полуботинки при укорочении стопы от 1 до 3 см малодетские на меху"});
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000005, Code = "03 - К6 МДо", NameRus = " Полуботинки при укорочении стопы от 1 до 3 см малодетские на овчине"});
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000005, Code = "03 - К6 Дм ", NameRus = "Полуботинки при укорочении стопы от 1 до 3 см детские на меху"});
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000005, Code = "03 - К6 До", NameRus = " Полуботинки при укорочении стопы от 1 до 3 см детские на овчине"});
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000005, Code = "03 - К6 Пм", NameRus = " Полуботинки при укорочении стопы от 1 до 3 см подростковые на меху"});
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000005, Code = "03 - К6 По", NameRus = " Полуботинки при укорочении стопы от 1 до 3 см подростковые на овчине"});
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000005, Code = "03 - К 6 - 1  ", NameRus = "  Женские полуботинки при укорочении стопы до 3 см"});
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000005, Code = "03 - К 7 Мм ", NameRus = "Полуботинки при укорочении стопы от 3 см и выше мужские на меху"});
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000005, Code = "03 - К 7 Мт ", NameRus = "Полуботинки при укорочении стопы от 3 см и выше мужские на текстиле"});
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000005, Code = "03 - К 7 Мк ", NameRus = "Полуботинки при укорочении стопы от 3 см и выше мужские на кожзаменителе"});
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000005, Code = "03 - К 7 Жм", NameRus = " Полуботинки при укорочении стопы от 3 см и выше женские на меху"});
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000005, Code = "03 - К 7 Жт", NameRus = " Полуботинки при укорочении стопы от 3 см и выше женские на текстиле"});
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000005, Code = "03 - К 7 Жк", NameRus = " Полуботинки при укорочении стопы от 3 см и выше женские на текстиле"});
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000005, Code = "03 - К7 МДм ", NameRus = "Полуботинки при укорочении стопы от 3 до 6 см малодетские на меху"});
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000005, Code = "03 - К7 МДо", NameRus = " Полуботинки при укорочении стопы от 3 до 6 см малодетские на овчине"});
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000005, Code = "03 - К7 Дм ", NameRus = "Полуботинки при укорочении стопы от 3 до 6 см детские на меху"});
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000005, Code = "03 - К7 До ", NameRus = "Полуботинки при укорочении стопы от 3 до 6 см детские на овчине"});
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000005, Code = "03 - К7 Пм ", NameRus = "Полуботинки при укорочении стопы от 3 до 6 см подростковые на меху"});
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000005, Code = "03 - К7 По ", NameRus = "Полуботинки при укорочении стопы от 3 до 6 см подростковые на овчине"});
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000005, Code = "03 - К 7 - 1  Женские", NameRus = " полуботинки с укорочением стопы от 3 см и выше"});
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000005, Code = "03 - К 10 Мм", NameRus = " Полуботинки после ампутации стопы по Шопару мужские на меху"});
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000005, Code = "03 - К 10 Мт ", NameRus = "Полуботинки после ампутации стопы по Шопару мужские на текстиле"});
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000005, Code = "03 - К 10 Мк ", NameRus = "Полуботинки после ампутации стопы по Шопару мужские на кожзаменителе"});
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000005, Code = "03 - К 10 Жм ", NameRus = "Полуботинки после ампутации стопы по Шопару женские на меху"});
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000005, Code = "03 - К 10 Жт ", NameRus = "Полуботинки после ампутации стопы по Шопару женские на текстиле"});
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000005, Code = "03 - К 10 Жк ", NameRus = "Полуботинки после ампутации стопы по Шопару женские на кожзаменителе"});
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000005, Code = "03 - К 11Мм", NameRus = " Полуботинки после ампутации стопы по Лисфранку при разной длине следа мужские на меху"});
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000005, Code = "03 - К 11Мт ", NameRus = "Полуботинки после ампутации стопы по Лисфранку при разной длине следа мужские на текстиле"});
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000005, Code = "03 - К 11Мк", NameRus = " Полуботинки после ампутации стопы по Лисфранку при разной длине следа мужские на кожзаменителе"});
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000005, Code = "03 - К 11 Жм", NameRus = " Полуботинки после ампутации стопы по Лисфранку при разной длине следа женские на меху"});
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000005, Code = "03 - К 11 Жт ", NameRus = "Полуботинки после ампутации стопы по Лисфранку при разной длине следа женские на текстиле"});
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000005, Code = "03 - К 11 Жк", NameRus = " Полуботинки после ампутации стопы по Лисфранку при разной длине следа женские на кожзаменителе"});
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000005, Code = "03 - К 13Мм ", NameRus = "Полуботинки на слоновую стопу мужские на меху" });
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000005, Code = "    03 - К 13Мт", NameRus = " Полуботинки на слоновую стопу мужские на текстиле"});
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000005, Code = "03 - К 13Мк", NameRus = " Полуботинки на слоновую стопу мужские на кожзаменителе"});
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000005, Code = "03 - К 13 Жм", NameRus = "Полуботинки на слоновую стопу женские на меху"});
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000005, Code = "03 - К 13 Жт ", NameRus = "Полуботинки на слоновую стопу женские на текстиле"});
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000005, Code = "03 - К 13 Жк ", NameRus = "Полуботинки на слоновую стопу женские на кожзаменителе"});
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000005, Code = "03 - К 14Мм", NameRus = "Полуботинки на сложно-деформированную стопу(конскую, эквиноварусную, половарусную, при косолапости) мужские на меху"});
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000005, Code = "03 - К 14Мт", NameRus = " Полуботинки на сложно-деформированную стопу(конскую, эквиноварусную, половарусную, при косолапости) мужские на текстиле"});
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000005, Code = "03 - К 14Мк ", NameRus = "Полуботинки на сложно-деформированную стопу(конскую, эквиноварусную, половарусную, при косолапости) мужские на кожзаменителе на кожзаменителе"});
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000005, Code = "03 - К 14Жм ", NameRus = "Полуботинки на сложно-деформированную стопу(конскую, эквиноварусную, половарусную, при косолапости) женские на меху"});
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000005, Code = "03 - К 14Жт ", NameRus = "Полуботинки на сложно-деформированную стопу(конскую, эквиноварусную, половарусную, при косолапости) женские на текстиле"});
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000005, Code = "03 - К 14Жк", NameRus = " Полуботинки на сложно-деформированную стопу(конскую, эквиноварусную, половарусную, при косолапости) женские на кожзаменителе"});
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000005, Code = "04 - К 1 Жт", NameRus = " Туфли на протез женские на текстиле"});
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000005, Code = "04 - К 1 Жк ", NameRus = "Туфли на протез женские на кожзаменителе"});
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000005, Code = "04 - К1 - 1", NameRus = " Комплект деталей верха летних туфель с открытым носком или комплект деталей верха сандалет"});
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000005, Code = "04 - К 2 Ж т", NameRus = " Туфли на протез на среднем каблуке женские на текстиле"});
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000005, Code = "04 - К 2 Ж к", NameRus = "  Туфли на протез на среднем каблуке женские на кожзаменителе"});
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000005, Code = "04 - К 3 Жт", NameRus = " Туфли на протез на высоком каблуке женские на текстиле"});
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000005, Code = "04 - К 3 Жк ", NameRus = "Туфли на протез на высоком каблуке женские на кожзаменителе"});
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000005, Code = "04 - К3 - 1", NameRus = " Туфли типа сандалет на высоком каблуке"});
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000005, Code = "04 - К 5", NameRus = "   Туфлина низком каблуке с супинатором, пронатором или невысокой боковой поддержкой"});
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000005, Code = "04 - К 6 Жт", NameRus = "Туфли на среднем каблуке с супинатором или пронатором, или невысокой боковой поддержкой женские на текстиле"});
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000005, Code = "04 - К 6 Жк", NameRus = " Туфли на среднем каблуке с супинатором или пронатором, или невысокой боковой поддержкой женские на кожзаменителе"});
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000005, Code = "04 - К 7 Жт ", NameRus = "Туфли на высоком каблуке с супинатором или пронатором, или невысокой боковой поддержкой женские на текстиле"});
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000005, Code = "04 - К 7 Жк", NameRus = " Туфли на высоком каблуке с супинатором или пронатором, или невысокой боковой поддержкой женские на кожзаменителе"});
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000005, Code = "04 - К 8 Жт", NameRus = " Туфли на низком каблуке при укорочении стопы от 1 до 3 см женские на текстиле"});
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000005, Code = "04 - К 8 Жк", NameRus = "Туфли на низком каблуке при укорочении стопы от 1 до 3 см женские на кожзаменителе"});
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000005, Code = "04 - К8 - 1,04 - К6,04 - К7, 04 - К9, 04 - К10 ", NameRus = " Туфли типа босоножки при укорочении стопы до 3 см"});
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000005, Code = "04 - К 9 Жт", NameRus = "Туфли на среднем каблуке при укорочении стопы от 1 до 3 см женские на текстиле"});
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000005, Code = "04 - К 9 Жк", NameRus = " Туфли на среднем каблуке при укорочении стопы от 1 до 3 см женские на кожзаменителе"});
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000005, Code = "04 - К 10Жт ", NameRus = "Туфли на высоком каблуке при укорочении стопы от 1 до 3 см женские на текстиле"});
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000005, Code = "04 - К 10 Жк", NameRus = "Туфли на высоком каблуке при укорочении стопы от 1 до 3 см женские на кожзаменителе"});
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000005, Code = "04 - К 11 Жт ", NameRus = "Туфли на низком каблуке при укорочении стопы от 3 см до 6 см женские на текстиле"});
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000005, Code = "04 - К 11 Жк ", NameRus = "Туфли на низком каблуке при укорочении стопы от 3 см до 6 см женские на кожзаменителе"});
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000005, Code = "04 - К11 - 1 ", NameRus = "   Ортопедические туфли типа босоножки при укорочении стопы от 4 до 6 см"});
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000005, Code = "04 - К 12 Жт", NameRus = " Туфли на среднем каблуке при укорочении стопы от 7 до 9 см женские на текстиле"});
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000005, Code = "04 - К 12 Жк", NameRus = "Туфли на среднем каблуке при укорочении стопы от 7 до 9 см женские на кожзаменителе"});
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000005, Code = "04 - К12 - 1 ", NameRus = "  Ортопедические туфли типа босоножки при укорочении стопы от 7 до 9 см"});
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000005, Code = "04 - К 13 Жт ", NameRus = "Туфли на ортопедический аппарат женские на текстиле"});
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000005, Code = "04 - К 13 Жк", NameRus = "Туфли на ортопедический аппарат женские на кожзаменителе"});
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000005, Code = "04 - К 14 Жт", NameRus = "Туфли на низком каблуке на сложно - деформированную стопу(конскую, эквиноварусную, половарусную, при косолапости) женские на текстиле"});
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000005, Code = "04 - К 14 Жк ", NameRus = "Туфли на низком каблуке на сложно - деформированную стопу(конскую, эквиноварусную, половарусную, при косолапости) женские на кожзаменителе"});
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000005, Code = "04 - К 15 Жт ", NameRus = "Туфли на среднем каблуке на сложно деформированную стопу(конскую, эквиноварусную, половарусную, при косолапости) женские на текстиле"});
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000005, Code = "04 - К 15 Жк", NameRus = "Туфли на среднем каблуке на сложно деформированную стопу(конскую, эквиноварусную, половарусную, при косолапости) женские на кожзаменителе"});
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000005, Code = "04 - К 16 Жт ", NameRus = "Туфли на низком каблуке после ампутации переднего отдела стопы по Шопару  или Лисфранку или при разной длине следа женские на текстиле"});
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000005, Code = "04 - К 16 Жк ", NameRus = "Туфли на низком каблуке после ампутации переднего отдела стопы по Шопару  или Лисфранку или при разной длине следа женские на кожзаменителе"});
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000005, Code = "04 - К 17 Жт ", NameRus = "Туфли на среднем каблуке после ампутации переднего отдела стопы по Шопару  или Лисфранку или при разной длине следа женские на текстиле"});
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000005, Code = "04 - К 17 Жк", NameRus = " Туфли на среднем каблуке после ампутации переднего отдела стопы по Шопару  или Лисфранку или при разной длине следа женские на кожзаменителе"});
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000005, Code = "04 - К 18 Жт ", NameRus = "Туфли на низком каблуке с высоким узким жестким задником женские на текстиле"});
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000005, Code = "04 - К 18 Жк", NameRus = "Туфли на низком каблуке с высоким узким жестким задником женские на кожзаменителе"});
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000005, Code = "04 - К20   ", NameRus = "Туфли на низком каблуке на слоновую стопу"});
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000005, Code = "013 - К 1 - 1", NameRus = "   Сандалеты на протез"});
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000005, Code = "013 - К 3 - 1 ", NameRus = " Сандалеты с супинатором или пронатором"});
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000005, Code = "013 - К 6 - 1 ", NameRus = " Сандалеты при укорочении стопы от 1 до 6 см"});
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000005, Code = "013 - К 6 - 2 ", NameRus = " Сандалеты при укорочении стопы от 7 до 9 см"});
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000005, Code = "014 - К 1, 014 - К 3 ", NameRus = " Тапочки без задника с супинатором или пронатором"});
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000005, Code = "014 - К 6", NameRus = "Тапочки без задника при укорочении стопы от 1 до 3 см"});
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000005, Code = "014 - К 7", NameRus = "Тапочки без задника при укорочении стопы от 3 до 6 см"});
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000005, Code = "ПИО - 02 ", NameRus = " Подколенник"});
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000005, Code = "ПИО - 01 ", NameRus = " Кожаные брюки(сиденье кожаное)"});
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000005, Code = "ПИО - 03 ", NameRus = " чулки - ползунки взрослые"});
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000005, Code = "  ", NameRus = " стельки взрослые" });
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000005, Code = "  ", NameRus = "стельки косок" });
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000005, Code = "  ", NameRus = " съемный корсет взрослый" });
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000005, Code = "АН8 - 07 ", NameRus = " Аппарат на всю ногу с двойным следом"});
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000005, Code = "02 - К 8 МД, Д" ,NameRus = "  П Ботинки при укорочении стопы от 7 до 9 см малодетские, детские, подростковые"});
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000005, Code = "02 - К 9 МД, Д", NameRus = " П Ботинки при укорочении стопы от 10 до 12 см малодетские, детские, подростковые"});
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000005, Code = "02 - К 10 МД, Д", NameRus = "  П Ботинки при укорочении стопы от 13 до 15 см малодетские, детские, подростковые"});
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000005, Code = "02 - К 12 П", NameRus = "  Ботинки  с двойным следом подростковые"});
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000005, Code = "02 - К13П ", NameRus = " Ботинки на слоновую стопу  подростковые"});
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000005, Code = "02 - К 14 МД, Д, П ", NameRus = " Ботинки на сложно-деформированную стопу(конскую, эквиноварусную, половарусную, при косолапости) малодетские, детские, подростковые"});
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000005, Code = "02 - К 15 П Ботинки ", NameRus = " после ампутации голени по Пирогову подростковые"});
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000005, Code = "02 - К 16 МД, Д, П ", NameRus = " Ботинки после ампутации по Шопару малодетские, детские, подростковые"});
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000005, Code = "02 - К 19 МД, Д, П ", NameRus = " Ботинки  с жестким задником,  задником продленным до носка, с жесткими берцами малодетские, детские, подростковые"});
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000005, Code = "013 - К1 - 1П, 013 - К3 - 1П ", NameRus = " Сандалеты подростковые"});
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000005, Code = "014 - К1П, 014 - К3П   ", NameRus = "  Комнатные тапочки с супинатором или пронатором подростковые"});
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000005, Code = "014 - К6П", NameRus = "  Комнатные тапочки при укорочении стопы до 3 см подростковые"});
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000005, Code = "014 - К7П ", NameRus = " Комнатные тапочки при укорочении стопы от 3 до 6 см подростковые"});
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000005, Code = "  ", NameRus =  "стельки детские"});
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000005, Code = "  ", NameRus = "стельки косок детский" });
                    context.DicSubProductTypes.Add(new DicSubProductType() { DicProductTypeId = 1000005, Code = "  ", NameRus = "съемный корсет детский" });

                    context.SaveChanges();
            
                }


                //при ампутации--Костный опил
                if (!context.DicBoneSawdusts.Any())
                {
                    context.DicBoneSawdusts.Add(new DicBoneSawdust() { NameRus = "болезненный" });
                    context.DicBoneSawdusts.Add(new DicBoneSawdust() { NameRus = "безболезненный" });
                    context.DicBoneSawdusts.Add(new DicBoneSawdust() { NameRus = "неровный" });
                    context.DicBoneSawdusts.Add(new DicBoneSawdust() { NameRus = "гладкий" });
                    context.DicBoneSawdusts.Add(new DicBoneSawdust() { NameRus = "остеофиты" });
                    context.SaveChanges();
                }

                if (!context.DicDescriptionProducts.Any())
                {
                    context.DicDescriptionProducts.Add(new DicDescriptionProduct() { Id = 1000007, NameRus = "Шино-кожный" });
                    context.DicDescriptionProducts.Add(new DicDescriptionProduct() { Id = 1000008, NameRus = "деревянный " });
                    context.DicDescriptionProducts.Add(new DicDescriptionProduct() { Id = 1000009, NameRus = "Голень" });
                    context.DicDescriptionProducts.Add(new DicDescriptionProduct() { Id = 1000010, NameRus = "Фиксационный аппарат" });
                    context.DicDescriptionProducts.Add(new DicDescriptionProduct() { Id = 1000011, NameRus = "Протез голени по Пирогову" });
                    context.SaveChanges();
                }

                if (!context.DicSubDescriptionProducts.Any())
                {
                    context.DicSubDescriptionProducts.Add(new DicSubDescriptionProduct() { DicDescriptionProductId = 1000007, NameRus = "Протез левого бедра, шино-кожный" });
                    context.DicSubDescriptionProducts.Add(new DicSubDescriptionProduct() { DicDescriptionProductId = 1000007, NameRus = "Узел, 16 ФЛ с замком " });
                    context.DicSubDescriptionProducts.Add(new DicSubDescriptionProduct() { DicDescriptionProductId = 1000007, NameRus = "Узел, 23 ФЛ с замком" });
                    context.DicSubDescriptionProducts.Add(new DicSubDescriptionProduct() { DicDescriptionProductId = 1000007, NameRus = "Узел 7ПЛ" });
                    context.DicSubDescriptionProducts.Add(new DicSubDescriptionProduct() { DicDescriptionProductId = 1000007, NameRus = "Узел, 16 ФЛ без замка" });
                    context.DicSubDescriptionProducts.Add(new DicSubDescriptionProduct() { DicDescriptionProductId = 1000007, NameRus = "Узел, 23 ФЛ без замка" });
                    context.DicSubDescriptionProducts.Add(new DicSubDescriptionProduct() { DicDescriptionProductId = 1000007, NameRus = "Узел 7ПЛ " });
                    context.DicSubDescriptionProducts.Add(new DicSubDescriptionProduct() { DicDescriptionProductId = 1000007, NameRus = "Креплени, гильза бедра на трех ремешках" });
                    context.DicSubDescriptionProducts.Add(new DicSubDescriptionProduct() { DicDescriptionProductId = 1000007, NameRus = "Пояс узкий" });
                    context.DicSubDescriptionProducts.Add(new DicSubDescriptionProduct() { DicDescriptionProductId = 1000007, NameRus = "Пояс широкий" });
                    context.DicSubDescriptionProducts.Add(new DicSubDescriptionProduct() { DicDescriptionProductId = 1000007, NameRus = "Кожвертлук роликовый ремень, помочь, облицовка пенополиуритана" });
                    context.DicSubDescriptionProducts.Add(new DicSubDescriptionProduct() { DicDescriptionProductId = 1000007, NameRus = "Стопа резиновая р.стопа с примеркой" });
                    context.DicSubDescriptionProducts.Add(new DicSubDescriptionProduct() { DicDescriptionProductId = 1000007, NameRus = "Стопа резиновая р.стопа без примеркой" });
                  context.DicSubDescriptionProducts.Add(new DicSubDescriptionProduct() { DicDescriptionProductId = 1000008, NameRus = "Протез левого бедра, деревянный" });
                  context.DicSubDescriptionProducts.Add(new DicSubDescriptionProduct() { DicDescriptionProductId = 1000008, NameRus = "Узел, 16 ФЛ с замком " });
                  context.DicSubDescriptionProducts.Add(new DicSubDescriptionProduct() { DicDescriptionProductId = 1000008, NameRus = "Креплени, гильза бедра на трех ремешках" });
                  context.DicSubDescriptionProducts.Add(new DicSubDescriptionProduct() { DicDescriptionProductId = 1000008, NameRus = "Пояс узкий" });
                  context.DicSubDescriptionProducts.Add(new DicSubDescriptionProduct() { DicDescriptionProductId = 1000008, NameRus = "Пояс широкий" });
                  context.DicSubDescriptionProducts.Add(new DicSubDescriptionProduct() { DicDescriptionProductId = 1000008, NameRus = "Кожвертлук роликовый ремень, помочь, облицовка пенополиуритана" });
                  context.DicSubDescriptionProducts.Add(new DicSubDescriptionProduct() { DicDescriptionProductId = 1000008, NameRus = "Стопа резиновая р.стопа с примеркой " });
                  context.DicSubDescriptionProducts.Add(new DicSubDescriptionProduct() { DicDescriptionProductId = 1000008, NameRus = "Стопа резиновая р.стопа без примеркой" });
                  context.DicSubDescriptionProducts.Add(new DicSubDescriptionProduct() { DicDescriptionProductId = 1000009, NameRus = "Протез левой голени, шино-кожный" });
                  context.DicSubDescriptionProducts.Add(new DicSubDescriptionProduct() { DicDescriptionProductId = 1000009, NameRus = "Узел, 16 ФЛ с замком " });
                  context.DicSubDescriptionProducts.Add(new DicSubDescriptionProduct() { DicDescriptionProductId = 1000009, NameRus = "Узел, 23 ФЛ с замком" });
                  context.DicSubDescriptionProducts.Add(new DicSubDescriptionProduct() { DicDescriptionProductId = 1000009, NameRus = "Крепление, гильза бедра и голени на трех ремешках" });
                  context.DicSubDescriptionProducts.Add(new DicSubDescriptionProduct() { DicDescriptionProductId = 1000009, NameRus = "Крепление, гильза бедра и голени на шнурках" });
                  context.DicSubDescriptionProducts.Add(new DicSubDescriptionProduct() { DicDescriptionProductId = 1000009, NameRus = "Пояс узкий" });
                  context.DicSubDescriptionProducts.Add(new DicSubDescriptionProduct() { DicDescriptionProductId = 1000009, NameRus = "Пояс широкий " });
                  context.DicSubDescriptionProducts.Add(new DicSubDescriptionProduct() { DicDescriptionProductId = 1000009, NameRus = "Кожвертлук " });
                  context.DicSubDescriptionProducts.Add(new DicSubDescriptionProduct() { DicDescriptionProductId = 1000009, NameRus = "Стопа резиновая р.стопа с примеркой" });
                  context.DicSubDescriptionProducts.Add(new DicSubDescriptionProduct() { DicDescriptionProductId = 1000009, NameRus = "Стопа резиновая р.стопа без примеркой" });
                  context.DicSubDescriptionProducts.Add(new DicSubDescriptionProduct() { DicDescriptionProductId = 1000009, NameRus = "Уздечкой" });
                  context.DicSubDescriptionProducts.Add(new DicSubDescriptionProduct() { DicDescriptionProductId = 1000009, NameRus = "Крепление, гильза  голени на шнурках деревянный" });
                  context.DicSubDescriptionProducts.Add(new DicSubDescriptionProduct() { DicDescriptionProductId = 1000009, NameRus = "Крепление, гильза  голени на шнурках " });
                   context.DicSubDescriptionProducts.Add(new DicSubDescriptionProduct() { DicDescriptionProductId = 1000010, NameRus = "На правую  конечность" });
                   context.DicSubDescriptionProducts.Add(new DicSubDescriptionProduct() { DicDescriptionProductId = 1000010, NameRus = "На нижнюю конечность" });
                   context.DicSubDescriptionProducts.Add(new DicSubDescriptionProduct() { DicDescriptionProductId = 1000010, NameRus = "На нижнюю конечность и на правую конечность" });
                   context.DicSubDescriptionProducts.Add(new DicSubDescriptionProduct() { DicDescriptionProductId = 1000010, NameRus = "Шинокожанный" });
                   context.DicSubDescriptionProducts.Add(new DicSubDescriptionProduct() { DicDescriptionProductId = 1000010, NameRus = "Полиэтиленовый" });
                   context.DicSubDescriptionProducts.Add(new DicSubDescriptionProduct() { DicDescriptionProductId = 1000010, NameRus = "С сидением" });
                   context.DicSubDescriptionProducts.Add(new DicSubDescriptionProduct() { DicDescriptionProductId = 1000010, NameRus = "Без сидения" });
                   context.DicSubDescriptionProducts.Add(new DicSubDescriptionProduct() { DicDescriptionProductId = 1000010, NameRus = "Башмачком" });
                   context.DicSubDescriptionProducts.Add(new DicSubDescriptionProduct() { DicDescriptionProductId = 1000010, NameRus = "Безбашмачка" });
                   context.DicSubDescriptionProducts.Add(new DicSubDescriptionProduct() { DicDescriptionProductId = 1000010, NameRus = "С замком в коленном шарнире" });
                   context.DicSubDescriptionProducts.Add(new DicSubDescriptionProduct() { DicDescriptionProductId = 1000010, NameRus = "Без замка в коленном шарнире" });
                   context.DicSubDescriptionProducts.Add(new DicSubDescriptionProduct() { DicDescriptionProductId = 1000010, NameRus = "Движение в голеностопном суставе полное" });
                   context.DicSubDescriptionProducts.Add(new DicSubDescriptionProduct() { DicDescriptionProductId = 1000010, NameRus = "Движение в голеностопном суставе частичное" });
                   context.DicSubDescriptionProducts.Add(new DicSubDescriptionProduct() { DicDescriptionProductId = 1000010, NameRus = "Крепление узкий" });
                   context.DicSubDescriptionProducts.Add(new DicSubDescriptionProduct() { DicDescriptionProductId = 1000010, NameRus = "Крепление широкий " });
                   context.DicSubDescriptionProducts.Add(new DicSubDescriptionProduct() { DicDescriptionProductId = 1000010, NameRus = "Кожвертлук" });
                   context.DicSubDescriptionProducts.Add(new DicSubDescriptionProduct() { DicDescriptionProductId = 1000010, NameRus = "Крепление гильза бедра и голени на ремешках" });
                   context.DicSubDescriptionProducts.Add(new DicSubDescriptionProduct() { DicDescriptionProductId = 1000010, NameRus = "Крепление гильза бедра и голени на шнуровке" });
                   context.DicSubDescriptionProducts.Add(new DicSubDescriptionProduct() { DicDescriptionProductId = 1000010, NameRus = "Крепление гильза бедра и голени на ремешках с примеркой" });
                   context.DicSubDescriptionProducts.Add(new DicSubDescriptionProduct() { DicDescriptionProductId = 1000010, NameRus = "Крепление гильза бедра и голени на ремешках без примерки " });
                   context.DicSubDescriptionProducts.Add(new DicSubDescriptionProduct() { DicDescriptionProductId = 1000010, NameRus = "Крепление гильза бедра и голени на шнуровке с примеркой" });
                   context.DicSubDescriptionProducts.Add(new DicSubDescriptionProduct() { DicDescriptionProductId = 1000010, NameRus = "Крепление гильза бедра и голени на шнуровке без примерки" });
                    context.DicSubDescriptionProducts.Add(new DicSubDescriptionProduct() { DicDescriptionProductId = 1000011, NameRus = "Протез левого бедра, шино-кожный" });
                    context.DicSubDescriptionProducts.Add(new DicSubDescriptionProduct() { DicDescriptionProductId = 1000011, NameRus = "Чашка " });
                    context.DicSubDescriptionProducts.Add(new DicSubDescriptionProduct() { DicDescriptionProductId = 1000011, NameRus = "Креплени, гильза бедра на трех ремешках" });
                    context.DicSubDescriptionProducts.Add(new DicSubDescriptionProduct() { DicDescriptionProductId = 1000011, NameRus = "Пояс узкий" });
                    context.DicSubDescriptionProducts.Add(new DicSubDescriptionProduct() { DicDescriptionProductId = 1000011, NameRus = "Пояс широкий" });
                    context.DicSubDescriptionProducts.Add(new DicSubDescriptionProduct() { DicDescriptionProductId = 1000011, NameRus = "Кожвертлук роликовый ремень, помочь, облицовка пенополиуритана" });
                    context.DicSubDescriptionProducts.Add(new DicSubDescriptionProduct() { DicDescriptionProductId = 1000011, NameRus = "Стопа резиновая р.стопа с примеркой " });
                    context.DicSubDescriptionProducts.Add(new DicSubDescriptionProduct() { DicDescriptionProductId = 1000011, NameRus = "Стопа резиновая р.стопа без примеркой" });

                    //
                    //
                    context.DicSubDescriptionProducts.Add(new DicSubDescriptionProduct() { DicDescriptionProductId = 1000012, NameRus = "выкладка свода " });
                    context.DicSubDescriptionProducts.Add(new DicSubDescriptionProduct() { DicDescriptionProductId = 1000012, NameRus = "отведение каблука до конца первого пальца" });
                    context.DicSubDescriptionProducts.Add(new DicSubDescriptionProduct() { DicDescriptionProductId = 1000012, NameRus = "отведение каблука до пучков " });
                    context.DicSubDescriptionProducts.Add(new DicSubDescriptionProduct() { DicDescriptionProductId = 1000012, NameRus = "отведение каблука до носка" });
                    context.DicSubDescriptionProducts.Add(new DicSubDescriptionProduct() { DicDescriptionProductId = 1000013, NameRus = "выкладка свода " });
                    context.DicSubDescriptionProducts.Add(new DicSubDescriptionProduct() { DicDescriptionProductId = 1000013, NameRus = "пронатор отведение каблука к наруже до пучка " });
                    context.DicSubDescriptionProducts.Add(new DicSubDescriptionProduct() { DicDescriptionProductId = 1000013, NameRus = "пронатор отведение каблука к наруже до носка " });
                    context.DicSubDescriptionProducts.Add(new DicSubDescriptionProduct() { DicDescriptionProductId = 1000014, NameRus = "жесткий перед " });
                    context.DicSubDescriptionProducts.Add(new DicSubDescriptionProduct() { DicDescriptionProductId = 1000014, NameRus = "искусственный носок " });
                    context.DicSubDescriptionProducts.Add(new DicSubDescriptionProduct() { DicDescriptionProductId = 1000015, NameRus = "расширенная подошва " });
                    context.DicSubDescriptionProducts.Add(new DicSubDescriptionProduct() { DicDescriptionProductId = 1000015, NameRus = "набить сбоку колодки " });
                    context.DicSubDescriptionProducts.Add(new DicSubDescriptionProduct() { DicDescriptionProductId = 1000015, NameRus = "набить сверху пальцев " });
                    context.DicSubDescriptionProducts.Add(new DicSubDescriptionProduct() { DicDescriptionProductId = 1000015, NameRus = "выкладка свода " });
                    context.DicSubDescriptionProducts.Add(new DicSubDescriptionProduct() { DicDescriptionProductId = 1000015, NameRus = "углубить пятку под место омазалости " });
                    context.DicSubDescriptionProducts.Add(new DicSubDescriptionProduct() { DicDescriptionProductId = 1000016, NameRus = "пробка под пяту " });
                    context.DicSubDescriptionProducts.Add(new DicSubDescriptionProduct() { DicDescriptionProductId = 1000016, NameRus = "выкладка свода " });
                    context.DicSubDescriptionProducts.Add(new DicSubDescriptionProduct() { DicDescriptionProductId = 1000016, NameRus = "Круговой жесткий корсет  " });
                    context.DicSubDescriptionProducts.Add(new DicSubDescriptionProduct() { DicDescriptionProductId = 1000016, NameRus = "углубить пятку под место омазалости " });

                    context.SaveChanges();
               }
                //при ампутации--Описание изделия--деревянный 
                //if (!context.Derevyin.Any())
                //{
                //    context.Derevyin.Add(new Derevyin() { OpisIzdeliaId = 08, NameRus = "Протез левого бедра, деревянный" });
                //    context.Derevyin.Add(new Derevyin() { OpisIzdeliaId = 08, NameRus = "Узел, 16 ФЛ с замком " });
                //    context.Derevyin.Add(new Derevyin() { OpisIzdeliaId = 08, NameRus = "Креплени, гильза бедра на трех ремешках" });
                //    context.Derevyin.Add(new Derevyin() { OpisIzdeliaId = 08, NameRus = "Пояс узкий" });
                //    context.Derevyin.Add(new Derevyin() { OpisIzdeliaId = 08, NameRus = "Пояс широкий" });
                //    context.Derevyin.Add(new Derevyin() { OpisIzdeliaId = 08, NameRus = "Кожвертлук роликовый ремень, помочь, облицовка пенополиуритана" });
                //    context.Derevyin.Add(new Derevyin() { OpisIzdeliaId = 08, NameRus = "Стопа резиновая р.стопа с примеркой " });
                //    context.Derevyin.Add(new Derevyin() { OpisIzdeliaId = 08, NameRus = "Стопа резиновая р.стопа без примеркой" });
                //    context.SaveChanges();
                //}
                //при ампутации--Описание изделия--Голень
                //if (!context.Golen.Any())
                //{
                //    context.Golen.Add(new Golen() { OpisIzdeliaId = 09, NameRus = "Протез левой голени, шино-кожный" });
                //    context.Golen.Add(new Golen() { OpisIzdeliaId = 09, NameRus = "Узел, 16 ФЛ с замком " });
                //    context.Golen.Add(new Golen() { OpisIzdeliaId = 09, NameRus = "Узел, 23 ФЛ с замком" });
                //    context.Golen.Add(new Golen() { OpisIzdeliaId = 09, NameRus = "Крепление, гильза бедра и голени на трех ремешках" });
                //    context.Golen.Add(new Golen() { OpisIzdeliaId = 09, NameRus = "Крепление, гильза бедра и голени на шнурках" });
                //    context.Golen.Add(new Golen() { OpisIzdeliaId = 09, NameRus = "Пояс узкий" });
                //    context.Golen.Add(new Golen() { OpisIzdeliaId = 09, NameRus = "Пояс широкий " });
                //    context.Golen.Add(new Golen() { OpisIzdeliaId = 09, NameRus = "Кожвертлук " });
                //    context.Golen.Add(new Golen() { OpisIzdeliaId = 09, NameRus = "Стопа резиновая р.стопа с примеркой" });
                //    context.Golen.Add(new Golen() { OpisIzdeliaId = 09, NameRus = "Стопа резиновая р.стопа без примеркой" });
                //    context.Golen.Add(new Golen() { OpisIzdeliaId = 09, NameRus = "Уздечкой" });
                //    context.Golen.Add(new Golen() { OpisIzdeliaId = 09, NameRus = "Крепление, гильза  голени на шнурках деревянный" });
                //    context.Golen.Add(new Golen() { OpisIzdeliaId = 09, NameRus = "Крепление, гильза  голени на шнурках " });

                //    context.SaveChanges();
                //}
                //при ампутации--Описание изделия-Фиксационный аппарат
                //if (!context.FiksApp.Any())
                //{
                //    context.FiksApp.Add(new FiksApp() { OpisIzdeliaId = 10, NameRus = "На правую  конечность" });
                //    context.FiksApp.Add(new FiksApp() { OpisIzdeliaId = 10, NameRus = "На нижнюю конечность" });
                //    context.FiksApp.Add(new FiksApp() { OpisIzdeliaId = 10, NameRus = "На нижнюю конечность и на правую конечность" });
                //    context.FiksApp.Add(new FiksApp() { OpisIzdeliaId = 10, NameRus = "Шинокожанный" });
                //    context.FiksApp.Add(new FiksApp() { OpisIzdeliaId = 10, NameRus = "Полиэтиленовый" });
                //    context.FiksApp.Add(new FiksApp() { OpisIzdeliaId = 10, NameRus = "С сидением" });
                //    context.FiksApp.Add(new FiksApp() { OpisIzdeliaId = 10, NameRus = "Без сидения" });
                //    context.FiksApp.Add(new FiksApp() { OpisIzdeliaId = 10, NameRus = "Башмачком" });
                //    context.FiksApp.Add(new FiksApp() { OpisIzdeliaId = 10, NameRus = "Безбашмачка" });
                //    context.FiksApp.Add(new FiksApp() { OpisIzdeliaId = 10, NameRus = "С замком в коленном шарнире" });
                //    context.FiksApp.Add(new FiksApp() { OpisIzdeliaId = 10, NameRus = "Без замка в коленном шарнире" });
                //    context.FiksApp.Add(new FiksApp() { OpisIzdeliaId = 10, NameRus = "Движение в голеностопном суставе полное" });
                //    context.FiksApp.Add(new FiksApp() { OpisIzdeliaId = 10, NameRus = "Движение в голеностопном суставе частичное" });
                //    context.FiksApp.Add(new FiksApp() { OpisIzdeliaId = 10, NameRus = "Крепление узкий" });
                //    context.FiksApp.Add(new FiksApp() { OpisIzdeliaId = 10, NameRus = "Крепление широкий " });
                //    context.FiksApp.Add(new FiksApp() { OpisIzdeliaId = 10, NameRus = "Кожвертлук" });
                //    context.FiksApp.Add(new FiksApp() { OpisIzdeliaId = 10, NameRus = "Крепление гильза бедра и голени на ремешках" });
                //    context.FiksApp.Add(new FiksApp() { OpisIzdeliaId = 10, NameRus = "Крепление гильза бедра и голени на шнуровке" });
                //    context.FiksApp.Add(new FiksApp() { OpisIzdeliaId = 10, NameRus = "Крепление гильза бедра и голени на ремешках с примеркой" });
                //    context.FiksApp.Add(new FiksApp() { OpisIzdeliaId = 10, NameRus = "Крепление гильза бедра и голени на ремешках без примерки " });
                //    context.FiksApp.Add(new FiksApp() { OpisIzdeliaId = 10, NameRus = "Крепление гильза бедра и голени на шнуровке с примеркой" });
                //    context.FiksApp.Add(new FiksApp() { OpisIzdeliaId = 10, NameRus = "Крепление гильза бедра и голени на шнуровке без примерки" });
                //    context.SaveChanges();
                //}
                //при ампутации--Описание изделия--Протез голени по Пирогову 
                //if (!context.ProtezGoleni.Any())
                //{
                //    context.ProtezGoleni.Add(new ProtezGoleni() { OpisIzdeliaId = 11, NameRus = "Протез левого бедра, шино-кожный" });
                //    context.ProtezGoleni.Add(new ProtezGoleni() { OpisIzdeliaId = 11, NameRus = "Чашка " });
                //    context.ProtezGoleni.Add(new ProtezGoleni() { OpisIzdeliaId = 11, NameRus = "Креплени, гильза бедра на трех ремешках" });
                //    context.ProtezGoleni.Add(new ProtezGoleni() { OpisIzdeliaId = 11, NameRus = "Пояс узкий" });
                //    context.ProtezGoleni.Add(new ProtezGoleni() { OpisIzdeliaId = 11, NameRus = "Пояс широкий" });
                //    context.ProtezGoleni.Add(new ProtezGoleni() { OpisIzdeliaId = 11, NameRus = "Кожвертлук роликовый ремень, помочь, облицовка пенополиуритана" });
                //    context.ProtezGoleni.Add(new ProtezGoleni() { OpisIzdeliaId = 11, NameRus = "Стопа резиновая р.стопа с примеркой " });
                //    context.ProtezGoleni.Add(new ProtezGoleni() { OpisIzdeliaId = 11, NameRus = "Стопа резиновая р.стопа без примеркой" });
                //    context.SaveChanges();
                //}
                
                if (!context.DicStumpMobilities.Any())
                {
                    context.DicStumpMobilities.Add(new DicStumpMobility() { NameRus = "нормальная" });
                    context.DicStumpMobilities.Add(new DicStumpMobility() { NameRus = "ограничение движения"});
                    context.DicStumpMobilities.Add(new DicStumpMobility() { NameRus = "контрактура" });

                    context.SaveChanges();
                }
                
                if (!context.DicScars.Any())
                {
                    context.DicScars.Add(new DicScar() { NameRus = "линейный" });
                    context.DicScars.Add(new DicScar() { NameRus = "звездчатый" });
                    context.DicScars.Add(new DicScar() { NameRus = "центральный" });
                    context.DicScars.Add(new DicScar() { NameRus = "передний" });
                    context.DicScars.Add(new DicScar() { NameRus = "задний"});
                    context.DicScars.Add(new DicScar() { NameRus = "боковой" });
                    context.DicScars.Add(new DicScar() { NameRus = "подвижный" });
                    context.DicScars.Add(new DicScar() { NameRus = "спаянный" });
                    context.DicScars.Add(new DicScar() { NameRus = "безболезненный" });
                    context.DicScars.Add(new DicScar() { NameRus = "келоидный" });
                    context.SaveChanges();
                }
                
                if (!context.DicStumpSkinAndTissues.Any())
                {
                    context.DicStumpSkinAndTissues.Add(new DicStumpSkinAndTissues() { NameRus = "нормальный" });
                    context.DicStumpSkinAndTissues.Add(new DicStumpSkinAndTissues() { NameRus = "синюшный" });
                    context.DicStumpSkinAndTissues.Add(new DicStumpSkinAndTissues() { NameRus = "отечный" });
                    context.DicStumpSkinAndTissues.Add(new DicStumpSkinAndTissues() { NameRus = "потертости" });
                    context.DicStumpSkinAndTissues.Add(new DicStumpSkinAndTissues() { NameRus = "трещины" });
                    context.DicStumpSkinAndTissues.Add(new DicStumpSkinAndTissues() { NameRus = "язвы" });
                    context.DicStumpSkinAndTissues.Add(new DicStumpSkinAndTissues() { NameRus = "свищи" });
                    context.DicStumpSkinAndTissues.Add(new DicStumpSkinAndTissues() { NameRus = "невромы" });

                    context.SaveChanges();
                }
                
                if (!context.DicStumpShapes.Any())
                {
                    context.DicStumpShapes.Add(new DicStumpShape() { NameRus = "цилиндрическая" });
                    context.DicStumpShapes.Add(new DicStumpShape() { NameRus = "булавовидная" });
                    context.DicStumpShapes.Add(new DicStumpShape() { NameRus = "умеренно-коническая"});
                    context.DicStumpShapes.Add(new DicStumpShape() { NameRus = "резко-коническая"});
                    context.DicStumpShapes.Add(new DicStumpShape() { NameRus = "избыток ткани" });
                    context.DicStumpShapes.Add(new DicStumpShape() { NameRus = "атрофия" });
                    context.SaveChanges();
                }

                if (!context.DicStumpSupports.Any())
                {
                    context.DicStumpSupports.Add(new DicStumpSupport() { NameRus = "да" });
                    context.DicStumpSupports.Add(new DicStumpSupport() { NameRus = "нет" });
                    context.SaveChanges();
                }

                //if (!context.DicStumpSupports.Any())
                //{
                //    context.DicStumpSupports.Add(new DicStumpSupport() { NameRus = "на шнуровке" });
                //    context.DicStumpSupports.Add(new DicStumpSupport() { NameRus = "на замочках" });
                //    context.DicStumpSupports.Add(new DicStumpSupport() { NameRus = "на ремешках"});
                //    context.DicStumpSupports.Add(new DicStumpSupport() { NameRus = "на резинках" });
                //    context.DicStumpSupports.Add(new DicStumpSupport() { NameRus = "с одним перекидным ремешком" });
                //    context.SaveChanges();
                //}

                if (!context.DicSoleTypes.Any())
                {
                    context.DicSoleTypes.Add(new DicSoleType() { NameRus = "микропористая" });
                    context.DicSoleTypes.Add(new DicSoleType() { NameRus = "полиуретановая" });
                    context.SaveChanges();
                }


                if (!context.DicColors.Any())
                {
                    context.DicColors.Add(new DicColor() {  NameRus = "черный " });
                    context.DicColors.Add(new DicColor() {  NameRus = "красный " });
                    context.DicColors.Add(new DicColor() {  NameRus = "комбинированный  " });
                    context.DicColors.Add(new DicColor() {  NameRus = "бордовый " });
                    context.SaveChanges();
                }

                if (!context.DicNameProducts.Any())
                {
                    context.DicNameProducts.Add(new DicNameProduct() { Id = 1000017, NameRus = "Шины" });
                    context.DicNameProducts.Add(new DicNameProduct() { Id = 1000018, NameRus = "Шины" });
                    context.DicNameProducts.Add(new DicNameProduct() { Id = 1000019, NameRus = "Шины" });
                    context.DicNameProducts.Add(new DicNameProduct() { Id = 1000020, NameRus = "Стопа" });
                    context.DicNameProducts.Add(new DicNameProduct() { Id = 1000021, NameRus = "Стопа" });
                    context.DicNameProducts.Add(new DicNameProduct() { Id = 1000024, NameRus = "Узел" });
                    context.DicNameProducts.Add(new DicNameProduct() { Id = 1000025, NameRus = "Узел" });
                    context.DicNameProducts.Add(new DicNameProduct() { Id = 1000022, NameRus = "Щиколотка" });
                    context.DicNameProducts.Add(new DicNameProduct() { Id = 1000023, NameRus = "Щиколотка" });
                    context.DicNameProducts.Add(new DicNameProduct() { Id = 1000026, NameRus = "Вертлук " });
                    context.DicNameProducts.Add(new DicNameProduct() { Id = 1000027, NameRus = "П/кольцо" });
                    context.DicNameProducts.Add(new DicNameProduct() { Id = 1000028, NameRus = "П/кольцо" });
                    context.DicNameProducts.Add(new DicNameProduct() { Id = 1000029, NameRus = "Сиденье " });
                    context.DicNameProducts.Add(new DicNameProduct() { Id = 1000030, NameRus = "Кисть " });
                    context.DicNameProducts.Add(new DicNameProduct() { Id = 1000031, NameRus = "Стелька " });
                    context.DicNameProducts.Add(new DicNameProduct() { Id = 1000032, NameRus = "Болванка " });
                    context.DicNameProducts.Add(new DicNameProduct() { Id = 1000033, NameRus = "Насадка  " });
                    context.DicNameProducts.Add(new DicNameProduct() { Id = 1000034, NameRus = "Чашка  " });
                    context.SaveChanges();
                }

                if (!context.DicCodeProducts.Any())
                {
                    context.DicCodeProducts.Add(new DicCodeProduct() { DicNameProductId = 1000017, NameRus = "010 шарнир " });
                    context.DicCodeProducts.Add(new DicCodeProduct() { DicNameProductId = 1000017, NameRus = "050 шарнир " });
                    context.DicCodeProducts.Add(new DicCodeProduct() { DicNameProductId = 1000017, NameRus = "б/шарнир  " });
                    context.DicCodeProducts.Add(new DicCodeProduct() { DicNameProductId = 1000017, NameRus = "010 норм " });
                    context.DicCodeProducts.Add(new DicCodeProduct() { DicNameProductId = 1000017, NameRus = "016 усл " });
                    context.DicCodeProducts.Add(new DicCodeProduct() { DicNameProductId = 1000017, NameRus = "005 В" });
                    context.DicCodeProducts.Add(new DicCodeProduct() { DicNameProductId = 1000017, NameRus = "002 без замка " });
                    context.DicCodeProducts.Add(new DicCodeProduct() { DicNameProductId = 1000017, NameRus = "008 с замком " });
                    context.DicCodeProducts.Add(new DicCodeProduct() { DicNameProductId = 1000017, NameRus = "032 " });
                    context.DicCodeProducts.Add(new DicCodeProduct() { DicNameProductId = 1000017, NameRus = "031 " });
                    context.DicCodeProducts.Add(new DicCodeProduct() { DicNameProductId = 1000017, NameRus = "0110 У  " });
                    context.DicCodeProducts.Add(new DicCodeProduct() { DicNameProductId = 1000017, NameRus = "005 " });
                    context.DicCodeProducts.Add(new DicCodeProduct() { DicNameProductId = 1000017, NameRus = "002 " });
                    context.DicCodeProducts.Add(new DicCodeProduct() { DicNameProductId = 1000017, NameRus = "031 с замком  " });
                    context.DicCodeProducts.Add(new DicCodeProduct() { DicNameProductId = 1000017, NameRus = "002 с замком " });
                    context.DicCodeProducts.Add(new DicCodeProduct() { DicNameProductId = 1000017, NameRus = "008 без замка " });
                    context.DicCodeProducts.Add(new DicCodeProduct() { DicNameProductId = 1000017, NameRus = "004" });
                    context.DicCodeProducts.Add(new DicCodeProduct() { DicNameProductId = 1000017, NameRus = "007 " });
                    context.DicCodeProducts.Add(new DicCodeProduct() { DicNameProductId = 1000017, NameRus = "0766 " });
                    context.DicCodeProducts.Add(new DicCodeProduct() { DicNameProductId = 1000017, NameRus = "764 " });

                    context.DicCodeProducts.Add(new DicCodeProduct() { DicNameProductId = 1000018, NameRus = "010 шарнир " });
                    context.DicCodeProducts.Add(new DicCodeProduct() { DicNameProductId = 1000018, NameRus = "050 шарнир " });
                    context.DicCodeProducts.Add(new DicCodeProduct() { DicNameProductId = 1000018, NameRus = "б/шарнир  " });
                    context.DicCodeProducts.Add(new DicCodeProduct() { DicNameProductId = 1000018, NameRus = "010 норм " });
                    context.DicCodeProducts.Add(new DicCodeProduct() { DicNameProductId = 1000018, NameRus = "016 усл " });
                    context.DicCodeProducts.Add(new DicCodeProduct() { DicNameProductId = 1000018, NameRus = "005 В" });
                    context.DicCodeProducts.Add(new DicCodeProduct() { DicNameProductId = 1000018, NameRus = "002 без замка " });
                    context.DicCodeProducts.Add(new DicCodeProduct() { DicNameProductId = 1000018, NameRus = "008 с замком " });
                    context.DicCodeProducts.Add(new DicCodeProduct() { DicNameProductId = 1000018, NameRus = "032 " });
                    context.DicCodeProducts.Add(new DicCodeProduct() { DicNameProductId = 1000018, NameRus = "031 " });
                    context.DicCodeProducts.Add(new DicCodeProduct() { DicNameProductId = 1000018, NameRus = "0110 У  " });
                    context.DicCodeProducts.Add(new DicCodeProduct() { DicNameProductId = 1000018, NameRus = "005 " });
                    context.DicCodeProducts.Add(new DicCodeProduct() { DicNameProductId = 1000018, NameRus = "002 " });
                    context.DicCodeProducts.Add(new DicCodeProduct() { DicNameProductId = 1000018, NameRus = "031 с замком  " });
                    context.DicCodeProducts.Add(new DicCodeProduct() { DicNameProductId = 1000018, NameRus = "002 с замком " });
                    context.DicCodeProducts.Add(new DicCodeProduct() { DicNameProductId = 1000018, NameRus = "008 без замка " });
                    context.DicCodeProducts.Add(new DicCodeProduct() { DicNameProductId = 1000018, NameRus = "004" });
                    context.DicCodeProducts.Add(new DicCodeProduct() { DicNameProductId = 1000018, NameRus = "007 " });
                    context.DicCodeProducts.Add(new DicCodeProduct() { DicNameProductId = 1000018, NameRus = "0766 " });
                    context.DicCodeProducts.Add(new DicCodeProduct() { DicNameProductId = 1000018, NameRus = "764 " });

                    context.DicCodeProducts.Add(new DicCodeProduct() { DicNameProductId = 1000019, NameRus = "010 шарнир " });
                    context.DicCodeProducts.Add(new DicCodeProduct() { DicNameProductId = 1000019, NameRus = "050 шарнир " });
                    context.DicCodeProducts.Add(new DicCodeProduct() { DicNameProductId = 1000019, NameRus = "б/шарнир  " });
                    context.DicCodeProducts.Add(new DicCodeProduct() { DicNameProductId = 1000019, NameRus = "010 норм " });
                    context.DicCodeProducts.Add(new DicCodeProduct() { DicNameProductId = 1000019, NameRus = "016 усл " });
                    context.DicCodeProducts.Add(new DicCodeProduct() { DicNameProductId = 1000019, NameRus = "005 В" });
                    context.DicCodeProducts.Add(new DicCodeProduct() { DicNameProductId = 1000019, NameRus = "002 без замка " });
                    context.DicCodeProducts.Add(new DicCodeProduct() { DicNameProductId = 1000019, NameRus = "008 с замком " });
                    context.DicCodeProducts.Add(new DicCodeProduct() { DicNameProductId = 1000019, NameRus = "032 " });
                    context.DicCodeProducts.Add(new DicCodeProduct() { DicNameProductId = 1000019, NameRus = "031 " });
                    context.DicCodeProducts.Add(new DicCodeProduct() { DicNameProductId = 1000019, NameRus = "0110 У  " });
                    context.DicCodeProducts.Add(new DicCodeProduct() { DicNameProductId = 1000019, NameRus = "005 " });
                    context.DicCodeProducts.Add(new DicCodeProduct() { DicNameProductId = 1000019, NameRus = "002 " });
                    context.DicCodeProducts.Add(new DicCodeProduct() { DicNameProductId = 1000019, NameRus = "031 с замком  " });
                    context.DicCodeProducts.Add(new DicCodeProduct() { DicNameProductId = 1000019, NameRus = "002 с замком " });
                    context.DicCodeProducts.Add(new DicCodeProduct() { DicNameProductId = 1000019, NameRus = "008 без замка " });
                    context.DicCodeProducts.Add(new DicCodeProduct() { DicNameProductId = 1000019, NameRus = "004" });
                    context.DicCodeProducts.Add(new DicCodeProduct() { DicNameProductId = 1000019, NameRus = "007 " });
                    context.DicCodeProducts.Add(new DicCodeProduct() { DicNameProductId = 1000019, NameRus = "0766 " });
                    context.DicCodeProducts.Add(new DicCodeProduct() { DicNameProductId = 1000019, NameRus = "764 " });

                    context.DicCodeProducts.Add(new DicCodeProduct() { DicNameProductId = 1000020, NameRus = "8017 фильцевая" });
                    context.DicCodeProducts.Add(new DicCodeProduct() { DicNameProductId = 1000020, NameRus = "511 (25-30) м" });
                    context.DicCodeProducts.Add(new DicCodeProduct() { DicNameProductId = 1000020, NameRus = "512 (22-27) ж.на низ" });
                    context.DicCodeProducts.Add(new DicCodeProduct() { DicNameProductId = 1000020, NameRus = "каб, 513 ж. На сред." });
                    context.DicCodeProducts.Add(new DicCodeProduct() { DicNameProductId = 1000020, NameRus = "каб." });
                    context.DicCodeProducts.Add(new DicCodeProduct() { DicNameProductId = 1000020, NameRus = "511-513" });
                    context.DicCodeProducts.Add(new DicCodeProduct() { DicNameProductId = 1000020, NameRus = "9015" });
                    context.DicCodeProducts.Add(new DicCodeProduct() { DicNameProductId = 1000020, NameRus = "шины-лапки 0716" });
                    context.DicCodeProducts.Add(new DicCodeProduct() { DicNameProductId = 1000020, NameRus = "0714" });
                    context.DicCodeProducts.Add(new DicCodeProduct() { DicNameProductId = 1000020, NameRus = "0717" });
                    context.DicCodeProducts.Add(new DicCodeProduct() { DicNameProductId = 1000020, NameRus = "943332" });

                    context.DicCodeProducts.Add(new DicCodeProduct() { DicNameProductId = 1000021, NameRus = "8017 фильцевая" });
                    context.DicCodeProducts.Add(new DicCodeProduct() { DicNameProductId = 1000021, NameRus = "511 (25-30) м" });
                    context.DicCodeProducts.Add(new DicCodeProduct() { DicNameProductId = 1000021, NameRus = "512 (22-27) ж.на низ" });
                    context.DicCodeProducts.Add(new DicCodeProduct() { DicNameProductId = 1000021, NameRus = "каб, 513 ж. На сред." });
                    context.DicCodeProducts.Add(new DicCodeProduct() { DicNameProductId = 1000021, NameRus = "каб." });
                    context.DicCodeProducts.Add(new DicCodeProduct() { DicNameProductId = 1000021, NameRus = "511-513" });
                    context.DicCodeProducts.Add(new DicCodeProduct() { DicNameProductId = 1000021, NameRus = "9015" });
                    context.DicCodeProducts.Add(new DicCodeProduct() { DicNameProductId = 1000021, NameRus = "шины-лапки 0716" });
                    context.DicCodeProducts.Add(new DicCodeProduct() { DicNameProductId = 1000021, NameRus = "0714" });
                    context.DicCodeProducts.Add(new DicCodeProduct() { DicNameProductId = 1000021, NameRus = "0717" });
                    context.DicCodeProducts.Add(new DicCodeProduct() { DicNameProductId = 1000021, NameRus = "943332" });

                    context.DicCodeProducts.Add(new DicCodeProduct() { DicNameProductId = 1000024, NameRus = "По Пирогову" });
                    context.DicCodeProducts.Add(new DicCodeProduct() { DicNameProductId = 1000024, NameRus = "23П-01 (d-94)" });
                    context.DicCodeProducts.Add(new DicCodeProduct() { DicNameProductId = 1000024, NameRus = "23П-02 (d-102)" });
                    context.DicCodeProducts.Add(new DicCodeProduct() { DicNameProductId = 1000024, NameRus = "23П-03 (d- 107)" });
                    context.DicCodeProducts.Add(new DicCodeProduct() { DicNameProductId = 1000024, NameRus = "23П-04 (d- 116)" });
                    context.DicCodeProducts.Add(new DicCodeProduct() { DicNameProductId = 1000024, NameRus = "23П-05 (d- 125)" });
                    context.DicCodeProducts.Add(new DicCodeProduct() { DicNameProductId = 1000024, NameRus = "23П-06 (d- 135)" });
                    context.DicCodeProducts.Add(new DicCodeProduct() { DicNameProductId = 1000024, NameRus = "23 П" });
                    context.DicCodeProducts.Add(new DicCodeProduct() { DicNameProductId = 1000024, NameRus = "16 Ф" });
                    context.DicCodeProducts.Add(new DicCodeProduct() { DicNameProductId = 1000024, NameRus = "16М б/замка" });
                    context.DicCodeProducts.Add(new DicCodeProduct() { DicNameProductId = 1000024, NameRus = "16Ф с замком" });
                    context.DicCodeProducts.Add(new DicCodeProduct() { DicNameProductId = 1000024, NameRus = "7ПЛ" });
                    context.DicCodeProducts.Add(new DicCodeProduct() { DicNameProductId = 1000024, NameRus = "стелька-металличская " });
                    context.DicCodeProducts.Add(new DicCodeProduct() { DicNameProductId = 1000024, NameRus = "1140 шино-лапки" });
                    context.DicCodeProducts.Add(new DicCodeProduct() { DicNameProductId = 1000024, NameRus = "узел несущий" });
                    context.DicCodeProducts.Add(new DicCodeProduct() { DicNameProductId = 1000024, NameRus = "943333" });

                    context.DicCodeProducts.Add(new DicCodeProduct() { DicNameProductId = 1000025, NameRus = "По Пирогову" });
                    context.DicCodeProducts.Add(new DicCodeProduct() { DicNameProductId = 1000025, NameRus = "23П-01 (d-94)" });
                    context.DicCodeProducts.Add(new DicCodeProduct() { DicNameProductId = 1000025, NameRus = "23П-02 (d-102)" });
                    context.DicCodeProducts.Add(new DicCodeProduct() { DicNameProductId = 1000025, NameRus = "23П-03 (d- 107)" });
                    context.DicCodeProducts.Add(new DicCodeProduct() { DicNameProductId = 1000025, NameRus = "23П-04 (d- 116)" });
                    context.DicCodeProducts.Add(new DicCodeProduct() { DicNameProductId = 1000025, NameRus = "23П-05 (d- 125)" });
                    context.DicCodeProducts.Add(new DicCodeProduct() { DicNameProductId = 1000025, NameRus = "23П-06 (d- 135)" });
                    context.DicCodeProducts.Add(new DicCodeProduct() { DicNameProductId = 1000025, NameRus = "23 П" });
                    context.DicCodeProducts.Add(new DicCodeProduct() { DicNameProductId = 1000025, NameRus = "16 Ф" });
                    context.DicCodeProducts.Add(new DicCodeProduct() { DicNameProductId = 1000025, NameRus = "16М б/замка" });
                    context.DicCodeProducts.Add(new DicCodeProduct() { DicNameProductId = 1000025, NameRus = "16Ф с замком" });
                    context.DicCodeProducts.Add(new DicCodeProduct() { DicNameProductId = 1000025, NameRus = "7ПЛ" });
                    context.DicCodeProducts.Add(new DicCodeProduct() { DicNameProductId = 1000025, NameRus = "стелька-металличская " });
                    context.DicCodeProducts.Add(new DicCodeProduct() { DicNameProductId = 1000025, NameRus = "1140 шино-лапки" });
                    context.DicCodeProducts.Add(new DicCodeProduct() { DicNameProductId = 1000025, NameRus = "узел несущий" });
                    context.DicCodeProducts.Add(new DicCodeProduct() { DicNameProductId = 1000025, NameRus = "943333" });


                    context.DicCodeProducts.Add(new DicCodeProduct() { DicNameProductId = 1000022, NameRus = "8019 дерев-я" });
                    context.DicCodeProducts.Add(new DicCodeProduct() { DicNameProductId = 1000022, NameRus = "8019 дерев-я шинолапки" });
                    context.DicCodeProducts.Add(new DicCodeProduct() { DicNameProductId = 1000022, NameRus = "1205 АН" });
                    context.DicCodeProducts.Add(new DicCodeProduct() { DicNameProductId = 1000022, NameRus = "1205 БМ щикол.металлическая" });
                    context.DicCodeProducts.Add(new DicCodeProduct() { DicNameProductId = 1000022, NameRus = "8019 -дерев-я. б/шины" });
                    context.DicCodeProducts.Add(new DicCodeProduct() { DicNameProductId = 1000022, NameRus = "5260 (24-26)" });
                    context.DicCodeProducts.Add(new DicCodeProduct() { DicNameProductId = 1000022, NameRus = "5260-01 (26,5-28,5)" });
                    context.DicCodeProducts.Add(new DicCodeProduct() { DicNameProductId = 1000022, NameRus = "8019 (27-30)" });
                    context.DicCodeProducts.Add(new DicCodeProduct() { DicNameProductId = 1000022, NameRus = "8019-02" });
                    context.DicCodeProducts.Add(new DicCodeProduct() { DicNameProductId = 1000022, NameRus = "5260-23 П" });
                    context.DicCodeProducts.Add(new DicCodeProduct() { DicNameProductId = 1000022, NameRus = "5260" });


                    context.DicCodeProducts.Add(new DicCodeProduct() { DicNameProductId = 1000023, NameRus = "8019 дерев-я" });
                    context.DicCodeProducts.Add(new DicCodeProduct() { DicNameProductId = 1000023, NameRus = "8019 дерев-я шинолапки" });
                    context.DicCodeProducts.Add(new DicCodeProduct() { DicNameProductId = 1000023, NameRus = "1205 АН" });
                    context.DicCodeProducts.Add(new DicCodeProduct() { DicNameProductId = 1000023, NameRus = "1205 БМ щикол.металлическая" });
                    context.DicCodeProducts.Add(new DicCodeProduct() { DicNameProductId = 1000023, NameRus = "8019 -дерев-я. б/шины" });
                    context.DicCodeProducts.Add(new DicCodeProduct() { DicNameProductId = 1000023, NameRus = "5260 (24-26)" });
                    context.DicCodeProducts.Add(new DicCodeProduct() { DicNameProductId = 1000023, NameRus = "5260-01 (26,5-28,5)" });
                    context.DicCodeProducts.Add(new DicCodeProduct() { DicNameProductId = 1000023, NameRus = "8019 (27-30)" });
                    context.DicCodeProducts.Add(new DicCodeProduct() { DicNameProductId = 1000023, NameRus = "8019-02" });
                    context.DicCodeProducts.Add(new DicCodeProduct() { DicNameProductId = 1000023, NameRus = "5260-23 П" });
                    context.DicCodeProducts.Add(new DicCodeProduct() { DicNameProductId = 1000023, NameRus = "5260" });


                    context.DicCodeProducts.Add(new DicCodeProduct() { DicNameProductId = 1000027, NameRus = "124 фигурн. Желобленное" });
                    context.DicCodeProducts.Add(new DicCodeProduct() { DicNameProductId = 1000027, NameRus = "121 пр." });
                    context.DicCodeProducts.Add(new DicCodeProduct() { DicNameProductId = 1000027, NameRus = "124 фигурн." });
                    context.DicCodeProducts.Add(new DicCodeProduct() { DicNameProductId = 1000027, NameRus = "122" });
                    context.DicCodeProducts.Add(new DicCodeProduct() { DicNameProductId = 1000027, NameRus = "121" });

                    context.DicCodeProducts.Add(new DicCodeProduct() { DicNameProductId = 1000028, NameRus = "124 фигурн. Желобленное" });
                    context.DicCodeProducts.Add(new DicCodeProduct() { DicNameProductId = 1000028, NameRus = "121 пр." });
                    context.DicCodeProducts.Add(new DicCodeProduct() { DicNameProductId = 1000028, NameRus = "124 фигурн." });
                    context.DicCodeProducts.Add(new DicCodeProduct() { DicNameProductId = 1000028, NameRus = "122" });
                    context.DicCodeProducts.Add(new DicCodeProduct() { DicNameProductId = 1000028, NameRus = "121" });


                    context.DicCodeProducts.Add(new DicCodeProduct() { DicNameProductId = 1000029, NameRus = "120-3" });
                    
                    context.DicCodeProducts.Add(new DicCodeProduct() { DicNameProductId = 1000032, NameRus = "811 голень" });
                    context.DicCodeProducts.Add(new DicCodeProduct() { DicNameProductId = 1000032, NameRus = "Металлический вертлуг 125" });

                    context.DicCodeProducts.Add(new DicCodeProduct() { DicNameProductId = 1000034, NameRus = "113" });
                    context.DicCodeProducts.Add(new DicCodeProduct() { DicNameProductId = 1000034, NameRus = "1197" });
                    context.DicCodeProducts.Add(new DicCodeProduct() { DicNameProductId = 1000034, NameRus = "119" });
                    context.DicCodeProducts.Add(new DicCodeProduct() { DicNameProductId = 1000034, NameRus = "Металлический вертлуг 125" });

                    context.SaveChanges();
                }

                if (!context.DicPersonCardStatuses.Any())
                {
                    context.DicPersonCardStatuses.Add(new DicPersonCardStatus() { Id = 1, Name="Неактивно" });
                    context.DicPersonCardStatuses.Add(new DicPersonCardStatus() { Id = 2, Name="Активировано" });
                    context.DicPersonCardStatuses.Add(new DicPersonCardStatus() { Id = 3, Name="Диагнозировано" });
                    context.DicPersonCardStatuses.Add(new DicPersonCardStatus() { Id = 4, Name="Тех-врач-рассмотрено" });
                    context.DicPersonCardStatuses.Add(new DicPersonCardStatus() { Id = 5, Name="Визировано" });
                    context.SaveChanges();
                }

            }
            catch { }
        }
    }
    
}

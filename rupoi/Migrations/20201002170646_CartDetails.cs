﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

namespace rupoi.Migrations
{
    public partial class CartDetails : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "BoneDustTypes",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    Name = table.Column<string>(maxLength: 250, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BoneDustTypes", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "DiagnosisType",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    Name = table.Column<string>(maxLength: 250, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DiagnosisType", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "DirectToService",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    CartId = table.Column<int>(nullable: true),
                    DirectDate = table.Column<DateTime>(nullable: false),
                    DirectDiagnosis = table.Column<string>(maxLength: 500, nullable: true),
                    Organization = table.Column<string>(maxLength: 200, nullable: true),
                    Doctor = table.Column<string>(maxLength: 200, nullable: true),
                    ServiceType = table.Column<string>(maxLength: 100, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DirectToService", x => x.Id);
                    table.ForeignKey(
                        name: "FK_DirectToService_Cart_CartId",
                        column: x => x.CartId,
                        principalTable: "Cart",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "ProductType",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    Name = table.Column<string>(maxLength: 250, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ProductType", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "ScarTypes",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    Name = table.Column<string>(maxLength: 250, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ScarTypes", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "SkinConditionTypes",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    Name = table.Column<string>(maxLength: 250, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SkinConditionTypes", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "StumpForm",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    Name = table.Column<string>(maxLength: 250, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_StumpForm", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "ProstheticsData",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    CartId = table.Column<int>(nullable: true),
                    Length = table.Column<int>(nullable: false),
                    StumpFormId = table.Column<int>(nullable: true),
                    StumpMobility = table.Column<int>(nullable: false),
                    StumpMobilityDescription = table.Column<string>(nullable: true),
                    ScarTypeId = table.Column<int>(nullable: true),
                    SkinConditionTypeId = table.Column<int>(nullable: true),
                    StumpSupport = table.Column<bool>(nullable: false),
                    ObjectivData = table.Column<string>(maxLength: 500, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ProstheticsData", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ProstheticsData_Cart_CartId",
                        column: x => x.CartId,
                        principalTable: "Cart",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_ProstheticsData_ScarTypes_ScarTypeId",
                        column: x => x.ScarTypeId,
                        principalTable: "ScarTypes",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_ProstheticsData_SkinConditionTypes_SkinConditionTypeId",
                        column: x => x.SkinConditionTypeId,
                        principalTable: "SkinConditionTypes",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_ProstheticsData_StumpForm_StumpFormId",
                        column: x => x.StumpFormId,
                        principalTable: "StumpForm",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_DirectToService_CartId",
                table: "DirectToService",
                column: "CartId");

            migrationBuilder.CreateIndex(
                name: "IX_ProstheticsData_CartId",
                table: "ProstheticsData",
                column: "CartId");

            migrationBuilder.CreateIndex(
                name: "IX_ProstheticsData_ScarTypeId",
                table: "ProstheticsData",
                column: "ScarTypeId");

            migrationBuilder.CreateIndex(
                name: "IX_ProstheticsData_SkinConditionTypeId",
                table: "ProstheticsData",
                column: "SkinConditionTypeId");

            migrationBuilder.CreateIndex(
                name: "IX_ProstheticsData_StumpFormId",
                table: "ProstheticsData",
                column: "StumpFormId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "BoneDustTypes");

            migrationBuilder.DropTable(
                name: "DiagnosisType");

            migrationBuilder.DropTable(
                name: "DirectToService");

            migrationBuilder.DropTable(
                name: "ProductType");

            migrationBuilder.DropTable(
                name: "ProstheticsData");

            migrationBuilder.DropTable(
                name: "ScarTypes");

            migrationBuilder.DropTable(
                name: "SkinConditionTypes");

            migrationBuilder.DropTable(
                name: "StumpForm");
        }
    }
}

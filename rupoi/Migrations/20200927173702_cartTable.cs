﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

namespace rupoi.Migrations
{
    public partial class cartTable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Cart",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    Inn = table.Column<string>(maxLength: 16, nullable: true),
                    DocumentType = table.Column<int>(nullable: false),
                    FirstName = table.Column<string>(nullable: true),
                    Name = table.Column<string>(nullable: true),
                    ParentName = table.Column<string>(nullable: true),
                    Sex = table.Column<int>(nullable: false),
                    BirthDate = table.Column<DateTime>(nullable: false),
                    DocumentSeries = table.Column<string>(maxLength: 5, nullable: true),
                    DocumentNumber = table.Column<string>(maxLength: 10, nullable: true),
                    DocumentIssueDate = table.Column<DateTime>(nullable: false),
                    DocumentIssuedBy = table.Column<string>(maxLength: 100, nullable: true),
                    Address = table.Column<string>(maxLength: 250, nullable: true),
                    PhoneNumber = table.Column<string>(maxLength: 250, nullable: true),
                    LovzType = table.Column<string>(maxLength: 250, nullable: true),
                    LovzGroupe = table.Column<int>(nullable: false),
                    Note = table.Column<string>(maxLength: 250, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Cart", x => x.Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Cart");
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using rupoi.Models;
using rupoi.Services.Interfaces;
using rupoi.Data;
using rupoi.Services;
using Microsoft.EntityFrameworkCore;

namespace rupoi.Services
{
    public class ProsthesisDataService : IProsthesisDataService
    {
        ApplicationDbContext db { get; set; }

        public ProsthesisDataService(DbContextOptions<ApplicationDbContext> options)
        {
            db = new ApplicationDbContext(options);
        }

        public void CreateProsthesisData(ProsthesisData person)
        {
            db.ProsthesisDatas.Add(person);
            db.SaveChanges();
        }

        public ProsthesisData GetProsthesisData(int? id)
        {
            return db.ProsthesisDatas.Where(p => p.Id == id).FirstOrDefault();
        }

        public IEnumerable<ProsthesisData> GetProsthesisDatas()
        {
            return db.ProsthesisDatas.ToList();
        }

        public void Dispose()
        {
            db.Dispose();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using rupoi.Models;
using rupoi.Services.Interfaces;
using rupoi.Data;
using rupoi.Services;
using Microsoft.EntityFrameworkCore;

namespace rupoi.Services
{
    public class PersonService : IPersonService
    {
        ApplicationDbContext db { get; set; }

        public PersonService(DbContextOptions<ApplicationDbContext> options)
        {
            db = new ApplicationDbContext(options);
        }

        public void CreatePerson(Person person)
        {
            db.Persons.Add(person);
            db.SaveChanges();
        }

        public Person GetPerson(int? id)
        {

            var person = db.Persons
                .Where(p => p.Id == id)
                .Include(p => p.DicDocument)
                .Include(p => p.DicRegion)
                .Include(p => p.DicDistrict)
                .Include(p => p.DicVillage)
                .Include(p => p.DicCity)
                .Include(p => p.DicDocumentSeries)
                .Include(p => p.DicDisabilityReason)
                .Include(p => p.DicDisabilityGroup)
                .Include(p => p.DicDisabilityCategory)
                .FirstOrDefault();

            if (person == null) 
            {
                return null;
            }

            person.ServiceDirections = db.ServiceDirections
                .Where(p => p.PersonId == person.Id)
                .Include(p => p.DicServiceType)
                .ToList();

            person.ProsthesisDatas = db.ProsthesisDatas
                .Where(p => p.PersonId == person.Id)
                .Include(p => p.DicStumpShape)
                .Include(p => p.DicStumpMobility)
                .Include(p => p.DicScar)
                .Include(p => p.DicStumpSkinAndTissues)
                .Include(p => p.DicBoneSawdust)
                .Include(p => p.DicStumpSupport)
                .ToList();

            person.Prostheses = db.Prostheses
                .Where(p => p.PersonId == person.Id)
                .Include(p => p.DicHospitalized)
                .Include(p => p.DicTypeOrder)
                .Include(p => p.DicImpression)
                .Include(p => p.DicOrderStatus)
                .Include(p => p.DicProsthesisSide)
                .Include(p => p.DicService)
                .Include(p => p.DicSubDiagnosis)
                .Include(p => p.DicSubProductType)
                .Include(p => p.DicFinishedStatus)
                .ToList();

            person.Shoes = db.Shoes
                .Where(p => p.PersonId == person.Id)
                .Include(p => p.DicModel)
                .Include(p => p.DicMountingType)
                .Include(p => p.DicNameProduct)
                .Include(p => p.DicOrderStatus)
                .Include(p => p.DicService)
                .Include(p => p.DicSoleType)
                .Include(p => p.DicSubDescriptionProduct)
                .Include(p => p.DicSubDiagnosis)
                .Include(p => p.DicImpression)
                .Include(p => p.DicHospitalized)
                .Include(p => p.DicColor)
                .Include(p => p.DicFinishedStatus)
                .ToList();

            person.Ottobocks = db.Ottobocks
                .Where(p => p.PersonId == person.Id)
                .Include(p => p.DicHospitalized)
                .Include(p => p.DicTypeOrder)
                .Include(p => p.DicImpression)
                .Include(p => p.DicOrderStatus)
                .Include(p => p.DicProsthesisSide)
                .Include(p => p.DicService)
                .Include(p => p.DicSubDiagnosis)
                .Include(p => p.DicSubProductType)
                .Include(p => p.DicFinishedStatus)
                .ToList();

            return person;

            //return db.Persons
            //    .Include(p => p.ServiceDirections) //
            //    .ThenInclude(p => p.DicServiceType)


            //    .Include(p => p.ProsthesisDatas) //
            //    .ThenInclude(p => p.DicStumpShape)
            //    .Include(p => p.ProsthesisDatas)
            //    .ThenInclude(p => p.DicStumpMobility)
            //    .Include(p => p.ProsthesisDatas)
            //    .ThenInclude(p => p.DicScar)
            //    .Include(p => p.ProsthesisDatas)
            //    .ThenInclude(p => p.DicStumpSkinAndTissues)
            //    .Include(p => p.ProsthesisDatas)
            //    .ThenInclude(p => p.DicBoneSawdust)
            //    .Include(p => p.ProsthesisDatas)
            //    .ThenInclude(p => p.DicStumpSupport)
            //    .Include(p => p.Prostheses)
            //    .ThenInclude(p => p.DicCodeProduct)
            //    .Include(p => p.Prostheses)
            //    .ThenInclude(p => p.DicHospitalized)
            //    .Include(p => p.Prostheses)
            //    .ThenInclude(p => p.DicImpression)
            //    .Include(p => p.Prostheses)
            //    .ThenInclude(p => p.DicNameProduct)
            //    .Include(p => p.Prostheses)
            //    .ThenInclude(p => p.DicOrderStatus)
            //    .Include(p => p.Prostheses)
            //    .ThenInclude(p => p.DicProsthesisSide)
            //    .Include(p => p.Prostheses)
            //    .ThenInclude(p => p.DicService)
            //    .Include(p => p.Prostheses)
            //    .ThenInclude(p => p.DicSubDiagnosis)
            //    .Include(p => p.Prostheses)
            //    .ThenInclude(p => p.DicSubProductType)
            //    .Include(p => p.Prostheses)
            //    .ThenInclude(p => p.FinishedStatus)
;
        }

        public IEnumerable<Person> FindPerson(string pin, string surname, string name)
        {
            var persons = new List<Person>();

            if (surname == null || name == null)
            {
                persons = db.Persons.Where(c =>
                    (c.PIN != null ? c.PIN == pin : false) ||
                    (c.Surname != null ? c.Surname == surname : false) ||
                    (c.Name != null ? c.Name == name : false)).ToList();
            } else
            {
                persons = db.Persons.Where(c =>
                    (c.PIN != null ? c.PIN == pin : false) ||
                    (c.Surname != null ? c.Surname == surname : false) &
                    (c.Name != null ? c.Name == name : false)).ToList();
            }

            return persons;
        }

        public IEnumerable<Person> GetPersons()
        {
            return db.Persons.ToList();
        }

        public void Dispose()
        {
            db.Dispose();
        }
    }
}

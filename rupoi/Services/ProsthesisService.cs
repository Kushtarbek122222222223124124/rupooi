﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using rupoi.Models;
using rupoi.Services.Interfaces;
using rupoi.Data;
using rupoi.Services;
using Microsoft.EntityFrameworkCore;

namespace rupoi.Services
{
    public class ProsthesisService : IProsthesisService
    {
        ApplicationDbContext db { get; set; }

        public ProsthesisService(DbContextOptions<ApplicationDbContext> options)
        {
            db = new ApplicationDbContext(options);
        }

        public void CreateProsthesis(Prosthesis person)
        {
            db.Prostheses.Add(person);
            db.SaveChanges();
        }

        public Prosthesis GetProsthesis(int? id)
        {
            return db.Prostheses.Where(p => p.Id == id).FirstOrDefault();
        }

        public IEnumerable<Prosthesis> GetProstheses()
        {
            return db.Prostheses.ToList();
        }

        public void AddPrintData(Prosthesis prosthesis)
        {
            db.Prostheses.Update(prosthesis);
            db.SaveChanges();
        }

        public ProsthesisProductsTable GetProductsTable(int? prosthesisId)
        {
            var tableId = GetProsthesis(prosthesisId).ProsthesisProductsTableId;

            var table = db.ProsthesisProductsTables
                .Where(t => t.Id == tableId)
                .Include(t => t.ProsthesisProductsRows)
                .ThenInclude(t => t.DicCodeProduct)
                .Include(t => t.ProsthesisProductsRows)
                .ThenInclude(t => t.DicNameProduct)
                .FirstOrDefault();

            return table;
        }

        public int CreateProductsTableAndGetId(ProsthesisProductsTableBind tableBind)
        {
            ProsthesisProductsTable table = new ProsthesisProductsTable();
            db.ProsthesisProductsTables.Add(table);
            db.SaveChanges();

            db.ProsthesisProductsRows.Add(new ProsthesisProductsRow() { 
                ProsthesisProductsTableId = table.Id,
                DicNameProductId = tableBind.ProductNameId_1, 
                DicCodeProductId = tableBind.ProductCode_1,
                Size = tableBind.ProductSize_1,
                LeftQuontity = tableBind.ProductLeftQuontity_1,
                RightQuontity = tableBind.ProductRightQuontity_1,
            });

            db.ProsthesisProductsRows.Add(new ProsthesisProductsRow()
            {
                ProsthesisProductsTableId = table.Id,
                DicNameProductId = tableBind.ProductNameId_2,
                DicCodeProductId = tableBind.ProductCode_2,
                Size = tableBind.ProductSize_2,
                LeftQuontity = tableBind.ProductLeftQuontity_2,
                RightQuontity = tableBind.ProductRightQuontity_2,
            });

            db.ProsthesisProductsRows.Add(new ProsthesisProductsRow()
            {
                ProsthesisProductsTableId = table.Id,
                DicNameProductId = tableBind.ProductNameId_3,
                DicCodeProductId = tableBind.ProductCode_3,
                Size = tableBind.ProductSize_3,
                LeftQuontity = tableBind.ProductLeftQuontity_3,
                RightQuontity = tableBind.ProductRightQuontity_3,
            });
            db.ProsthesisProductsRows.Add(new ProsthesisProductsRow()
            {
                ProsthesisProductsTableId = table.Id,
                DicNameProductId = tableBind.ProductNameId_4,
                DicCodeProductId = tableBind.ProductCode_4,
                Size = tableBind.ProductSize_4,
                LeftQuontity = tableBind.ProductLeftQuontity_4,
                RightQuontity = tableBind.ProductRightQuontity_4,
            });
            db.ProsthesisProductsRows.Add(new ProsthesisProductsRow()
            {
                ProsthesisProductsTableId = table.Id,
                DicNameProductId = tableBind.ProductNameId_5,
                DicCodeProductId = tableBind.ProductCode_5,
                Size = tableBind.ProductSize_5,
                LeftQuontity = tableBind.ProductLeftQuontity_5,
                RightQuontity = tableBind.ProductRightQuontity_5,
            });
            db.ProsthesisProductsRows.Add(new ProsthesisProductsRow()
            {
                ProsthesisProductsTableId = table.Id,
                DicNameProductId = tableBind.ProductNameId_6,
                DicCodeProductId = tableBind.ProductCode_6,
                Size = tableBind.ProductSize_6,
                LeftQuontity = tableBind.ProductLeftQuontity_6,
                RightQuontity = tableBind.ProductRightQuontity_6,
            });
            db.ProsthesisProductsRows.Add(new ProsthesisProductsRow()
            {
                ProsthesisProductsTableId = table.Id,
                DicNameProductId = tableBind.ProductNameId_7,
                DicCodeProductId = tableBind.ProductCode_7,
                Size = tableBind.ProductSize_7,
                LeftQuontity = tableBind.ProductLeftQuontity_7,
                RightQuontity = tableBind.ProductRightQuontity_7,
            });
            db.ProsthesisProductsRows.Add(new ProsthesisProductsRow()
            {
                ProsthesisProductsTableId = table.Id,
                DicNameProductId = tableBind.ProductNameId_8,
                DicCodeProductId = tableBind.ProductCode_8,
                Size = tableBind.ProductSize_8,
                LeftQuontity = tableBind.ProductLeftQuontity_8,
                RightQuontity = tableBind.ProductRightQuontity_8,
            });
            db.ProsthesisProductsRows.Add(new ProsthesisProductsRow()
            {
                ProsthesisProductsTableId = table.Id,
                DicNameProductId = tableBind.ProductNameId_9,
                DicCodeProductId = tableBind.ProductCode_9,
                Size = tableBind.ProductSize_9,
                LeftQuontity = tableBind.ProductLeftQuontity_9,
                RightQuontity = tableBind.ProductRightQuontity_9,
            });
            db.ProsthesisProductsRows.Add(new ProsthesisProductsRow()
            {
                ProsthesisProductsTableId = table.Id,
                DicNameProductId = tableBind.ProductNameId_10,
                DicCodeProductId = tableBind.ProductCode_10,
                Size = tableBind.ProductSize_10,
                LeftQuontity = tableBind.ProductLeftQuontity_10,
                RightQuontity = tableBind.ProductRightQuontity_10,
            });
            db.ProsthesisProductsRows.Add(new ProsthesisProductsRow()
            {
                ProsthesisProductsTableId = table.Id,
                DicNameProductId = tableBind.ProductNameId_11,
                DicCodeProductId = tableBind.ProductCode_11,
                Size = tableBind.ProductSize_11,
                LeftQuontity = tableBind.ProductLeftQuontity_11,
                RightQuontity = tableBind.ProductRightQuontity_11,
            });
            db.ProsthesisProductsRows.Add(new ProsthesisProductsRow()
            {
                ProsthesisProductsTableId = table.Id,
                DicNameProductId = tableBind.ProductNameId_12,
                DicCodeProductId = tableBind.ProductCode_12,
                Size = tableBind.ProductSize_12,
                LeftQuontity = tableBind.ProductLeftQuontity_12,
                RightQuontity = tableBind.ProductRightQuontity_12,
            });
            db.ProsthesisProductsRows.Add(new ProsthesisProductsRow()
            {
                ProsthesisProductsTableId = table.Id,
                DicNameProductId = tableBind.ProductNameId_13,
                DicCodeProductId = tableBind.ProductCode_13,
                Size = tableBind.ProductSize_13,
                LeftQuontity = tableBind.ProductLeftQuontity_13,
                RightQuontity = tableBind.ProductRightQuontity_13,
            });
            db.ProsthesisProductsRows.Add(new ProsthesisProductsRow()
            {
                ProsthesisProductsTableId = table.Id,
                DicNameProductId = tableBind.ProductNameId_14,
                DicCodeProductId = tableBind.ProductCode_14,
                Size = tableBind.ProductSize_14,
                LeftQuontity = tableBind.ProductLeftQuontity_14,
                RightQuontity = tableBind.ProductRightQuontity_14,
            });
            db.ProsthesisProductsRows.Add(new ProsthesisProductsRow()
            {
                ProsthesisProductsTableId = table.Id,
                DicNameProductId = tableBind.ProductNameId_15,
                DicCodeProductId = tableBind.ProductCode_15,
                Size = tableBind.ProductSize_15,
                LeftQuontity = tableBind.ProductLeftQuontity_15,
                RightQuontity = tableBind.ProductRightQuontity_15,
            });
            db.ProsthesisProductsRows.Add(new ProsthesisProductsRow()
            {
                ProsthesisProductsTableId = table.Id,
                DicNameProductId = tableBind.ProductNameId_16,
                DicCodeProductId = tableBind.ProductCode_16,
                Size = tableBind.ProductSize_16,
                LeftQuontity = tableBind.ProductLeftQuontity_16,
                RightQuontity = tableBind.ProductRightQuontity_16,
            });
            db.ProsthesisProductsRows.Add(new ProsthesisProductsRow()
            {
                ProsthesisProductsTableId = table.Id,
                DicNameProductId = tableBind.ProductNameId_17,
                DicCodeProductId = tableBind.ProductCode_17,
                Size = tableBind.ProductSize_17,
                LeftQuontity = tableBind.ProductLeftQuontity_17,
                RightQuontity = tableBind.ProductRightQuontity_17,
            });
            
            db.SaveChanges();

            return table.Id;
        }


        public void Dispose()
        {
            db.Dispose();
        }
    }
}

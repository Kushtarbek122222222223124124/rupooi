﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using rupoi.Models;
using rupoi.Services.Interfaces;
using rupoi.Data;
using rupoi.Services;
using Microsoft.EntityFrameworkCore;

namespace rupoi.Services
{
    public class ShoeService : IShoeService
    {
        ApplicationDbContext db { get; set; }
        public ShoeService(DbContextOptions<ApplicationDbContext> options)
        {
            db = new ApplicationDbContext(options);
        }
        public void CreateShoe(Shoe shoe)
        {
            db.Shoes.Add(shoe);
            db.SaveChanges();
        }
        public Shoe GetShoe(int? id)
        {
            return db.Shoes.Where(p => p.Id == id).FirstOrDefault();
        }
        public IEnumerable<Shoe> GetShoes()
        {
            return db.Shoes.ToList();
        }
        public void AddPrintData(Shoe shoe)
        {
            db.Shoes.Update(shoe);
            db.SaveChanges();
        }


        public ShoeOperationsTable GetOperationsTable(int? shoeId)
        {
            var tableId = GetShoe(shoeId).ShoeOperationsTableId;
            var table = db.ShoeOperationsTables
                .Where(t => t.Id == tableId)
                .Include(t => t.ShoeOperationsRows)
                .ThenInclude(t => t.DicShoeOperation)
                .FirstOrDefault();

            return table;
        }

        public int CreateOperationsTableAndGetId(ShoeOperationTableBind tableBind)
        {
            ShoeOperationsTable table = new ShoeOperationsTable();
            db.ShoeOperationsTables.Add(table);
            db.SaveChanges();

            db.ShoeOperationsRows.Add(new ShoeOperationsRow()
            {
                ShoeOperationsTableId = table.Id,

                DicShoeOperationId = tableBind.OperationId_1,
                ExecutorFullName = tableBind.ExecutorFullName_1,
                ExecuteDate = tableBind.ExecuteDate_1,
                OtkMark = tableBind.OtkMark_1
            });

            db.ShoeOperationsRows.Add(new ShoeOperationsRow()
            {
                ShoeOperationsTableId = table.Id,

                DicShoeOperationId = tableBind.OperationId_2,
                ExecutorFullName = tableBind.ExecutorFullName_2,
                ExecuteDate = tableBind.ExecuteDate_2,
                OtkMark = tableBind.OtkMark_2
            });

            db.ShoeOperationsRows.Add(new ShoeOperationsRow()
            {
                ShoeOperationsTableId = table.Id,

                DicShoeOperationId = tableBind.OperationId_3,
                ExecutorFullName = tableBind.ExecutorFullName_3,
                ExecuteDate = tableBind.ExecuteDate_3,
                OtkMark = tableBind.OtkMark_3
            });

            db.ShoeOperationsRows.Add(new ShoeOperationsRow()
            {
                ShoeOperationsTableId = table.Id,

                DicShoeOperationId = tableBind.OperationId_4,
                ExecutorFullName = tableBind.ExecutorFullName_4,
                ExecuteDate = tableBind.ExecuteDate_4,
                OtkMark = tableBind.OtkMark_4
            });

            db.ShoeOperationsRows.Add(new ShoeOperationsRow()
            {
                ShoeOperationsTableId = table.Id,

                DicShoeOperationId = tableBind.OperationId_5,
                ExecutorFullName = tableBind.ExecutorFullName_5,
                ExecuteDate = tableBind.ExecuteDate_5,
                OtkMark = tableBind.OtkMark_5
            });

            db.ShoeOperationsRows.Add(new ShoeOperationsRow()
            {
                ShoeOperationsTableId = table.Id,

                DicShoeOperationId = tableBind.OperationId_6,
                ExecutorFullName = tableBind.ExecutorFullName_6,
                ExecuteDate = tableBind.ExecuteDate_6,
                OtkMark = tableBind.OtkMark_6
            });

            db.ShoeOperationsRows.Add(new ShoeOperationsRow()
            {
                ShoeOperationsTableId = table.Id,

                DicShoeOperationId = tableBind.OperationId_7,
                ExecutorFullName = tableBind.ExecutorFullName_7,
                ExecuteDate = tableBind.ExecuteDate_7,
                OtkMark = tableBind.OtkMark_7
            });

            db.ShoeOperationsRows.Add(new ShoeOperationsRow()
            {
                ShoeOperationsTableId = table.Id,

                DicShoeOperationId = tableBind.OperationId_8,
                ExecutorFullName = tableBind.ExecutorFullName_8,
                ExecuteDate = tableBind.ExecuteDate_8,
                OtkMark = tableBind.OtkMark_8
            });

            db.ShoeOperationsRows.Add(new ShoeOperationsRow()
            {
                ShoeOperationsTableId = table.Id,

                DicShoeOperationId = tableBind.OperationId_9,
                ExecutorFullName = tableBind.ExecutorFullName_9,
                ExecuteDate = tableBind.ExecuteDate_9,
                OtkMark = tableBind.OtkMark_9
            });

            db.ShoeOperationsRows.Add(new ShoeOperationsRow()
            {
                ShoeOperationsTableId = table.Id,

                DicShoeOperationId = tableBind.OperationId_10,
                ExecutorFullName = tableBind.ExecutorFullName_10,
                ExecuteDate = tableBind.ExecuteDate_10,
                OtkMark = tableBind.OtkMark_10
            });

            db.ShoeOperationsRows.Add(new ShoeOperationsRow()
            {
                ShoeOperationsTableId = table.Id,

                DicShoeOperationId = tableBind.OperationId_11,
                ExecutorFullName = tableBind.ExecutorFullName_11,
                ExecuteDate = tableBind.ExecuteDate_11,
                OtkMark = tableBind.OtkMark_11
            });

            db.ShoeOperationsRows.Add(new ShoeOperationsRow()
            {
                ShoeOperationsTableId = table.Id,

                DicShoeOperationId = tableBind.OperationId_12,
                ExecutorFullName = tableBind.ExecutorFullName_12,
                ExecuteDate = tableBind.ExecuteDate_12,
                OtkMark = tableBind.OtkMark_12
            });

            db.SaveChanges();

            return table.Id;
        }
        public void Dispose()
        {
            db.Dispose();
        }
    }
}

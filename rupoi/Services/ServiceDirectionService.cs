﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using rupoi.Models;
using rupoi.Services.Interfaces;
using rupoi.Data;
using rupoi.Services;
using Microsoft.EntityFrameworkCore;

namespace rupoi.Services
{
    public class ServiceDirectionService : IServiceDirectionService
    {
        ApplicationDbContext db { get; set; }

        public ServiceDirectionService(DbContextOptions<ApplicationDbContext> options)
        {
            db = new ApplicationDbContext(options);
        }

        public void CreateServiceDirection(ServiceDirection serviceDirection)
        {
            db.ServiceDirections.Add(serviceDirection);
            db.SaveChanges();
        }

        public ServiceDirection GetServiceDirection(int? id)
        {
            return db.ServiceDirections
                .Where(p => p.Id == id)
                .Include(p => p.DicServiceType)
                .FirstOrDefault();
        }

        public IEnumerable<ServiceDirection> GetServiceDirections()
        {
            return db.ServiceDirections
                .Include(p => p.DicServiceType)
                .ToList();
        }

        public void Dispose()
        {
            db.Dispose();
        }
    }
}

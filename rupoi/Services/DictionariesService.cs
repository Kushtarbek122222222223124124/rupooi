﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using Microsoft.EntityFrameworkCore;
using rupoi.Data;
using rupoi.Data.Dictionaries;
using rupoi.Models.Dictionaries;
using rupoi.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace rupoi.Services
{
    public class DictionariesService : ServiceBase, IDictionariesService
    {
        public DictionariesService(DbContextOptions<ApplicationDbContext> options, IMapper mapper) : base(options, mapper)
        {

        }

        public List<DictionaryModel> GetList(DictionariesFileterModel filetr, string type)
        {
            var ret = new List<DictionaryModel>();

            if(type == DictionaryTypesEnum.DiagnosisType.ToString())
            {
                ret = contex.DiagnosisType/*.Where(m => m.Name.Contains(filetr.Name))*/.ProjectTo<DictionaryModel>(mapper.ConfigurationProvider).ToList();
            }
            if(type == DictionaryTypesEnum.ScarType.ToString())
            {
                ret = contex.ScarTypes/*.Where(m => m.Name.Contains(filetr.Name))*/.ProjectTo<DictionaryModel>(mapper.ConfigurationProvider).ToList();
            }


            return ret;
        }

        public DictionaryModel Find(int id, string type)
        {
            var ret = new DictionaryModel();

            if (type == DictionaryTypesEnum.DiagnosisType.ToString())
            {
                var a = contex.DiagnosisType.Find(id);

                ret = mapper.Map<DiagnosisType, DictionaryModel>(a);
            }
            if (type == DictionaryTypesEnum.ScarType.ToString())
            {
                var a = contex.ScarTypes.Find(id);
                ret = mapper.Map<ScarTypes, DictionaryModel>(a);
            }


            return ret;
        }

        public void Save(DictionaryModel model, string type)
        {
            var ret = new DictionaryModel();

            if (type == DictionaryTypesEnum.DiagnosisType.ToString())
            {  
                var a = mapper.Map<DictionaryModel, DiagnosisType>(model);
                if(a.Id > 0)
                {
                    /*contex.DiagnosisType.Add(a);
                    contex.Entry(a).State = EntityState.Modified;*/

                    contex.Update(a);
                    contex.SaveChangesAsync();
                }
                else
                {
                    contex.Add(a);
                }
                           
            }
            if (type == DictionaryTypesEnum.ScarType.ToString())
            {
                var a = mapper.Map<DictionaryModel, ScarTypes>(model);
               
                if (a.Id > 0)
                {
                    contex.Update(a);
                }
                else
                {
                    contex.Add(a);
                }
            }
            
            contex.SaveChanges();
        }
    }

    
}

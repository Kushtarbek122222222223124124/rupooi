﻿using rupoi.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace rupoi.Services.Interfaces
{
    public interface IProsthesisService
    {
        void CreateProsthesis(Prosthesis prosthesis);
        Prosthesis GetProsthesis(int? id);
        IEnumerable<Prosthesis> GetProstheses();
        void AddPrintData(Prosthesis prosthesis);
        int CreateProductsTableAndGetId(ProsthesisProductsTableBind prosthesisProductsTableBind);
        ProsthesisProductsTable GetProductsTable(int? prosthesisId);
        // filter();
        // machineLearning();
        void Dispose();
    }
}

﻿using rupoi.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace rupoi.Services.Interfaces
{
    public interface IProsthesisDataService
    {
        void CreateProsthesisData(ProsthesisData prosthesisData);
        ProsthesisData GetProsthesisData(int? id);
        IEnumerable<ProsthesisData> GetProsthesisDatas();
        // filter();
        // machineLearning();
        void Dispose();
    }
}

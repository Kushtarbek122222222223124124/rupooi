﻿using rupoi.Models.Registration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace rupoi.Services.Interfaces
{
    public interface IRegistrationService
    {
        IEnumerable<RegistrationListItem> List(RegistrationFilter filter, int start, int take);

        void SaveCart(RegistrationAdd model);
    }
}

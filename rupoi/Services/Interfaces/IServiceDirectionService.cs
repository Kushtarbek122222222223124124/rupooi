﻿using rupoi.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace rupoi.Services.Interfaces
{
    public interface IServiceDirectionService
    {
        void CreateServiceDirection(ServiceDirection serviceDirection);
        ServiceDirection GetServiceDirection(int? id);
        IEnumerable<ServiceDirection> GetServiceDirections();
        // filter();
        // machineLearning();
        void Dispose();
    }
}

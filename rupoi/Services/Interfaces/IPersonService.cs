﻿using rupoi.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace rupoi.Services.Interfaces
{
    public interface IPersonService
    {
        void CreatePerson(Person person);
        Person GetPerson(int? id);
        IEnumerable<Person> GetPersons();
        IEnumerable<Person> FindPerson(string pin, string surname, string name);
        // filter();
        // machineLearning();
        void Dispose();
    }
}

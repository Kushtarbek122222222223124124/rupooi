﻿using rupoi.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace rupoi.Services.Interfaces
{
    public interface IOttobockService
    {
        void CreateOttobock(Ottobock ottobock);
        Ottobock GetOttobock(int? id);
        IEnumerable<Ottobock> GetOttobocks();
        void AddPrintData(Ottobock ottobock);
        int CreateMaterialsAndEquipmentTableAndGetId(MaterialsAndEquipmentTableBind materialsAndEquipmentTableBind);
        MaterialsAndEquipmentTable GetMaterialsAndEquipmentTable(int? ottobockId);
        // filter();
        // machineLearning();
        void Dispose();
    }
}

﻿using rupoi.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace rupoi.Services.Interfaces
{
    public interface IShoeService
    {
        void CreateShoe(Shoe shoe);
        Shoe GetShoe(int? id);
        IEnumerable<Shoe> GetShoes();
        void AddPrintData(Shoe shoe);

        int CreateOperationsTableAndGetId(ShoeOperationTableBind shoeOperationTableBind);
        ShoeOperationsTable GetOperationsTable(int? shoeId);

        // filter();
        // machineLearning();
        void Dispose();
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using rupoi.Models;
using rupoi.Services.Interfaces;
using rupoi.Data;
using rupoi.Services;
using Microsoft.EntityFrameworkCore;

namespace rupoi.Services
{
    public class OttobockService : IOttobockService
    {
        ApplicationDbContext db { get; set; }

        public OttobockService(DbContextOptions<ApplicationDbContext> options)
        {
            db = new ApplicationDbContext(options);
        }

        public void CreateOttobock(Ottobock  ottobock)
        {
            db.Ottobocks.Add(ottobock);
            db.SaveChanges();
        }

        public Ottobock GetOttobock(int? id)
        {
            return db.Ottobocks.Where(p => p.Id == id).FirstOrDefault();
        }

        public IEnumerable<Ottobock> GetOttobocks()
        {
            return db.Ottobocks.ToList();
        }

        public void AddPrintData(Ottobock Ottobock)
        {
            db.Ottobocks.Update(Ottobock);
            db.SaveChanges();
        }

        public MaterialsAndEquipmentTable GetMaterialsAndEquipmentTable(int? ottobockId)
        {
            var tableId = GetOttobock(ottobockId).MaterialsAndEquipmentTableId;

            var table = db.MaterialsAndEquipmentTables
                .Where(t => t.Id == tableId)
                .Include(t => t.MaterialsAndEquipmentRows)
                .FirstOrDefault();

            return table;
        }

        public int CreateMaterialsAndEquipmentTableAndGetId(MaterialsAndEquipmentTableBind tableBind)
        {
            MaterialsAndEquipmentTable table = new MaterialsAndEquipmentTable();
            db.MaterialsAndEquipmentTables.Add(table);
            db.SaveChanges();

            db.MaterialsAndEquipmentRows.Add(new MaterialsAndEquipmentRow() { 
                MaterialsAndEquipmentTableId = table.Id,
                NameOfMaterials = tableBind.NameOfMaterials_1,
                VendorCode = tableBind.VendorCode_1,
                Note = tableBind.Note_1,
                Sum = tableBind.Sum_1,
                Quontity = tableBind.Quontity_1
            });

            db.MaterialsAndEquipmentRows.Add(new MaterialsAndEquipmentRow()
            {
                MaterialsAndEquipmentTableId = table.Id,
                NameOfMaterials = tableBind.NameOfMaterials_2,
                VendorCode = tableBind.VendorCode_2,
                Note = tableBind.Note_2,
                Sum = tableBind.Sum_2,
                Quontity = tableBind.Quontity_2
            });

            db.MaterialsAndEquipmentRows.Add(new MaterialsAndEquipmentRow()
            {
                MaterialsAndEquipmentTableId = table.Id,
                NameOfMaterials = tableBind.NameOfMaterials_3,
                VendorCode = tableBind.VendorCode_3,
                Note = tableBind.Note_3,
                Sum = tableBind.Sum_3,
                Quontity = tableBind.Quontity_3
            });

            db.MaterialsAndEquipmentRows.Add(new MaterialsAndEquipmentRow()
            {
                MaterialsAndEquipmentTableId = table.Id,
                NameOfMaterials = tableBind.NameOfMaterials_4,
                VendorCode = tableBind.VendorCode_4,
                Note = tableBind.Note_4,
                Sum = tableBind.Sum_4,
                Quontity = tableBind.Quontity_4
            });

            db.MaterialsAndEquipmentRows.Add(new MaterialsAndEquipmentRow()
            {
                MaterialsAndEquipmentTableId = table.Id,
                NameOfMaterials = tableBind.NameOfMaterials_5,
                VendorCode = tableBind.VendorCode_5,
                Note = tableBind.Note_5,
                Sum = tableBind.Sum_5,
                Quontity = tableBind.Quontity_5
            });

            db.MaterialsAndEquipmentRows.Add(new MaterialsAndEquipmentRow()
            {
                MaterialsAndEquipmentTableId = table.Id,
                NameOfMaterials = tableBind.NameOfMaterials_6,
                VendorCode = tableBind.VendorCode_6,
                Note = tableBind.Note_6,
                Sum = tableBind.Sum_6,
                Quontity = tableBind.Quontity_6
            });

            db.MaterialsAndEquipmentRows.Add(new MaterialsAndEquipmentRow()
            {
                MaterialsAndEquipmentTableId = table.Id,
                NameOfMaterials = tableBind.NameOfMaterials_7,
                VendorCode = tableBind.VendorCode_7,
                Note = tableBind.Note_7,
                Sum = tableBind.Sum_7,
                Quontity = tableBind.Quontity_7
            });

            db.MaterialsAndEquipmentRows.Add(new MaterialsAndEquipmentRow()
            {
                MaterialsAndEquipmentTableId = table.Id,
                NameOfMaterials = tableBind.NameOfMaterials_8,
                VendorCode = tableBind.VendorCode_8,
                Note = tableBind.Note_8,
                Sum = tableBind.Sum_8,
                Quontity = tableBind.Quontity_8
            });


            db.MaterialsAndEquipmentRows.Add(new MaterialsAndEquipmentRow()
            {
                MaterialsAndEquipmentTableId = table.Id,
                NameOfMaterials = tableBind.NameOfMaterials_9,
                VendorCode = tableBind.VendorCode_9,
                Note = tableBind.Note_9,
                Sum = tableBind.Sum_9,
                Quontity = tableBind.Quontity_9
            });



            db.MaterialsAndEquipmentRows.Add(new MaterialsAndEquipmentRow()
            {
                MaterialsAndEquipmentTableId = table.Id,
                NameOfMaterials = tableBind.NameOfMaterials_10,
                VendorCode = tableBind.VendorCode_10,
                Note = tableBind.Note_10,
                Sum = tableBind.Sum_10,
                Quontity = tableBind.Quontity_10
            });


            db.MaterialsAndEquipmentRows.Add(new MaterialsAndEquipmentRow()
            {
                MaterialsAndEquipmentTableId = table.Id,
                NameOfMaterials = tableBind.NameOfMaterials_11,
                VendorCode = tableBind.VendorCode_11,
                Note = tableBind.Note_11,
                Sum = tableBind.Sum_11,
                Quontity = tableBind.Quontity_11
            });

            db.MaterialsAndEquipmentRows.Add(new MaterialsAndEquipmentRow()
            {
                MaterialsAndEquipmentTableId = table.Id,
                NameOfMaterials = tableBind.NameOfMaterials_12,
                VendorCode = tableBind.VendorCode_12,
                Note = tableBind.Note_12,
                Sum = tableBind.Sum_12,
                Quontity = tableBind.Quontity_12
            });

            db.MaterialsAndEquipmentRows.Add(new MaterialsAndEquipmentRow()
            {
                MaterialsAndEquipmentTableId = table.Id,
                NameOfMaterials = tableBind.NameOfMaterials_13,
                VendorCode = tableBind.VendorCode_13,
                Note = tableBind.Note_13,
                Sum = tableBind.Sum_13,
                Quontity = tableBind.Quontity_13
            });

            db.MaterialsAndEquipmentRows.Add(new MaterialsAndEquipmentRow()
            {
                MaterialsAndEquipmentTableId = table.Id,
                NameOfMaterials = tableBind.NameOfMaterials_14,
                VendorCode = tableBind.VendorCode_14,
                Note = tableBind.Note_14,
                Sum = tableBind.Sum_14,
                Quontity = tableBind.Quontity_14
            });

            db.MaterialsAndEquipmentRows.Add(new MaterialsAndEquipmentRow()
            {
                MaterialsAndEquipmentTableId = table.Id,
                NameOfMaterials = tableBind.NameOfMaterials_15,
                VendorCode = tableBind.VendorCode_15,
                Note = tableBind.Note_15,
                Sum = tableBind.Sum_15,
                Quontity = tableBind.Quontity_15
            });

            db.MaterialsAndEquipmentRows.Add(new MaterialsAndEquipmentRow()
            {
                MaterialsAndEquipmentTableId = table.Id,
                NameOfMaterials = tableBind.NameOfMaterials_16,
                VendorCode = tableBind.VendorCode_16,
                Note = tableBind.Note_16,
                Sum = tableBind.Sum_16,
                Quontity = tableBind.Quontity_16
            });

            db.MaterialsAndEquipmentRows.Add(new MaterialsAndEquipmentRow()
            {
                MaterialsAndEquipmentTableId = table.Id,
                NameOfMaterials = tableBind.NameOfMaterials_17,
                VendorCode = tableBind.VendorCode_17,
                Note = tableBind.Note_17,
                Sum = tableBind.Sum_17,
                Quontity = tableBind.Quontity_17
            });

            db.SaveChanges();

            return table.Id;
        }


        public void Dispose()
        {
            db.Dispose();
        }
    }
}


/*
 [{
    "PersonId": "1",
    "OrderNum": "17",
    "OrderDate": "23.12.2020",
    "DicSubProductTypeId": "1",
    "DicSubDiagnosisId": "1",
    "DicProsthesisSideId": "1",
    "DicHospitalizedId": "1",
    "DicTypeOrderId": "1",
    "DicOrderStatusId": "1",
    "OrderStatusReason": "Указ президента",
    "DicServiceId": "1",
    "DicFinishedStatusId": "1",
    "DicImpressionId": "1"
},
{
    "VendorCode_1": "Хорошо",
    "NameOfMaterials_1": "Иметь",
    "Unit_1": "Мозги",
    "Quontity_1": "Вместо",
    "Sum_1": "Новенького",
    "Note_1": "Мерса"
}
]
 
 */
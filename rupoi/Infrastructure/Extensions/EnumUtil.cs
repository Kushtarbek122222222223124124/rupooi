﻿using rupoi.Resources;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Resources;
using System.Threading.Tasks;

namespace rupoi.Infrastructure.Extensions
{
    public static class EnumUtil
    {
        /// <summary>
        /// Âîçâðàùàåò ìàññèâ çíà÷åíèé êîíñòàíò â óêàçàííîì ïåðå÷èñëåíèè.
        /// </summary>
        /// <typeparam name="T">Òèï äàííûõ ïåðå÷èñëåíèÿ.</typeparam>
        /// <returns>
        /// Ìàññèâ çíà÷åíèé êîíñòàíò.
        /// </returns>
        public static IEnumerable<T> GetValues<T>()
        {
            return Enum.GetValues(typeof(T)).Cast<T>();
        }

        /// <summary>
        /// Âîçâðàùàåò ÷èñëîâîå çíà÷åíèå êîíñòàíòû ïî å¸ èìåíè äëÿ óêàçàííîãî ïåðå÷èñëåíèÿ.
        /// </summary>
        /// <typeparam name="T">Òèï äàííûõ ïåðå÷èñëåíèÿ.</typeparam>
        /// <param name="name">Èìÿ êîíñòàíòû.</param>
        /// <returns>
        /// ×èñëîâîå çíà÷åíèå êîíñòàíòû.
        /// </returns>
        public static int GetValue<T>(string name)
        {
            var result = 0;

            try
            {
                result = (int)Enum.Parse(typeof(T), name);
            }
            catch
            {
                // ignored
            }

            return result;
        }

        /// <summary>
        /// Âîçâðàùàåò ñïèñîê ëîêàëèçîâàííûõ íàèìåíîâàíèé çíà÷åíèé êîíñòàíò â óêàçàííîì ïåðå÷èñëåíèè.
        /// </summary>
        /// <typeparam name="T">Òèï äàííûõ ïåðå÷èñëåíèÿ.</typeparam>
        /// <returns>
        /// Ñïèñîê ëîêàëèçîâàííûõ íàèìåíîâàíèé çíà÷åíèé êîíñòàíò.
        /// </returns>
        public static List<object> GetLocalizedList<T>()
        {
            var rm = new ResourceManager(typeof(LocResource));
            var ret = new List<object>();

            foreach (T item in Enum.GetValues(typeof(T)))
            {
                var resourceDisplayName = rm.GetString(typeof(T).Name + "_" + item);
                var caption = string.IsNullOrWhiteSpace(resourceDisplayName) ? "" : resourceDisplayName;
                var id = GetValue<T>(item.ToString());

                ret.Add(new EnumList { Id = id, Caption = caption });
            }

            return ret;
        }

        /// <summary>
        /// Âîçâðàùàåò ñïèñîê ëîêàëèçîâàííûõ íàèìåíîâàíèé çíà÷åíèé êîíñòàíò â óêàçàííîì ïåðå÷èñëåíèè.
        /// </summary>
        /// <typeparam name="T">Òèï äàííûõ ïåðå÷èñëåíèÿ.</typeparam>
        /// <returns>
        /// Ñïèñîê ëîêàëèçîâàííûõ íàèìåíîâàíèé çíà÷åíèé êîíñòàíò.
        /// </returns>
        public static List<EnumListString> GetLocalizedListString<T>()
        {
            var rm = new ResourceManager(typeof(LocResource));
            var ret = new List<EnumListString>();

            foreach (T item in Enum.GetValues(typeof(T)))
            {
                var resourceDisplayName = rm.GetString(typeof(T).Name + "_" + item);
                var caption = string.IsNullOrWhiteSpace(resourceDisplayName) ? "" : resourceDisplayName;
                ret.Add(new EnumListString { Id = item.ToString(), Caption = caption });
            }

            return ret;
        }

        /// <summary>
        /// Âîçâðàùàåò ñïðàâî÷íèê ëîêàëèçîâàííûõ íàèìåíîâàíèé çíà÷åíèé êîíñòàíò, ñôîðìèðîâàííûé èç óêàçàííîãî ïåðå÷èñëåíèÿ.
        /// </summary>
        /// <typeparam name="T">Òèï äàííûõ ïåðå÷èñëåíèÿ.</typeparam>
        /// <returns>
        /// Ñïèñîê ýëåìåíòîâ ñïðàâî÷íèêà ëîêàëèçîâàííûõ íàèìåíîâàíèé çíà÷åíèé êîíñòàíò äëÿ âûïàäàþùèõ ñïèñêîâ â âèäå êîëëåêöèè, â êîòîðîé â êà÷åñòâå êëþ÷åé âûñòóïàåò ÷èñëîâîå çíà÷åíèå êîíñòàíòû, à êà÷åñòâå çíà÷åíèé - ëîêàëèçîâàííîå íàèìåíîâàíèå çíà÷åíèÿ êîíñòàíòû.
        /// </returns>
        public static Dictionary<string, string> GetLocalizedDictionary<T>()
        {
            var rm = new ResourceManager(typeof(LocResource));
            var ret = new Dictionary<string, string>();

            foreach (T val in Enum.GetValues(typeof(T)))
            {
                var resourceDisplayName = rm.GetString(typeof(T).Name + "_" + val);
                var caption = string.IsNullOrWhiteSpace(resourceDisplayName) ? /*string.Format("[[{0}]]", val)*/ "" : resourceDisplayName;
                ret[val.ToString()] = caption;
            }

            return ret;
        }

        /// <summary>
        /// Âîçâðàùàåò ñïðàâî÷íèê ëîêàëèçîâàííûõ íàèìåíîâàíèé çíà÷åíèé êîíñòàíò, ñôîðìèðîâàííûé èç óêàçàííîãî ïåðå÷èñëåíèÿ.
        /// </summary>
        /// <typeparam name="T">Òèï äàííûõ ïåðå÷èñëåíèÿ.</typeparam>
        /// <returns>
        /// Ñïèñîê ýëåìåíòîâ ñïðàâî÷íèêà ëîêàëèçîâàííûõ íàèìåíîâàíèé çíà÷åíèé êîíñòàíò äëÿ âûïàäàþùèõ ñïèñêîâ â âèäå êîëëåêöèè, â êîòîðîé â êà÷åñòâå êëþ÷åé âûñòóïàåò ÷èñëîâîå çíà÷åíèå êîíñòàíòû, à êà÷åñòâå çíà÷åíèé - ëîêàëèçîâàííîå íàèìåíîâàíèå çíà÷åíèÿ êîíñòàíòû.
        /// </returns>
        public static Dictionary<int, string> GetLocalizedDictionaryIntegerId<T>()
        {

            var rm = new ResourceManager(typeof(LocResource));
            var ret = new Dictionary<int, string>();

            foreach (T item in Enum.GetValues(typeof(T)))
            {
                var resourceDisplayName = rm.GetString(typeof(T).Name + "_" + item);
                var caption = string.IsNullOrWhiteSpace(resourceDisplayName) ? "" : resourceDisplayName;
                var id = GetValue<T>(item.ToString());
                ret[id] = caption;
            }

            return ret;
        }

        /// <summary>
        /// Âîçâðàùàåò ëîêàëèçîâàííîå íàèìåíîâàíèå çíà÷åíèÿ êîíñòàíòû ïî å¸ ÷èñëîâîìó çíà÷åíèþ äëÿ óêàçàííîãî ïåðå÷èñëåíèÿ.
        /// </summary>
        /// <typeparam name="T">Òèï äàííûõ ïåðå÷èñëåíèÿ.</typeparam>
        /// <param name="enumVal">×èñëîâîå çíà÷åíèå êîíñòàíòû.</param>
        /// <returns>
        /// Ëîêàëèçîâàííîå íàèìåíîâàíèå çíà÷åíèÿ êîíñòàíòû.
        /// </returns>
        public static string GetLocalizedNameByValue<T>(int enumVal)
        {
            var rm = new ResourceManager(typeof(LocResource));
            var ret = "";

            try
            {
                var enumName = Enum.GetName(typeof(T), enumVal);
                var resourceDisplayName = rm.GetString(typeof(T).Name + "_" + enumName);
                ret = string.IsNullOrWhiteSpace(resourceDisplayName) ? "" : resourceDisplayName;
            }
            catch
            {
                // ignored
            }

            return ret;
        }

        /// <summary>
        /// Âîçâðàùàåò ëîêàëèçîâàííîå íàèìåíîâàíèå çíà÷åíèÿ êîíñòàíòû ïî å¸ èìåíè äëÿ óêàçàííîãî ïåðå÷èñëåíèÿ.
        /// </summary>
        /// <typeparam name="T">Òèï äàííûõ ïåðå÷èñëåíèÿ.</typeparam>
        /// <param name="enumName">Èìÿ êîíñòàíòû.</param>
        /// <returns>
        /// Ëîêàëèçîâàííîå íàèìåíîâàíèå çíà÷åíèÿ êîíñòàíòû.
        /// </returns>
        public static string GetLocalizedNameByName<T>(string enumName)
        {
            var rm = new ResourceManager(typeof(LocResource));
            var ret = "";

            try
            {
                var resourceDisplayName = rm.GetString(typeof(T).Name + "_" + enumName);
                ret = string.IsNullOrWhiteSpace(resourceDisplayName) ? "" : resourceDisplayName;
            }
            catch
            {
                // ignored
            }

            return ret;
        }

        public static string getCreateTempEnumTableSql<T>(string tableName, bool intKey = false)
        {
            var rm = new ResourceManager(typeof(LocResource));
            tableName = "@" + tableName;
            var ret = "";
            if (intKey)
            {
                ret = "DECLARE  " + tableName + " TABLE(Id int, Name nvarchar(250));";
                foreach (T item in Enum.GetValues(typeof(T)))
                {
                    var resourceDisplayName = rm.GetString(typeof(T).Name + "_" + item);
                    var caption = string.IsNullOrWhiteSpace(resourceDisplayName) ? "" : resourceDisplayName;
                    var id = GetValue<T>(item.ToString());
                    ret += "INSERT INTO " + tableName + " (Id, Name) VALUES (" + id + ", '" + caption + "');";
                }
            }
            else
            {
                ret = "DECLARE  " + tableName + " TABLE(Id nvarchar(250), Name nvarchar(250));";

                foreach (T item in Enum.GetValues(typeof(T)))
                {
                    var resourceDisplayName = rm.GetString(typeof(T).Name + "_" + item);
                    var caption = string.IsNullOrWhiteSpace(resourceDisplayName) ? "" : resourceDisplayName;
                    ret += "INSERT INTO " + tableName + " (Id, Name) VALUES (" + item.ToString() + ", '" + caption + "');";
                }
            }

            return ret;
        }
    }

    /// <summary>
    /// Êëàññ ýëåìåíòîâ ïðîñòîãî ñïèñêà äëÿ ïåðå÷èñëåíèé.
    /// </summary>
    public class EnumList
    {
        /// <summary>
        /// Ïîëó÷àåò èëè çàäà¸ò èäåíòèôèêàòîð.
        /// </summary>
        /// <value>
        /// Èäåíòèôèêàòîð.
        /// </value>
        public int Id { get; set; }

        /// <summary>
        /// Ïîëó÷àåò èëè çàäà¸ò íàèìåíîâàíèå.
        /// </summary>
        /// <value>
        /// Íàèìåíîâàíèå.
        /// </value>
        public string Caption { get; set; }
    }

    /// <summary>
    /// Êëàññ ýëåìåíòîâ ïðîñòîãî ñïèñêà äëÿ ïåðå÷èñëåíèé.
    /// </summary>
    public class EnumListString
    {
        /// <summary>
        /// Ïîëó÷àåò èëè çàäà¸ò èäåíòèôèêàòîð.
        /// </summary>
        /// <value>
        /// Èäåíòèôèêàòîð.
        /// </value>
        public string Id { get; set; }

        /// <summary>
        /// Ïîëó÷àåò èëè çàäà¸ò íàèìåíîâàíèå.
        /// </summary>
        /// <value>
        /// Íàèìåíîâàíèå.
        /// </value>
        public string Caption { get; set; }
    }
}

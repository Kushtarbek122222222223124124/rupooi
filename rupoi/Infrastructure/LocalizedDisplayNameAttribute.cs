﻿using rupoi.Resources;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

namespace rupoi.Infrastructure
{
    public class LocalizedDisplayNameAttribute : System.ComponentModel.DisplayNameAttribute
    {
        public LocalizedDisplayNameAttribute()
            : base()
        {
        }
        public LocalizedDisplayNameAttribute(string resourceId)
            : this(resourceId, null)
        {
        }

        public LocalizedDisplayNameAttribute(string resourceId, string defaultValue)
            : base(resourceId)
        {
            DefaultValue = defaultValue;
        }

        public override string DisplayName { get { return GetMessageFromResource(base.DisplayName, this.DefaultValue); } }

        public string DefaultValue { get; set; }

        private static string GetMessageFromResource(string resourceName, string defaultValue)
        {
            Type type = typeof(LocResource);
            if (type.GetProperty(resourceName, BindingFlags.Static | BindingFlags.Public) != null)
            {
                PropertyInfo nameProperty = type.GetProperty(resourceName, BindingFlags.Static | BindingFlags.Public);
                object result = nameProperty.GetValue(nameProperty.DeclaringType, null);

                return result.ToString();
            }

            if (defaultValue != null)
            {
                return defaultValue;
            }

            return resourceName;
        }
    }
}
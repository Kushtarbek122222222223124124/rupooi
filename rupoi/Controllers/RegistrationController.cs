﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore.ChangeTracking.Internal;
using Microsoft.Extensions.Logging;
using rupoi.Models.Registration;
using rupoi.Services.Interfaces;

namespace rupoi.Controllers
{
    public class RegistrationController : Controller
    {

        private readonly ILogger<HomeController> _logger;
        private readonly IRegistrationService registrationService;

        public RegistrationController(ILogger<HomeController> logger, IRegistrationService registrationService)
        {
            _logger = logger;
            this.registrationService = registrationService;
        }


        [Route("/Registration/Create.vue")]
        public IActionResult Create()
        {
            var model = new RegistrationAdd();

            return PartialView(model);
        }

        public IActionResult Save(RegistrationAdd model)
        {

            //if(ModelState.IsValid)


            registrationService.SaveCart(model);

            return Json(true);
        }
    }
}

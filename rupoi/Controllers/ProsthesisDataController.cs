﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using rupoi.Data;
using rupoi.Services.Interfaces;
using rupoi.Models;

namespace rupoi.Controllers
{
    [Route("api/[controller]")]
    public class ProsthesisDataController : ControllerBase
    {
        private readonly ApplicationDbContext context;
        IProsthesisDataService prosthesisDataService;

        public ProsthesisDataController(ApplicationDbContext _context, IProsthesisDataService _prosthesisDataService)
        {
            context = _context;
            prosthesisDataService = _prosthesisDataService;
        }

        [HttpGet]
        [Route("[action]/{personId}")]
        public ActionResult<CreateProsthesisData> CreateProsthesisData(int? personId)
        {
            if (personId == null)
            {
                return BadRequest();
            }

            return new CreateProsthesisData()
            {
                PersonId = personId,
                DicStumpShapes = context.DicStumpShapes.ToList(),
                DicStumpMobilities = context.DicStumpMobilities.ToList(),
                DicScars = context.DicScars.ToList(),
                DicStumpSkinAndTissues = context.DicStumpSkinAndTissues.ToList(),
                DicBoneSawdusts = context.DicBoneSawdusts.ToList(),
                DicStumpSupports = context.DicStumpSupports.ToList(),
            };
        }

        // GET: api/ProsthesisData
        [HttpGet]
        public async Task<ActionResult<IEnumerable<ProsthesisData>>> GetProsthesisDatas()
        {
            return await context.ProsthesisDatas.ToListAsync();
        }

        // GET: api/ProsthesisData/5
        [HttpGet("{id}")]
        public async Task<ActionResult<ProsthesisData>> GetProsthesisData(int id)
        {
            var prosthesisData = await context.ProsthesisDatas.FindAsync(id);

            if (prosthesisData == null)
            {
                return NotFound();
            }

            return prosthesisData;
        }

        // PUT: api/ProsthesisData/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutProsthesisData(int id, ProsthesisData prosthesisData)
        {
            if (id != prosthesisData.Id)
            {
                return BadRequest();
            }

            context.Entry(prosthesisData).State = EntityState.Modified;

            try
            {
                await context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ProsthesisDataExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/ProsthesisData
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public async Task<ActionResult<ProsthesisData>> PostProsthesisData(ProsthesisData prosthesisData)
        {
            prosthesisDataService.CreateProsthesisData(prosthesisData);

            return CreatedAtAction("GetProsthesisData", new { id = prosthesisData.Id }, prosthesisData);
        }

        // DELETE: api/ProsthesisData/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<ProsthesisData>> DeleteProsthesisData(int id)
        {
            var prosthesisData = await context.ProsthesisDatas.FindAsync(id);
            if (prosthesisData == null)
            {
                return NotFound();
            }

            context.ProsthesisDatas.Remove(prosthesisData);
            await context.SaveChangesAsync();

            return prosthesisData;
        }

        private bool ProsthesisDataExists(int id)
        {
            return context.ProsthesisDatas.Any(e => e.Id == id);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using rupoi.Data;
using rupoi.Services.Interfaces;
using rupoi.Models;

namespace rupoi.Controllers
{
    [Route("api/[controller]")]
    public class ProsthesisController : ControllerBase
    {
        private readonly ApplicationDbContext context;
        IProsthesisService prosthesisService;

        public ProsthesisController(ApplicationDbContext _context, IProsthesisService _prosthesisService)
        {
            context = _context;
            prosthesisService = _prosthesisService;
        }


        [HttpGet]
        [Route("[action]/{prosthesisId}")]
        public ActionResult<AddPrintDataProsthesis> AddPrintDataProsthesis(int? prosthesisId)
        {
            if (prosthesisId == null)
            {
                return BadRequest();
            }

            return new AddPrintDataProsthesis()
            {
                ProsthesisId = prosthesisId,
                DicNameProducts = context.DicNameProducts.ToList(),
                DicCodeProducts = context.DicCodeProducts.ToList(),
                DicImpressions = context.DicImpressions.ToList()
            };
        }

        [HttpPost]
        [Route("[action]")]
        public ActionResult AddPrintDataProsthesis(Prosthesis prosthesis)
        {

            if (prosthesis == null & prosthesis.Id == 0)
            {
                return BadRequest();
            }

            Prosthesis oldProsthesis = prosthesisService.GetProsthesis(prosthesis.Id);

            oldProsthesis.DicImpressionId = prosthesis.DicImpressionId;

            prosthesisService.AddPrintData(oldProsthesis);

            return NoContent();
        }


        [HttpGet]
        [Route("[action]/{prosthesisId}")]
        public ActionResult<ProsthesisProductsTable> ProsthesisProductsTable(int? prosthesisId)
        {
            if (prosthesisId == null)
            {
                return BadRequest();
            }

            return prosthesisService.GetProductsTable(prosthesisId);
        }

        [HttpGet]
        [Route("[action]/{personId}")]
        public ActionResult<CreateProsthesis> CreateProsthesis(int? personId)
        {
            if (personId == null)
            {
                return BadRequest();
            }

            return new CreateProsthesis()
            {
                PersonId = personId,
                DicProductTypes = context.DicProductTypes.ToList(),
                DicSubProductTypes = context.DicSubProductTypes.ToList(),
                DicDiagnoses = context.DicDiagnoses.ToList(),
                DicSubDiagnoses = context.DicSubDiagnoses.ToList(),
                DicProsthesisSides = context.DicProsthesisSides.ToList(),
                DicHospitalizeds = context.DicHospitalizeds.ToList(),
                DicTypeOrders = context.DicTypeOrders.ToList(),
                DicOrderStatuses = context.DicOrderStatuses.ToList(),
                DicServices = context.DicServices.ToList(),
                DicFinishedStatuses = context.DicFinishedStatuses.ToList(),
                DicNameProducts = context.DicNameProducts.Include(n => n.DicCodeProducts).ToList()
            };
        }


        // GET: api/Prosthesis
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Prosthesis>>> GetProstheses()
        {
            return await context.Prostheses.ToListAsync();
        }

        // GET: api/Prosthesis/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Prosthesis>> GetProsthesis(int id)
        {
            var prosthesis = await context.Prostheses.FindAsync(id);

            if (prosthesis == null)
            {
                return NotFound();
            }

            return prosthesis;
        }

        // PUT: api/Prosthesis/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutProsthesis(int id, Prosthesis prosthesis)
        {
            if (id != prosthesis.Id)
            {
                return BadRequest();
            }

            context.Entry(prosthesis).State = EntityState.Modified;

            try
            {
                await context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ProsthesisExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Prosthesis
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public async Task<ActionResult<Prosthesis>> PostProsthesis(Prosthesis prosthesis, ProsthesisProductsTableBind prosthesisProductsTableBind)
        {
            prosthesis.ProsthesisProductsTableId = prosthesisService.CreateProductsTableAndGetId(prosthesisProductsTableBind);
            prosthesisService.CreateProsthesis(prosthesis);

            return CreatedAtAction("GetProsthesis", new { id = prosthesis.Id }, prosthesis);
        }

        // DELETE: api/Prosthesis/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<Prosthesis>> DeleteProsthesis(int id)
        {
            var prosthesis = await context.Prostheses.FindAsync(id);
            if (prosthesis == null)
            {
                return NotFound();
            }

            context.Prostheses.Remove(prosthesis);
            await context.SaveChangesAsync();

            return prosthesis;
        }

        private bool ProsthesisExists(int id)
        {
            return context.Prostheses.Any(e => e.Id == id);
        }
    }
}

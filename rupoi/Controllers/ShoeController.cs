﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using rupoi.Data;
using rupoi.Services.Interfaces;
using rupoi.Models;

namespace rupoi.Controllers
{
    [Route("api/[controller]")]
    public class ShoeController : ControllerBase
    {
        private readonly ApplicationDbContext context;
        IShoeService shoeService;

        public ShoeController(ApplicationDbContext _context, IShoeService _shoeService)
        {
            context = _context;
            shoeService = _shoeService;
        }

        [HttpGet]
        [Route("[action]/{shoeId}")]
        public ActionResult<AddPrintDataShoe> AddPrintDataShoe(int? shoeId)
        {
            if (shoeId == null)
            {
                return BadRequest();
            }

            return new AddPrintDataShoe()
            {
                ShoeId = shoeId,
                DicSubDescriptionProducts = context.DicSubDescriptionProducts.ToList(),
                DicNameProducts = context.DicNameProducts.ToList(),
                DicMountingTypes = context.DicMountingTypes.ToList(),
                DicSoleTypes = context.DicSoleTypes.ToList(),
                DicImpressions = context.DicImpressions.ToList()
            };
        }

        [HttpPost]
        [Route("[action]")]
        public ActionResult AddPrintDataShoe(Shoe shoe)
        {

            if (shoe == null || shoe.Id == 0)
            {
                return NotFound();
            }

            Shoe oldShoe = shoeService.GetShoe(shoe.Id);

            if (oldShoe == null)
            {
                return NotFound();
            }

            oldShoe.DicSubDescriptionProductId = shoe.DicSubDescriptionProductId;
            oldShoe.DicNameProductId = shoe.DicNameProductId;
            oldShoe.DicMountingTypeId = shoe.DicMountingTypeId;
            oldShoe.DicSoleTypeId = shoe.DicSoleTypeId;
            oldShoe.ArchTabSize = shoe.ArchTabSize;
            oldShoe.PronatorSize = shoe.PronatorSize;
            oldShoe.FootSize = shoe.FootSize;
            oldShoe.ShorteningSize = shoe.ShorteningSize;
            oldShoe.HeelCorkSize = shoe.HeelCorkSize;
            oldShoe.DicImpressionId = shoe.DicImpressionId;

            shoeService.AddPrintData(oldShoe);

            return NoContent();
        }

        [HttpGet]
        [Route("[action]/{shoeId}")]
        public ActionResult<ShoeOperationsTable> ShoeOperationsTable(int? shoeId)
        {
            if (shoeId == null)
            {
                return BadRequest();
            }

            return shoeService.GetOperationsTable(shoeId.Value);
        }


        [HttpGet]
        [Route("[action]/{personId}")]
        public ActionResult<CreateShoe> CreateShoe(int? personId)
        {
            if (personId == null)
            {
                return BadRequest();
            }

            return new CreateShoe()
            {
                PersonId = personId,
                DicProductTypes = context.DicProductTypes.ToList(),
                DicSubProductTypes = context.DicSubProductTypes.ToList(),
                DicDiagnoses = context.DicDiagnoses.ToList(),
                DicSubDiagnoses = context.DicSubDiagnoses.ToList(),
                DicModels = context.DicModels.ToList(),
                DicColors = context.DicColors.ToList(),
                DicHospitalizeds = context.DicHospitalizeds.ToList(),
                DicTypeOrders = context.DicTypeOrders.ToList(),
                DicOrderStatuses = context.DicOrderStatuses.ToList(),
                DicServices = context.DicServices.ToList(),
                DicFinishedStatuses = context.DicFinishedStatuses.ToList(),

                DicShoeOperations = context.DicShoeOperations.ToList(),
            };
        }


        // GET: api/Shoe
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Shoe>>> GetShoes()
        {
            return await context.Shoes.ToListAsync();
        }

        // GET: api/Shoe/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Shoe>> GetShoe(int id)
        {
            var shoe = await context.Shoes.FindAsync(id);

            if (shoe == null)
            {
                return NotFound();
            }

            return shoe;
        }

        // PUT: api/Shoe/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutShoe(int id, Shoe shoe)
        {
            if (id != shoe.Id)
            {
                return BadRequest();
            }

            context.Entry(shoe).State = EntityState.Modified;

            try
            {
                await context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ShoeExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Shoe
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public async Task<ActionResult<Shoe>> PostShoe(Shoe shoe, ShoeOperationTableBind shoeOperationTableBind)
        {
            shoe.ShoeOperationsTableId = shoeService.CreateOperationsTableAndGetId(shoeOperationTableBind);
            shoeService.CreateShoe(shoe);

            return CreatedAtAction("GetShoe", new { id = shoe.Id }, shoe);
        }

        // DELETE: api/Shoe/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<Shoe>> DeleteShoe(int id)
        {
            var shoe = await context.Shoes.FindAsync(id);
            if (shoe == null)
            {
                return NotFound();
            }

            context.Shoes.Remove(shoe);
            await context.SaveChangesAsync();

            return shoe;
        }

        private bool ShoeExists(int id)
        {
            return context.Shoes.Any(e => e.Id == id);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using rupoi.Data;
using rupoi.Services.Interfaces;
using rupoi.Models;

namespace rupoi.Controllers
{
    [Route("api/[controller]")]
    public class ServiceDirectionController : ControllerBase
    {
        private readonly ApplicationDbContext context;
        IServiceDirectionService directionService;

        public ServiceDirectionController(ApplicationDbContext _context, IServiceDirectionService _directionService)
        {
            context = _context;
            directionService = _directionService;
        }

        [HttpGet]
        [Route("[action]/{personId}")]
        public ActionResult<CreateServiceDirection> CreateServiceDirection(int? personId)
        {
            if (personId == null)
            {
                return BadRequest();
            }

            return new CreateServiceDirection()
            {
                PersonId = personId,
                DicServiceTypes = context.DicServiceTypes.ToList()
            };
        }

        // GET: api/ServiceDirection
        [HttpGet]
        public async Task<ActionResult<IEnumerable<ServiceDirection>>> GetServiceDirections()
        {
            return await context.ServiceDirections.ToListAsync();
        }

        // GET: api/ServiceDirection/5
        [HttpGet("{id}")]
        public async Task<ActionResult<ServiceDirection>> GetServiceDirection(int id)
        {
            var serviceDirection = await context.ServiceDirections.FindAsync(id);

            if (serviceDirection == null)
            {
                return NotFound();
            }

            return serviceDirection;
        }

        // PUT: api/ServiceDirection/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutServiceDirection(int id, ServiceDirection serviceDirection)
        {
            if (id != serviceDirection.Id)
            {
                return BadRequest();
            }

            context.Entry(serviceDirection).State = EntityState.Modified;

            try
            {
                await context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ServiceDirectionExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/ServiceDirection
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public async Task<ActionResult<ServiceDirection>> PostServiceDirection(ServiceDirection serviceDirection)
        {
            directionService.CreateServiceDirection(serviceDirection);

            return CreatedAtAction("GetServiceDirection", new { id = serviceDirection.Id }, serviceDirection);
        }

        // DELETE: api/ServiceDirection/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<ServiceDirection>> DeleteServiceDirection(int id)
        {
            var serviceDirection = await context.ServiceDirections.FindAsync(id);
            if (serviceDirection == null)
            {
                return NotFound();
            }

            context.ServiceDirections.Remove(serviceDirection);
            await context.SaveChangesAsync();

            return serviceDirection;
        }

        private bool ServiceDirectionExists(int id)
        {
            return context.ServiceDirections.Any(e => e.Id == id);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using rupoi.Data;
using rupoi.Models;
using rupoi.Services.Interfaces;

namespace rupoi.Controllers
{
    [Route("api/[controller]")]
    //[ApiController]
    public class OttobockController : ControllerBase
    {
        private readonly ApplicationDbContext context;
        IOttobockService ottobockService;

        public OttobockController(ApplicationDbContext _context, IOttobockService _ottobockService)
        {
            context = _context;
            ottobockService = _ottobockService;
        }

        [HttpGet]
        [Route("[action]/{personId}")]
        public ActionResult<CreateOttobock> CreateOttobock(int? personId)
        {
            if (personId == null)
            {
                return BadRequest();
            }

            return new CreateOttobock()
            {
                PersonId = personId,
                DicProductTypes = context.DicProductTypes.ToList(),
                DicSubProductTypes = context.DicSubProductTypes.ToList(),
                DicDiagnoses = context.DicDiagnoses.ToList(),
                DicSubDiagnoses = context.DicSubDiagnoses.ToList(),
                DicProsthesisSides = context.DicProsthesisSides.ToList(),
                DicHospitalizeds = context.DicHospitalizeds.ToList(),
                DicTypeOrders = context.DicTypeOrders.ToList(),
                DicOrderStatuses = context.DicOrderStatuses.ToList(),
                DicServices = context.DicServices.ToList(),
                DicFinishedStatuses = context.DicFinishedStatuses.ToList(),
                DicNameProducts = context.DicNameProducts.Include(n => n.DicCodeProducts).ToList()
            };
        }

        [HttpGet]
        [Route("[action]/{ottobockId}")]
        public ActionResult<MaterialsAndEquipmentTable> MaterialsAndEquipmentTable(int? ottobockId)
        {
            if (ottobockId == null)
            {
                return BadRequest();
            }

            return ottobockService.GetMaterialsAndEquipmentTable(ottobockId);
        }

        // GET: api/Ottobock
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Ottobock>>> GetOttobocks()
        {
            return await context.Ottobocks.ToListAsync();
        }

        // GET: api/Ottobock/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Ottobock>> GetOttobock(int id)
        {
            var ottobock = await context.Ottobocks.FindAsync(id);

            if (ottobock == null)
            {
                return NotFound();
            }

            return ottobock;
        }

        // PUT: api/Ottobock/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutOttobock(int id, Ottobock ottobock)
        {
            if (id != ottobock.Id)
            {
                return BadRequest();
            }

            context.Entry(ottobock).State = EntityState.Modified;

            try
            {
                await context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!OttobockExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Ottobock
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public async Task<ActionResult<Ottobock>> PostOttobock(Ottobock ottobock, MaterialsAndEquipmentTableBind materialsAndEquipmentTableBind)
        {
            ottobock.MaterialsAndEquipmentTableId = ottobockService.CreateMaterialsAndEquipmentTableAndGetId(materialsAndEquipmentTableBind);
            ottobockService.CreateOttobock(ottobock);

            return CreatedAtAction("GetOttobock", new { id = ottobock.Id }, ottobock);
        }

        // DELETE: api/Ottobock/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<Ottobock>> DeleteOttobock(int id)
        {
            var ottobock = await context.Ottobocks.FindAsync(id);
            if (ottobock == null)
            {
                return NotFound();
            }

            context.Ottobocks.Remove(ottobock);
            await context.SaveChangesAsync();

            return ottobock;
        }

        private bool OttobockExists(int id)
        {
            return context.Ottobocks.Any(e => e.Id == id);
        }
    }
}

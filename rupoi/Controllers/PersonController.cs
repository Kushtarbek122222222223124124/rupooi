﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using rupoi.Data;
using rupoi.Models;
using rupoi.Services.Interfaces;

namespace rupoi.Controllers
{
    [Route("api/[controller]")]
    public class PersonController : ControllerBase
    {
        private readonly ApplicationDbContext context;
        IPersonService personService;


        public PersonController(IPersonService _personService, ApplicationDbContext _context)
        {
            context = _context;
            personService = _personService;
        }

        [HttpGet]
        [Route("[action]/{pin}/{surname}/{name}")]
        public ActionResult<List<Person>> SearchPerson(string pin, string surname, string name)
        {

            if (String.IsNullOrEmpty(pin) & String.IsNullOrEmpty(surname) & String.IsNullOrEmpty(name))
            {
                return BadRequest();
            }

            if (String.IsNullOrEmpty(pin))
            {
                return BadRequest();
            }
            
            var persons = personService.FindPerson(pin, surname, name);
            return persons.ToList();
        }

        [HttpGet]
        [Route("[action]")]
        public CreatePerson CreatePerson()
        {
            return new CreatePerson()
            {
                DicDocuments = context.DicDocuments.ToList(),
                DicDocumentSeries = context.DicDocumentSeries.ToList(),
                DicRegions = context.DicRegions.ToList(),
                DicDistricts = context.DicDistricts.ToList(),
                DicVillages = context.DicVillages.ToList(),
                DicCities = context.DicCities.ToList(),
                DicDisabilityCategories = context.DicDisabilityCategories.ToList(),
                DicDisabilityGroups = context.DicDisabilityGroups.ToList(),
                DicDisabilityReasons = context.DicDisabilityReasons.ToList(),
            };
        }

        //[HttpGet]
        //[Route("[action]/{id}")]
        //public ActionResult<Person> DetailsPerson(int? id)
        //{
        //    if (id == null)
        //    {
        //        return BadRequest();
        //    }

        //    var person = personService.GetPerson(id);

        //    return person;
        //}


        // GET: api/Person
        [HttpGet]
        public IEnumerable<Person> GetPersons()
        {
            var persons = personService.GetPersons();
            return persons;
        }

        // GET: api/Person/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Person>> GetPerson(int id)
        {
            //var person = await context.Persons.FindAsync(id);

            //if (person == null)
            //{
            //    return NotFound();
            //}

            //return person;

            if (id == null)
            {
                return BadRequest();
            }

            var person = personService.GetPerson(id);

            return person;
        }

        // PUT: api/Person/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutPerson(int id, Person person)
        {
            if (id != person.Id)
            {
                return BadRequest();
            }

            context.Entry(person).State = EntityState.Modified;

            try
            {
                await context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!PersonExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Person
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public async Task<ActionResult<Person>> PostPerson(Person person)
        {
            personService.CreatePerson(person);

            return CreatedAtAction("GetPerson", new { id = person.Id }, person);
        }

        // DELETE: api/Person/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<Person>> DeletePerson(int id)
        {
            var person = await context.Persons.FindAsync(id);
            if (person == null)
            {
                return NotFound();
            }

            context.Persons.Remove(person);
            await context.SaveChangesAsync();

            return person;
        }

        private bool PersonExists(int id)
        {
            return context.Persons.Any(e => e.Id == id);
        }
    }
}

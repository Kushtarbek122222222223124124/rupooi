﻿using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace rupoi.Framework
{
    public class PhController : Controller
    {
        protected JsonStringResult JsonData(Object jsonData)
        {
            var json = JsonConvert.SerializeObject(jsonData, new IsoDateTimeConverter() { DateTimeFormat = "dd.MM.yyyy" });

            return new JsonStringResult(json);
        }
    }
}
